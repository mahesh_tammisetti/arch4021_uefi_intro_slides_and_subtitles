class: center, middle, intro

# Arch4021: Introductory UEFI

---

# Goals of this lecture

.center[.image-20[![](images/uefi.png)]]

This lecture serves as an introduction to firmware -- specifically for UEFI and
its components. Subjects that will be discussed:
- Fundamentals of Firmware
- Basics of UEFI
- UEFI Boot Flow
- Introduce UEFI Variables and services related to it
- Explain UEFI Authenticated Variables for UEFI Secure Boot needs

???

The subjects of this lecture are:
- Introduction to UEFI
- UEFI boot phases explanation
- Where to look for various UEFI-related specifications
- UEFI Secure Boot as a chain of trust technology

- [X] Firmware Fundamentals
    + [X] What is firmware
    + [X] Firmware implementations and history
- UEFI Basics
    + [X] Goals
    + [X] Features
    + [X] Architecture overview
    + [X] Implementations
    + [X] Criticism
- UEFI Boot Flow
    + [X] Design principles
    + [X] Boot flow overview
    + [X] UEFI Phases and their goals
    + [X] Post-EFI phases

---
class: center, middle, intro

# Firmware Fundamentals

---

# What is firmware

.center[.image-40[![](images/uefi_bios.jpg)] .image-50[![](images/fw_upgrade.jpg)]]

- Software that is hard to get to
- Software considered by users as a part of the hardware
- Lowest-level software providing hardware initialization
- Hardware initialization allows to make it more flexible and compatible
- OS could do that, but loading it is harder than one would think
- There are also critical parts that shouldn't be done by OS

.footnote[[ThinkPad Photo](https://commons.wikimedia.org/wiki/File:Thinkpad-t430-bios-main.jpg)
by Vitaly Zdanevich/CC BY-SA 4.0, Firmware upgrade - public domain]

---

# What is firmware

.center[.image-80[![](images/ami_chip.jpg)]]

- Firmware is stored in a separate chip on mainboard, e.g. on SPI. Access
to firmware code should be restricted
- For security and IP-protection reasons some platforms (such as ARM)
use boot ROM — read only storage for most critical firmware
- A program that loads the OS is called bootloader
- Firmware memory is typically freed after passing control to bootloader or OS

.footnote[[Photo](https://commons.wikimedia.org/wiki/File:AMI_Embedded_AT_Keyboard_Controller_Firmware.jpg)
by Mister rf/CC BY-SA 4.0]

???

To boot OS we have to enable at least boot device and provide FS driver so that
we can load it. In practice we do a lot more for various reasons. Usually for
compatibility and performing the most critical code. There is initialization
that has big impact on security, mistakes in another parts could permanently
destroy hardware.

It is important to consider memory usage. Features implemented in OS have to
reside in memory together with OS during whole operation, that's why shifting
some responsibility to bootloader or firmware may make more sense. Some
operations like switching to protected mode have to be done in bootloader so it
doesn't make sense to keep this functionality in OS.

Other way to explain reasoning behind firmware existence is also path that we
get through during boot process. It is from very hardware specific and
constrained environment (real mode reset vector area) to unified generic mode
(OS with capability of running application which use various hardware
component).

---

# Boot firmware history

.center[.image-60[![](images/pdp.jpg)]]

- History of boot process is rich and very interesting
- During early days computer booting sequence was entered manually by setting up
correct row of switches indicating options
- Old computers had less sophisticated initialization mechanism because those
were much simpler, there were not a lot of peripherals and interaction with
them were hardcoded

.footnote[[Photo](https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Digital_pdp8-e2.jpg/640px-Digital_pdp8-e2.jpg?download) by Florian Schaffer/CC BY-SA 4.0]

???

<https://en.wikipedia.org/wiki/Booting>
<http://wiki.osdev.org/BIOS>

---

# Boot firmware history

.center[.image-40[![](images/cokpit.jpg)]]

- These days various components can be used in different applications and because
of that have extensive configuration possibility
- We also have a lot of communication standards and controllers that handle them, which
brings the need for providing information about operational parameter and the way
that a given controller will be used
- Purpose of boot process is still the same: run target application

.footnote[[Photo](https://commons.wikimedia.org/wiki/File:Airbus-319-cockpit.jpg#/media/Plik:Airbus-319-cockpit.jpg) by Ralf Roletschek / roletschek.at]

???

Key roles of modern BIOS:
- initialize and test hardware
- load operating system (or other target application)
- initialize system management functions e.g. power and thermal management
- load patches during boot process e.g. firmware updates, microcode patches
- help in recovery in case of operating system (or other target application)
  malfunction

---

# Basic Input/Output System

.center[.image-80[![](images/bios_timeline_1.svg)]]

???

- The term BIOS was first used in 1975 by Gary Kildall in CP/M OS predecessor
  of MS-DOS
- BIOS was a name of boot firmware implementations on x86 used for over 50
  years. This term is familiar even to many non-technical users. That's why
  it's often used as a synonym for boot firmware.
- It was written in pure assembly. It needed very deep knowledge and debugging
  it was very time consuming.
- It provided hardware initialization, configuration utility and a set of
  runtime services. BIOS was looking for boot loader in first 512 bytes of
  possible boot media.
- Winzent firmware with rapid boot feature is example of legacy BIOS
  implementation with very interesting improvements
- typically closed source, proprietary license and delivered in binary form
- BIOS served it purpose very well for a long time. In the beginning even user
  applications used its runtime services.
- In 1990s many changes in technology came, 32-bit protected mode, a lot of new
  devices, faster CPUs and more memory were available.
- In DOS, applications had full access to hardware and BIOS services. When
  Windows 95 came, real mode became obsolete and all applications were
  separated from hardware.
- As written in assembly (which was the only way in early years), BIOS was
  getting more and more complicated, its services was used only by boot loader.
  Adapting it to modern needs was virtually impossible.
- In mid-1990s Intel started working on Extensible Firmware Interface (EFI) to
  replace BIOS on Itanium platform.
- In 2005, Intel released EFI 1.10 specification, the last one before the UEFI
  Forum was established by Intel with other companies including ARM, AMD, HP,
  Lenovo, Phoenix Technologies, Microsoft, IBPM and others.

---

# From BIOS to UEFI

.center[.image-99[![](images/bios_timeline_2.svg)]]

???

- In 2006, Platform Initialization v1.0 was released and in 2008 UEFI 2.1 was
  released.
- However until 2010 BIOS still was a standard on x86. Then PI and UEFI-based
  firmware implementations started to become widely used.
- For various reasons (e.g. old hardware, legacy software) BIOS is in use.

---

# From BIOS to UEFI

- Starting 2020 Intel removed support for Legacy BIOS - new platforms are
  tested only using UEFI
- Some vendors may still provide Compatibility Support Module (CSM) used to
  provide Legacy BIOS interface
- There is ongoing effort of open-source firmware community to mitigate
  problems that UEFI brings
- The latest UEFI specification, version 2.10, was published in August 2022

???

---

# Jargon

- Correctness of given naming scheme is subject to some debate.
    + All major industry players of computer systems supply chain keep calling
    their bootstrap firmware BIOS
    + Microsoft use UEFI term instead
    + Apple use EFI term, except security documentation where they use UEFI
- Firmware that provide functionality of old BIOS is typically called Legacy BIOS
- PI (_Platform Initialization_) and  UEFI (_Unified Extensible Firmware
  Interface_) compatible firmware implementations are sometimes called UEFI
  BIOS, or just UEFI

---

# Jargon

- Can we say that UEFI replaced BIOS?
    + No, we can say that UEFI and PI compliant firmware replaced previous BIOS
    implementations
- During presentation terms UEFI, BIOS and UEFI BIOS are used interchangeably.
- Please note UEFI and PI Specification still use EFI acronym, especially on
  diagrams, which in this case means UEFI.

.footnote[https://www.basicinputoutput.com/2014/08/will-i-be-jailed-for-saying-uefi-bios.html]

---

# Quiz #1

**What is the purpose of boot firmware?**

--

- Initialize hardware so that OS (or dedicated application) can be run

--

**What was the reason to replace BIOS?**

--

- Assembly code is hard to change
- BIOS was adapted to obsolete features of x86

--

**What is currently the most popular host CPU boot firmware in x86?**

--

- UEFI, since the transition from Legacy BIOS


---

class: center, middle, intro

# UEFI Basics

## Based on UEFI 2.9 (March 2021) and UEFI PI 1.7A (April 2020)

???

- Why version of specification is important?
    + Your UEFI BIOS may be not compliant with most recent version of specification
    + If we develop UEFI-related code we should always rely on specification
    version that our UEFI BIOS implements
    + This course is not based on most recent version of specification, but we
    touching very generic UEFI concepts which do not change very often.

---

# Initial UEFI goals

- Originally, EFI was designed to boot Itanium-based computers,
- Itanium was the first 64-bit CPU from Intel, totally different from x86, so
  reusing BIOS code was impossible
- Legacy BIOS was stuck in 16-bit real mode, so even for backward compatible
  platforms it was (and happens to be) a necessary evil
- EFI was supposed to overcome those and many other limitations coming from
  computer industry innovation
- Proprietary nature of BIOS was a compatibility issue rooted in bad design...
- Another goal was to replace obsolete assembly with a high level language
- Finally creating the UEFI specification was a brilliant move to preserve long
  lasting influence on a key component connecting the hardware and operating
  system

???

Increasing popularity of mobile devices and IoT was an opportunity well
leveraged by ARM, so that even Windows has been ported to that platform.
Increased significance of Open Source (especially GNU/Linux) may be another
reason for open spec, so that Open Source software can support it well. But of
course compatibility was an issue even on one platform and OS.

In 1990s assembly language was still widely used as a regular programming
language, mainly because of performance reasons — every byte and every CPU
cycle was precious. Also compilers produce much more efficient code. This is
no longer the case (except some embedded applications). Nowadays, so huge
assembly code base is just a one huge technical debt.

One might argue that C is not a high level language because we have a lot of
far more abstracted languages. However, anyone who works with assembly code
knows that it deserves to be called so. ;-)

---

# Current UEFI goals

- **_Coherent, scalable platform environment_**
    + Create a description of platform features and capabilities that can be
    consumed by OS and will work for a wide range of designs
- **_Abstraction of the OS from the firmware_**
    + OS loader creation simplification through known, stable and minimal
    interfaces
- **_Reasonable device abstraction free of legacy interfaces_**
    + Define abstract interfaces that enable support for a wide range of underlying
    hardware
- **_Abstraction of Option ROMs from the firmware_**
    + Interfaces for standard bus types allow clear separation of Option ROMs and
    their interface from firmware
- **_Architecturally shareable system partition_**
    + Allows platform features expansion by clearly defining how mass storage can
    be used for additional applications

???

UEFI Specification ver 2.9: 1.3 Goals

- First, specification aims to define a complete solution for the firmware to
  describe all platform features and provide that to an operating system in
  a coherent way. The goal is to have one way to describe multiple platform
  designs.
- Second, specification goal is to define interfaces to platform capabilities.
  Those interfaces should simplify design and implementation of OS loaders and
  provide stable boundary between platform, firmware and OS. Implementations do
  not exactly follow this goal since division of labor between OS loader and OS
  kernel is no clear. We will discuss that in section related to Boot Device
  Selection boot stage in context of "who should call ExitBootServices".
- Third goal should make OS loaders more portable since its operation should be
  abstracted from specific hardware implementation. Since we can install and
  boot most operating systems on most modern hardware this goal is implemented
  by UEFI Forum quite well.
- Fourth goal, lead to defining in specification interfaces that can abstract
  standard buses like PCI, USB or SCSI. More to that specification define
  abstraction for bus itself, so new buses can be added over time. This should
  improve Option ROMs discovery and execution, as well as standardize its
  format.
- Last goal, leads to creation of an additional partition to hold software that can
  be used by firmware. This technique creates not so constrained space, in
  comparison to SPI flash, where value added software can be installed and used
  by firmware. Typically we install bootloader, kernel and initramfs in this
  architecturally shareable system partition. It can also be used by firmware
  update mechanism to deliver update.

It is worth to mention:
- Evolutionary, not revolutionary
    + long transition
    + maintain current partners advantage
- Compatibility by design
- Simplifies addition of OS-neutral platform value-add.
    + give promise of improving ecosystem and partners advantage
- Built on existing investment
    + do not waste already spent money
- In reality it is quite far from the goal of using minimal interfaces
- There seem to be some design conflict between minimal interfaces and wide
  range of hardware support, but Firmware Architects also need marketing in
  their work.

---

# Criticism

- UEFI specification and implementation was criticised by numerous digital
  rights and privacy activists: Ron Minnich (coreboot creator), Matthew Garrett
  (Secure Boot, UEFI and Linux expert)
- DRY: Key issues is that UEFI does not solve the problem of drivers
  redundancy, both firmware and OS need a driver for hardware that often
  duplicates the work
- Secure Boot effectively prevented platform re-ownership and promoted vendor
  lock-in
- Opened path for serious vulnerabilities related to maintenance of CA
  infrastructure e.g.
  [BootHole](https://eclypsium.com/2020/07/29/theres-a-hole-in-the-boot/)
  critical vulnerability in GRUB2
- Vendor-specific implementations based on common reference code proved to be
  low quality and vulnerable
- UEFI preserve a long-lasting firmware supply chain, which is not transparent
  and effectively prevent building trustworthy relation between end user and
  vendors

???

- The number of interfaces is forever-growing, IBVs and major OEMs like Dell
  have a bunch of own protocols.
- While we agree it can be a goal to use minimal interfaces, the reality shows
  something opposite.
- UEFI firmware providers (IBVs) tend to bloat the images with many interfaces
  which results in copies of these interfaces inside SMM for Runtime Services.
- However if we consider only the top level interfaces defined by UEFI spec and
  do not care about next-level calls to other interfaces then maybe we can call
  it "minimal".
- Support for wide range of underlying hardware too often justify adding UEFI
  modules that have no right to be supported on some platforms, e.g. Intel TXT
  modules and ACMs on platforms that certainly cannot enable Intel TXT (e.g.
  due to non-TXT-capable chipset). On the other hand, you could put twice a
  number of modules and support multiple microarchitectures (as long as flash
  space and other requirements allow it).

---

# Criticism

- Probably the biggest issue of UEFI is its big complexity.
- In long run UEFI implementations are hard to maintain.
- High complexity implies worse performance and more potential security issues.

???

There are lots opinions here. Linus Torvalds says that UEFI is worse than BIOS in
every aspect [http://yarchive.net/comp/linux/efi.html]. Of course there is lots
of truth, especially in complexity parts. Some of the reaosons may be that the
post is very old (2006) so then probably UEFI was very troublesome. Anyway C code
and open standard is quite a step ahead.

"one can imagine EDKII supporting the same number of hardware platforms as
coreboot, but it's unclear and would be speculation how much extra that would
add to the code size in order to get an apples-to-apples comparison. Or if you
want to compare still on the supported platforms metric, I'd recommend making
it fully quantitative and say like "coreboot supports 40 more hardware
platforms than EDKII for a codebase that's 300kLoC smaller" or something like
that.", Xeno Kovah

About golden key: <https://arstechnica.com/information-technology/2016/08/microsoft-secure-boot-firmware-snafu-leaks-golden-key/>

<https://archive.fosdem.org/2007/interview/ronald%2bg%2bminnich.html> — using SMM
to fool OS example.

<https://boingboing.net/2011/12/27/the-coming-war-on-general-purp.html> — about
UEFI's contribution to limiting freedom.

---

# EFI/UEFI/EDKII

.center[.image-25[![](images/uefi.png)] .image-60[![](images/tianocore.svg)]]

- **UEFI PI** is platform integration specification covering various topics
  (core PEI/DXE, shared elements, management mode and more)
- **UEFI** is a specification for communication between OS and platform/
  firmware, it has multiple implementations. Interfaces support IA-32/x64,
  AArch32/64 and RISC-V
???

- PI is more about standardization of initialization, boot stages and objects
  used during boot platform process.
- UEFI is more about interface.
- If we recall BIOS goals it was both about initialization, which was needed
  for efficient abstraction of hardware to OS.
- As well as it was also about famous interrupts routines which were interface
  between hardware (or sometimes BIOS) and OS.
- Now perspective is divided in to two specs.

There is no official WIP support for OpenPOWER.

---

# EFI/UEFI/EDKII

- **UEFI Forum** is an alliance between several leading technology companies
  maintaining a set of specifications related to firmware-OS interfaces.
  Maintained specifications:
    + UEFI Specification
    + UEFI Shell Specification
    + UEFI Platform Initialization Specification
    + UEFI Platform Initialization Distribution Packaging Specification
    + ACPI Specification
- **TianoCore** is a community that supports an open source implementation of
  UEFI
- **EDKII** is an open source (2-clause BSD) reference implementation
of UEFI and PI specifications
    + Written in C
    + Not enough to create fully functional boot firmware, but creates a
  foundation for most commercial solutions
    + Developed and maintained by the TianoCore community

---

# EFI vs UEFI

- Key EFI features:
    + Support for large disks (>2TB, next limit 8 Zebibytes = 8 * 10^70)
    + Architecture independent
    + Modular design
    + Backward compatibility through CSM (Compatibility Support Module)
- Key UEFI features:
    + Unified interfaces for programming and services
    + Open standard with modern mostly-C implementation (binding for
  MicroPython and Rust are coming)
    + Support: PCI/PCIe, SCSI, USB, ACPI, and networking
    + Security (secure boot, driver signing, crypto and key management)
    + User interface
    + Modern runtime services
    + Non-volatile OS accessible configuration variables
    + Built-in shell

???

Please note what security features UEFI provides vs EFI.

- EFI works in protected mode which provides applications long, virtual address
  space - this addressable memory problem from legacy BIOS
- In 2005 UEFI Forum, as organization managing UEFI spec
- In 2013 ACPI specification was also move under the umbrella of UEFI Forum
- UEFI is highly abstracted and complex firmware architecture
- UEFI spec has over 2.5k pages, PI spec 1.5k
- UEFI API differs a lot from BIOS IRQ based interface:
    + because it is provided in open specification
    + it is easier to modify
    + it is more secure
    + it is faster

2TB HD limit was related to MBR (Master Boot Record) used in BIOS times. GPT
(GUID partition table) increased disk size limit and relaxed other limitations
including 4 primary partitions limit.

Legacy BIOS provides runtime services as well, however most of BIOS
implementations were real-mode only and modern OS are practically 100%
protected mode (in modern firmware, switching to protected mode is practically
first thing to do). For that reason its services was meaningful only in
bootloader development. In UEFI we have broad services set including network
boot, time services, and much more. Some of them available in modern OS.

### Links

<https://www.microbe.cz/docs/Beyond_BIOS_Second_Edition_Digital_Edition_(15-12-10)%20.pdf>
<https://en.wikipedia.org/wiki/BIOS_interrupt_call>

### Other problems with BIOS

- max 2.2TB HDD supported, UEFI can 9.4ZB (1 ZB = 1 * 10^9 TB)
- 16 bit mode of operation
- change from MBR to GPT, which remove 4 primary partition limit
- low quality and not flexible UI interfaces support
- written in assembly

---

# Architecture overview

.image-80[.center[![Image](images/pi_boot_phases.jpg)]]
.footnote[Source: <https://github.com/tianocore/tianocore.github.io/wiki/PI-Boot-Flow>]

???

- SEC phase is root of trust for platform firmware.
- After Life - power management transition stage like S3, or modern S0ix, cold
  boot or warm boot, depneding on the transition firmware may tak different path.

---

# Architecture overview

- As we can see, UEFI covers only a very small part of boot firmware:
    + Hardware initialization is a subject of PI specification. The most
    important phases here are: Security (SEC) and PEI (Pre-EFI Initialization)
    + UEFI specifies only the interface to communicate with drivers and the boot
    loader, but they themselves can be anything supporting that protocol. This
    phase is called Boot Device Selection (BDS)
    + There is also an interface for communication with the OS (UEFI variables,
    Capsule, etc.)
- Only Driver eXecution Environment (DXE) has a detailed description in the
  UEFI spec (2.5k pages)

---

# Implementations

- UEFI itself is a specification and has many implementations
- EDKII is a reference implementation and is usually used as a base for custom
  implementations
- EDKII can be built for many platforms including QEMU (Open Virtual Machine
  Firmware (OVMF)). But usually some blobs must be used
- There are also some other commercial implementations like InsydeH2O and AMI
  Aptio
- It's also possible to build UEFI compatible firmware by combining coreboot
  and EDKII (Tianocore payload)
- SlimBootloader claims to be open-source boot firmware, but essentially it is
  minimal subset of EDKII - it uses the same tooling, syntax and code base
  <https://github.com/slimbootloader/slimbootloader>

???

<https://www.insyde.com/products>
<https://ami.com/en/products/bios-uefi-firmware/aptio-v/>

---

# Other implementations

- Yabits - Minoca OS-based minimal UEFI implementation that can be used as
  coreboot payload: <https://github.com/yabits/uefi>
- U-Boot UEFI implementation - because of Arm Embedded Base Boot Requirement
  and following specifications U-Boot had to provide a UEFI compatible interface, so
  they implemented part of the specification:
  <https://u-boot.readthedocs.io/en/latest/develop/uefi/uefi.html>
- u-root - Go userspace with Linux bootloaders, LinuxBoot (Linux + u-root)
  implemented some UEFI features to boot compatible OSes
  <https://github.com/u-root/u-root>
- CloverBootloader - bootloader for macOS, Windows and Linux that provides UEFI support:
  <https://github.com/CloverHackyColor/CloverBootloader>

---

# EDKII code base

.image-80[.center[![Image](images/edk2_gh.png)]]

.center[

```
ansic:      1672054 (81.88%)
perl:        159954 (7.83%)
python:      132393 (6.48%)
asm:          39954 (1.96%)
```

]

???

Statistics were generated using sloccount using:

```
c095122d4b5f 2021-12-29|MdeModulePkg/PciBusDxe: Enumerator to check for RCiEP before looking for RP
```

---

# EDKII source tree

.center[.image-25[![](images/edk2_dirs.png)] .image-50[![](images/edk2_pkg.png)]]
.center[.image-40[![](images/edk2_module.png)]]

---

# EDKII build system quick cheat-sheet

- EDKII has its own build system we use to build firmware
- Every module/application has an `*.inf` file containing metadata:
    + Name + version
    + source files, entry point
    + dependencies (packages, modules, GUIDs)
- `*.inf` usage is similar to model used by Microsoft Windows
- Output of `*.inf` modules building is Portable Executable (PE)/Common Object
  File Format (COFF)
- Modules are organized in packages. Their metadata is stored in `*.dsc` and
`*.dec` files
- `edksetup.sh` — is a script to `source` in order to use the build system
- `EDK_TOOLS_PATH` — must point at `BaseTools` directory before sourcing
  `edksetup.sh`

```shell
build -a <arch> -p <path_to_dsc_file> <flags> -t <toolchain> -b <build_type>
```

.code-comment[build command template]

---

class: center, middle, intro

# Practice #1: EDKII build system

---

# How to get documentation of build process

Before doing anything, we should refer to the documentation.
The most recent documentation of the EDKII build process is stored as code in
the TianoCore Documentation repository called `edk2-BuildSpecification`. The
easiest way to explore it is to download an already generated PDF file, for 
example, available at Tianocore's github repository, together with all other
documenations. Specifically in the `tianocore/tianocore.github.io` repository's
wiki. The document we are looking for is here:

https://tianocore-docs.github.io/edk2-BuildSpecification/release-1.28/edk2-BuildSpecification-release-1.28.pdf

---

# How to get documentation of build process

Other way to get the documents, would be cloning the `edk2-BuildSpecification`
repository and hosting a local HTTP server. This is especially needed when we
want to contribute to it.

```shell
git clone https://github.com/tianocore-docs/edk2-BuildSpecification.git
cd edk2-BuildSpecification
git checkout gh-pages
python3 -m http.server
```

Then open 0.0.0.0:8000 in your web browser.

---

# Key environment variables

* `WORKSPACE` - environment variable which keeps the name of the directory in
  which the build process is performed
* `PACKAGES_PATH` - environment variable which keeps package search paths.
  Searching always uses the first match, where `WORKSPACE` is first directory
  to be searched
* `EDK_TOOLS_PATH` - the environment was already discussed, but it points to the
  BaseTools and Conf directories which contain build tools and their
  configuration

The EDKII build system supports Windows, Linux, macOS, and various compilers.
You may check `./BaseTools/Conf/tools_def.template` to see what development
environments are supported. Please note that despite the huge number of
environments not all of them work _out of the box_.

---

# Key file extensions

* `DEC` - EDKII Declaration (DEC) file format described in edk2-DecSpecification
  (1.27-3). This file declares information about what is provided in the
  package. An EDKII package is a collection of like content.
* `DSC` - EDKII Platform Description (DSC) file format described in
  edk2-DscSpecification (1.28-2). This file describes what and how modules,
  libraries and components are to be built, as well as defines library
  instances that will be used when linking EDKII modules.
* `FDF` - EDKII Flash Description (FDF) file format described in
  edk2-FdfSpecification (1.28.01. This file is used to define the content and
  binary image layouts for firmware images, update capsules and PCI option ROMs.

---

# Key file extensions

* `INF` - EDKII build information (INF) file format described in
  edk2-InfSpecification (1.27). This file is used to describe various modules
  binary, source and mixed, also an intermediate product of EDKII build system.
  This file format is a fundamental component of build system since it describes
  most basic unit of EDKII - modules.
* `IDF` - EDKII Image Description (IDF) files used for creating HII Image Packs
  used for graphical content presentation.
* `UNI` - EDKII Unicode string file format described in edk2-UniSpecification
  (1.40). This file is used for mapping token names to localized strings that
  are identified by an RFC4646 language code - in this way EDKII supports
  switching between multiple languages.

---

# Key file extensions

* `VFR` - EDKII Visual Forms Representation file format described in
  edk2-VfrSpecification (1.92). This is the source code format that is used by
  developers to create a user interface with varying pieces of data or
  questions. This is later compiled into a binary encoding IFR (Internal Forms
  Representation) that is used for the representation of user interface pages.

`DSC` files are most important for the build process since it defines our target
platform.

Please note we tried to use links to the most recent, already generated
documentation at the point of creation of this exercise, but looking at
published material may be misleading since the repository may contain newer
content. Feel free to check the available version as well as associated GitHub
repositories.

---

# Exercise #1: Explore EDKII source tree

Find all file types mentioned above in EDKII source tree and check its content.

---

# Exercise #2: EDKII build process

Let's go over the basic EDKII build process. Normally we would start with
installing tools required for compiling and cloning EDKII sources, but to save
time and network bandwidth this is already part of VM image. For reference, this
is what is required:

```shell
$ sudo apt update
$ sudo apt install git build-essential uuid-dev nasm iasl
$ git clone --recurse-submodules --branch edk2-stable202202 https://github.com/tianocore/edk2.git
```

---

# Exercise #2: EDKII build process

To simplify environment variables setup, EDKII provides the `edksetup.sh` script
for UNIX-like environments. To use it, type:
```shell
source edksetup.sh
```

Please check build command parameters by calling `build --help` or `build -h`.

The most important parameters at this point in the course are:

- `-p PLATFORMFILE` - Build the platform specified by the DSC file name
- `-b BUILDTARGET` - DEBUG or RELEASE build target
- `-t TOOLCHAIN` - Using the toolchain tag name to build the platform defined in
tools_def.template, in this course we use GCC5
- `-n THREADNUMBER` - Build the platform using multi-threaded compiler with
  defined number of threads, typically half of the available CPU threads
- `-a ARCH` - ARCH is one of list: IA32, X64, ARM, AARCH64, RISCV64 or EBC

???

There is no need to provide those parameters every time as input to the build
command. Default parameters can be determined in `./Conf/target.txt`. Please
note the build command can also run just part of the build process by getting a
stage name as the final parameter. The most important stages are:

- all - go through all build stages, this is the default option if no other
  stage option is given
- fds - use platform FDF to build final binary fimage. Useful if we changed FDF
  file and we want to rebuild the final binary
- genc - autogenerate all necessary code files
- genmake - autogenerate all necessary makefile files
- clean - remove all binary products of the build process, but keep intermediate
autogenerated files
- cleanall - clean everything from the target platform directory

Despite all these possibilities, frequent building and code changes may get you
into a weird state, which gives hard to diagnose compilation errors. To resolve
that typically `cleanall` is required.

---

# Exercise #3: Build EDKII

1. Setup environment variables by running `source edksetup.sh`
2. Before we build EDKII we need to build edk2 build tools first:
   ```shell
   make -C /home/user/edk2/BaseTools/Source/C
   ```
3. Find DSC file for OVMF X64
4. Run the `build` process, setting all the most important command line
parameters (use `DEBUG` as `BUILDTARGET`)

Final result should look tell us how much space each Firmware Volume takes:

.small-code[
```shell
(...)


FV Space Information
SECFV [19%Full] 212992 (0x34000) total, 42432 (0xa5c0) used, 170560 (0x29a40) free
PEIFV [22%Full] 917504 (0xe0000) total, 210152 (0x334e8) used, 707352 (0xacb18) free
DXEFV [33%Full] 12582912 (0xc00000) total, 4212944 (0x4048d0) used, 8369968 (0x7fb730) free
FVMAIN_COMPACT [35%Full] 3440640 (0x348000) total, 1233928 (0x12d408) used, 2206712 (0x21abf8) free

- Done -
Build end time: 00:30:41, Apr.05 2022
Build total time: 00:01:17
```
]

---

# Exercise #3: Build EDKII

Our firmware image can be found in
`Build/OvmfX64/<BUILDTARGET_TOOLCHAIN>/FV/OVMF.fd`, where
`<BUILDTARGET_TOOLCHAIN>` is combination of target and toolchain that we
defined as `build` command parameters.

--

```shell
build -p OvmfPkg/OvmfPkgX64.dsc \
-b DEBUG \
-t GCC5 \
-n $(nproc) \
-a X64
```

---

# Quiz #2: True or False

** DEC describes what and how modules, libraries and components are to be built. **

--

- False

--

**DEC declares information about what is provided in the package**

--

- True

--

**IDF define the content and binary image layouts for firmware images, update capsules and PCI option ROMs**

--

- False

--

**DSC describe various modules binary, source and mixed, also intermediate product of EDKII build system**

--

- False

---

# Quiz #2: True or False

**UNI is used for mapping token names to localized strings**

--

- True

--

**FDF define the content and binary image layouts for firmware images, update capsules and PCI option ROMs**

--

- True

--

**DSC declares information about what is provided in the package**

--

- False

--

**FDF is the source code format that is used by developers to create a user interface with varying pieces of data or questions**

--

- False

---

# Quiz #2: True or False

**INF describe various modules binary, source and mixed, also intermediate product of EDKII build system**

--

- True

--

**VFR is used for mapping token names to localized strings**

--

- False

--

**VFR is the source code format that is used by developers to create a user interface with varying pieces of data or questions**

--

- True

--

---

# Exercise #4: Running OVMF.fd in QEMU

Then let's run the build in QEMU:

```shell
qemu-system-x86_64 -drive if=pflash,\
format=raw,file=Build/OvmfX64/DEBUG_GCC5/FV/OVMF.fd \
-nographic -net none
```

We added `-net none` so that the network stack would not load in QEMU, so that
the [PXE](https://en.wikipedia.org/wiki/Preboot_Execution_Environment)
network-based-boot would not wait while trying to receive something to boot
over the network (which we're not providing, so it would just wait a while
until timing out). We will continue to use `-net none` throughout this class to
speed up the boot to the EFI shell. But just remember to remove it later if
you're ever trying to do anything with networking from within QEMU after this
class.

Once the system is fully booted, we should be at a UEFI Shell command line
prompt. As a reminder, to leave QEMU you can use the special command listed in
the help, which can be found using `Ctrl-a h`.  We should see the UEFI shell,
you can leave QEMU with `Ctrl-a x`

---

# Exercise #4: Running OVMF.fd in QEMU

Next, build it again, but instead of changing parameters to build command use
`Conf/target.txt` file. If you modify `Conf/target.txt` correctly, the build
command should run without problems and build a fresh `OVMF.fd`.

---

# Quiz #3

**What should be the correct value of TOOL_CHAIN_TAG in Conf/target.txt.**

--

- GCC5

---

???

25% of material

- Building should take 5min, so we can discuss criticism and get back to
  running in QEMU after 2 next slides.

---

class: center, middle, intro

# Practice #2: EDKII Debugging

???

In Practice #1 we learned how to compile and run UEFI firmware for QEMU (OVMF).
In this section we will learn couple debugging methods:

* Debugging using EDKII logging features
* Debugging using EDKII debug code statement
* Debugging using GDB remote target connecting to EDK II Debug Agent
* Debugging using GDB remote target connecting to QEMU

---

# Practice #2: Add DebugLib to DSC file

If you were creating a completely new platform, you would need to add DebugLib
to the platform's DSC file in order for it to support debugging. However, since
we are using the existing `OvmfPkg/OvmfPkgX64.dsc` let's check how DebugLib was
added:

```shell
grep DebugLib OvmfPkg/OvmfPkgX64.dsc -C1
```

```shell
!ifdef $(DEBUG_ON_SERIAL_PORT)
  DebugLib|MdePkg/Library/BaseDebugLibSerialPort/BaseDebugLibSerialPort.inf
!else
  DebugLib|OvmfPkg/Library/PlatformDebugLibIoPort/PlatformRomDebugLibIoPort.inf
!endif
--
!ifdef $(DEBUG_ON_SERIAL_PORT)
  DebugLib|MdePkg/Library/BaseDebugLibSerialPort/BaseDebugLibSerialPort.inf
!else
  DebugLib|OvmfPkg/Library/PlatformDebugLibIoPort/PlatformDebugLibIoPort.inf
!endif
--
...

```

???

As we can see there are multiple libraries available in our platform
description file. If we look inside the DSC file we will find that
`BaseDebugLibSerialPort` is used when `DEBUG_ON_SERIAL_PORT` is defined. We can
define it at build time using `-D DEBUG_ON_SERIAL_PORT`, but before we do that
let's see what happens if `DEBUG_ON_SERIAL_PORT` is not defined. In the DSC we
see that `PlatformRomDebugLibIoPort` or `PlatformDebugLibIoPort` are used.
First just in SEC libraries and second with other UEFI phases libraries.

---

# Exercise #1: PlatformRomDebugLibIoPort

1. We can look for PlatformRomDebugLibIoPort.inf
2. In section Sources we can find DebugLib.c
3. Inside we can find the following functions calls
   `DebugPrint->DebugVPrint->DebugPrintMarker`
4. The last thing `DebugPrintMarker` performs is `IoWriteFifo8` to some PCD
   (Platform Configuration Databse) - PCD in this case is decoded using
   `PcdGet16()` function. Please note the parameter of that function as we will
   refer to it in next exercise as "I/O Debug Port".
5. Please find "I/O Debug Port" in OvmfPkg.dec declaration file - the default
   file for PCD definitions.
6. Knowing that hex value, let's run QEMU. Do not forget to replace `<I/O Debug
   Port>` with the correct value, and see if we can get any debug prints from
   SEC phase in our debug.log:

---

# Exercise #1: PlatformRomDebugLibIoPort

```shell
qemu-system-x86_64 -nographic -bios Build/OvmfX64/DEBUG_GCC5/FV/OVMF.fd \
-chardev file,path=debug.log,id=edk2-debug \
-device isa-debugcon,iobase=<I/O Debug Port>,chardev=edk2-debug -net none
```

At a basic level, you can think of PCDs like global variables that are set in
configuration files, and which can be looked up by code at runtime via
`GetPcd()`. This allows code to be more generic and perform correctly, regardless
of which architecture it is compiled for. To learn more about PCDs you can read
the [Intel white paper](https://www.intel.com/content/dam/www/public/us/en/documents/white-papers/edkii-platform-config-database-entries-paper.pdf).

???

PCD - Platform Configuration Database

Platforms often abstract configuration details associated with their platform
and behavior. These configuration settings can be classified as two types:
1. Build-time generated platform settings
2. Run-time generation platform settings

A PCD entry is a setting which is established during the time that the platform
BIOS/Boot-loader is built.

---

# Exercise #2: Debugging by printing

This is one of the easiest, popular and efficient way of debugging
firmware. Let's add debug log to SEC module. One of the first lines that we saw
in debug log from Exercise #1 should be `SecCoreStartupWithStack`. Let's find
the `C` file where this string occurs:

```diff
diff --git a/OvmfPkg/Sec/SecMain.c b/OvmfPkg/Sec/SecMain.c
index 2c5561661e..64ca73fbf1 100644
--- a/OvmfPkg/Sec/SecMain.c
+++ b/OvmfPkg/Sec/SecMain.c

@@ -808,6 +808,8 @@ SecCoreStartupWithStack (
     AsmEnableCache ();
   }

+  DEBUG ((DEBUG_INFO, "hello\n"));
+
   DEBUG ((
     DEBUG_INFO,
     "SecCoreStartupWithStack(0x%x, 0x%x)\n",
```

---

# Exercise #2: Debugging by printing

Then recompile edk2:

```shell
build -p OvmfPkg/OvmfPkgX64.dsc -b DEBUG \
-t GCC5 -n $(nproc) -a X64
```

And then Dump the logs to make sure your modification is present in the log.
```shell
qemu-system-x86_64 -nographic -bios Build/OvmfX64/DEBUG_GCC5/FV/OVMF.fd \
-chardev file,path=debug.log,id=edk2-debug \
-device isa-debugcon,iobase=<I/O Debug Port>,chardev=edk2-debug -net none
```

---

# Exercise #3: Debugging using GDB

Let's use QEMU feature to connect GDB and debug EDKII code. The first entry in
log should be `SecCoreStartupWithStack(...)`, add another debug message above
it. The function is located in `OvmfPkg/Sec/SecMain.c`. For example:

Run QEMU:

```shell
qemu-system-x86_64 -nographic -bios Build/OvmfX64/DEBUG_GCC5/FV/OVMF.fd \
-chardev file,path=debug.log,id=edk2-debug \
-device isa-debugcon,iobase=0x402,chardev=edk2-debug -s -S -net none
```

In another terminal, start GDB and set it up for Debugging:

```shell
gdb -q
```

Load SecMain.debug file to obtain addresses of `.text` and `.data` sections

```shell
(gdb) file Build/OvmfX64/DEBUG_GCC5/X64/SecMain.debug
```

---

# Exercise #3: Debugging using GDB

```shell
Reading symbols from Build/OvmfX64/DEBUG_GCC5/X64/SecMain.debug...
(gdb) info file
Symbols from "/home/user/edk2/Build/OvmfX64/DEBUG_GCC5/X64/SecMain.debug".
Local exec file:
        `/home/user/edk2/Build/OvmfX64/DEBUG_GCC5/X64/SecMain.debug', file type elf64-x86-64.
        Entry point: 0x64f0
        0x0000000000000240 - 0x00000000000088fe is .text
        0x0000000000008900 - 0x0000000000009a60 is .data
        0x0000000000009a80 - 0x0000000000009a80 is .eh_frame
Reading symbols from Build/OvmfX64/DEBUG_GCC5/X64/SecMain.debug...
```

* This gives us the offset for the .text section of the debug ELF file (0x240),
  which we will call `<SECMAIN_TEXT_OFFSET>`, and the offset for the .data
  section (0x8900), which we will call `<SECMAIN_DATA_OFFSET>` and use in a
  command below. These values will change as you change debug and build options
  throughout the class.  

???

---

# Exercise #3: Debugging using GDB

* You may now discard the symbol file, because we will need to reload it with
correct offsets

```shell
(gdb) symbol-file 
Discard symbol table from `/home/user/edk2/Build/OvmfX64/DEBUG_GCC5/X64/SecMain.debug'? (y or n) y
```

* In other terminal look in `Build/OvmfX64/DEBUG_GCC5/Ovmf.map` for base address of `SecMain`

```shell
user@user-OST-VM:~/edk2$ grep SecMain Build/OvmfX64/DEBUG_GCC5/Ovmf.map
SecMain (Fixed Flash Address, BaseAddress=<SECMAIN_BASEADDRESS>, EntryPoint=0x00fffd2584, Type=PE)
(IMAGE=/home/user/edk2/Build/OvmfX64/DEBUG_GCC5/X64/OvmfPkg/Sec/SecMain/DEBUG/SecMain.efi)
```
e.g. 
```shell
user@user-OST-VM:~/edk2$ grep SecMain Build/OvmfX64/DEBUG_GCC5/Ovmf.map
SecMain (Fixed Flash Address, BaseAddress=0x00fffcc094, EntryPoint=0x00fffd2584, Type=PE)
(IMAGE=/home/user/edk2/Build/OvmfX64/DEBUG_GCC5/X64/OvmfPkg/Sec/SecMain/DEBUG/SecMain.efi)
```

---

# Exercise #3: Debugging using GDB

* Reload symbols using correct `<SECMAIN_BASEADDRESS>`,
`<SECMAIN_TEXT_OFFSET>`, and `<SECMAIN_DATA_OFFSET>` found above. :

```shell
add-symbol-file Build/OvmfX64/DEBUG_GCC5/X64/SecMain.debug \
(<SECMAIN_BASEADDRESS>+<SECMAIN_TEXT_OFFSET>) \
-s .data (<SECMAIN_BASEADDRESS>+<SECMAIN_DATA_OFFSET>)
```

Which for the above example values would translate to the following (but make
sure to use your values, incase you compiled with different options and have
different offsets.):

```shell
(gdb) add-symbol-file Build/OvmfX64/DEBUG_GCC5/X64/SecMain.debug (0x00fffcc094+0x240) \
-s .data (0x00fffcc094+0x8900)
add symbol table from file "Build/OvmfX64/DEBUG_GCC5/X64/SecMain.debug" at
	.text_addr = 0xfffcc2d4
	.data_addr = 0xfffd4994
(y or n) y
Reading symbols from Build/OvmfX64/DEBUG_GCC5/X64/SecMain.debug...

```

---

# Exercise #3: Debugging using GDB

* You can now use symbolic breakpoints. Add a breakpoint at function
`SecCoreStartupWithStack`

```shell
(gdb) break SecCoreStartupWithStack
Breakpoint 1 at 0xfffd1b02: file /home/user/edk2/OvmfPkg/Sec/SecMain.c, line 734.
```

* Connect to target:

```
(gdb) target remote :1234
```

* Continue execution by `(gdb) c`
* Step to the debug print you added in Exercise #2
* (You can use `list` to show source code in gdb, and `break
SecMain.c:<Line_Number>"` to break on the appropriate line.)

---

# Exercise #4: Using ASSERT

ASSERT is another macro delivered by DebugLib to simplify debugging process by
providing additional information about location of problem.

EDKII uses many debug macros which you can read about in the
[documentation](https://edk2-docs.gitbook.io/edk-ii-uefi-driver-writer-s-guide/31_testing_and_debugging_uefi_drivers/readme.4).

Let's try ASSERT, by addingvthe following modification:

```diff
diff --git a/OvmfPkg/Sec/SecMain.c b/OvmfPkg/Sec/SecMain.c
index 2c5561661e..fec863836a 100644
--- a/OvmfPkg/Sec/SecMain.c
+++ b/OvmfPkg/Sec/SecMain.c
@@ -815,6 +815,7 @@ SecCoreStartupWithStack (
     (UINT32)(UINTN)TopOfCurrentStack
     ));

+  ASSERT (FALSE);
   //
   // Initialize floating point operating environment
   // to be compliant with UEFI spec.
```

---

# Exercise #4: Using ASSERT

Recompile the code and run it in QEMU
```shell
qemu-system-x86_64 -nographic -bios Build/OvmfX64/DEBUG_GCC5/FV/OVMF.fd \
-chardev file,path=debug.log,id=edk2-debug \
-device isa-debugcon,iobase=<I/O Debug Port>,chardev=edk2-debug -s -S -net none
```
In debug.log file you should find following:
```shell
ASSERT /home/user/edk2/OvmfPkg/Sec/SecMain.c(823): ((BOOLEAN)(0==1))
```

As you can see `ASSERT` macro provide source code file and line, as well as
information about the value of the condition which triggered ASSERT. Macro is
triggered when its parameter value is FALSE.

---

# Exercise #5: Release-build ASSERT behavior

ASSERTs can trick developers into making security mistakes. Specifically if a
developer inserts an ASSERT that is meant to check for some security-relevant
condition such as an invalid value. A developer might thing that the ASSERT
will still do the check in a release-build code, when in may not. In practice,
whether an ASSERT macro continues to function in release-build code, or whether
it's simply compiled-out, depends on the macro definition, and the compile
options. So developers used to one behavior in one codebase may expect that
behavior in another codebase, but it may not actually behave the way they
expect.

Test whether the ASSERT you inserted will trigger and prevent system boot in a
RELEASE build of the source code.

**Now remove the assert** to avoid problems with further exercises.

---

# Quiz #4

**Do ASSERT statements have any affect on RELEASE-built UEFI firmware?**

--

- No

---

# Exercise #6: Disable compiler optimization

Debugging of some code parts may be challenging because of compiler
optimizations, which are enabled by default. Compiler optimization can be
disabled in two ways:

* In `DSC` for whole package
* In `INF` file just for one module

First, let's disable compiler optimization in `INF`

Build RELEASE and check the size of `SecMain` module

```shell
build -p OvmfPkg/OvmfPkgX64.dsc -b RELEASE -t GCC5 -n $(nproc) -a X64 all
user@user-OST-VM:~/edk2$ ls -al Build/OvmfX64/RELEASE_GCC5/X64/SecMain.efi 
-rw-rw-r-- 1 user user 32128 maj 14 00:11 Build/OvmfX64/RELEASE_GCC5/X64/SecMain.efi
```
Now build DEBUG and check the size of SecMain module
```shell
build -p OvmfPkg/OvmfPkgX64.dsc -b DEBUG -t GCC5 -n $(nproc) -a X64 all
user@user-OST-VM:~/edk2$ ls -al Build/OvmfX64/DEBUG_GCC5/X64/SecMain.efi
-rw-rw-r-- 1 user user 39808 maj 14 00:08 Build/OvmfX64/DEBUG_GCC5/X64/SecMain.efi
```

---

# Exercise #6: Disable compiler optimization

Disable optimization in DEBUG build

```diff
diff --git a/OvmfPkg/Sec/SecMain.inf b/OvmfPkg/Sec/SecMain.inf
index 95cf0025e1..3c0af1d1ed 100644
--- a/OvmfPkg/Sec/SecMain.inf
+++ b/OvmfPkg/Sec/SecMain.inf
@@ -80,3 +80,6 @@

 [FeaturePcd]
   gUefiOvmfPkgTokenSpaceGuid.PcdSmmSmramRequire
+
+[BuildOptions]
+  GCC:*_*_*_CC_FLAGS   = -O0
```

Now rebuild the debug build.

```shell
build -p OvmfPkg/OvmfPkgX64.dsc -b DEBUG -t GCC5 -n $(nproc) -a X64 all
```

And view the new file size.

```shell
user@user-OST-VM:~/edk2$ ls -al Build/OvmfX64/DEBUG_GCC5/X64/SecMain.efi
-rw-rw-r-- 1 user user 45376 maj 14 00:17 Build/OvmfX64/DEBUG_GCC5/X64/SecMain.efi
```

---

# Exercise #6: Disable compiler optimization

What we can see is that DEBUG is bigger from RELEASE ~25%, but unoptimized debug
binary is 40% bigger. The compiler definitely uses shortcuts, that may cause
problems when debugging EDKII code. So if you face unexpected behavior during
debugging it is worth to consider disabling compiler optimization.

---

# Exercise #7: Disable optimization in DSC

In the case of modifying a DSC file, we disable optimization in the same way,
under the same section `[BuildOptions]`. But in `OvmfPkgX64.dsc` we already
have some flags for GCC RELEASE build.

```diff
diff --git a/OvmfPkg/OvmfPkgX64.dsc b/OvmfPkg/OvmfPkgX64.dsc
index 718399299f..fddbc1cf00 100644
--- a/OvmfPkg/OvmfPkgX64.dsc
+++ b/OvmfPkg/OvmfPkgX64.dsc
@@ -74,6 +74,7 @@

 [BuildOptions]
   GCC:RELEASE_*_*_CC_FLAGS             = -DMDEPKG_NDEBUG
+  GCC:DEBUG_*_*_CC_FLAGS               = -O0
   INTEL:RELEASE_*_*_CC_FLAGS           = /D MDEPKG_NDEBUG
   MSFT:RELEASE_*_*_CC_FLAGS            = /D MDEPKG_NDEBUG
 !if $(TOOL_CHAIN_TAG) != "XCODE5" && $(TOOL_CHAIN_TAG) != "CLANGPDB"
 !if $(TOOL_CHAIN_TAG) != "XCODE5" && $(TOOL_CHAIN_TAG) != "CLANGPDB"
```

---

# Exercise #7: Disable optimization in DSC

Please note the change in the wildcard (`*`) placement compared to the previous
INF example, and the fact that instead of having `*_*_*_CC_FLAGS` as in module
build options (which would apply to both RELEASE and DEBUG), we distinguish
RELEASE and DEBUG build flags.

```shell
build -p OvmfPkg/OvmfPkgX64.dsc -b DEBUG -t GCC5 -n $(nproc) -a X64 all
```

Note the new file size.

```shell
user@user-OST-VM:~/edk2$ ls -al Build/OvmfX64/DEBUG_GCC5/X64/SecMain.efi
-rw-rw-r-- 1 user user 74432 maj 14 00:28 Build/OvmfX64/DEBUG_GCC5/X64/SecMain.efi
```

The effect is that all code used in this package is built without optimization,
which means that binaries are even bigger since not only the module itself but
everything this module uses is built without optimization. The increase in size
in comparison to RELEASE is more then 2x.

???

Of course the above method can be used to add arbitrary defines or compiler
parameters either package-wide or module-wide.

---

# Exercise #8: Debug using Intel UDK Debugger

EDKII provides the SourceLevelDebug family of packages, which support an
alternative way of debugging EDKII source code.  The
[documentation](https://www.intel.com/content/dam/develop/external/us/en/documents/udk-debugger-tool-user-manual-v1-11-820238.pdf)
is outdated, but worth reading if you plan to use this features with real
hardware. There is also a presentation from UEFI forum
[here](https://uefi.org/sites/default/files/resources/EDK_II_SW_debugger_v0.1_lj-Plugfest.pdf).
Source level debugging features supported on Linux are:

* GDB support
* SEC debugging after temporary RAM initialization
* SMM debugging
* Debugging for a multiprocessor environment - i.e. an ability to debug code
  running not only the bootstrap processor (BSP) (i.e. the first processor code
  that comes up), but also on application processors (APs) (the subsequent cores
  that are initialized by the BSP.)

---

# Exercise #8: Debug using Intel UDK Debugger

Download, unpack and install:

```shell
wget https://downloadmirror.intel.com/674526/udk-debugger-tool-v1-5-1-linux.zip
unzip udk-debugger-tool-v1-5-1-linux.zip
chmod +x ./UDK_Debugger_Tool_v1_5_1.bin
sudo ./UDK_Debugger_Tool_v1_5_1.bin -i console
```
[Mirror copy of udk-debugger-tool-v1-5-1-linux.zip in case the above link breaks](https://gitlab.com/opensecuritytraining/arch4021-intro-uefi-additional-files/-/blob/master/udk-debugger-tool-v1-5-1-linux.zip)

* Follow instructions with default options until `Debug Port Channel`
  selection, for which you should choose `4` (TCP), and then provide
  server:`localhost` and port `1234`.
* After exiting the installer, if you examine `/etc/udkdebugger.conf`, the
  debug port section should contain the following:
  ```shell
  [Debug Port]
  Channel = TCP
  Port = 1234
  Server = localhost
  ```

---

# Exercise #9: Enable Source Level Debugging

* Modify the DSC file:

```diff
diff --git a/OvmfPkg/OvmfPkgX64.dsc b/OvmfPkg/OvmfPkgX64.dsc
index 718399299f..2b1fb75c05 100644
--- a/OvmfPkg/OvmfPkgX64.dsc
+++ b/OvmfPkg/OvmfPkgX64.dsc
@@ -31,7 +31,7 @@
   #
   DEFINE SECURE_BOOT_ENABLE      = FALSE
   DEFINE SMM_REQUIRE             = FALSE
-  DEFINE SOURCE_DEBUG_ENABLE     = FALSE
+  DEFINE SOURCE_DEBUG_ENABLE     = TRUE

 !include OvmfPkg/OvmfTpmDefines.dsc.inc
```

* Build

```shell
build -p OvmfPkg/OvmfPkgX64.dsc -b DEBUG -t GCC5 -n $(nproc) -a X64 all
```

---

# Exercise #9: Enable Source Level Debugging

* Run QEMU, please note serial parameter, which sets up a server to which Intel
UDK Debugger can connect

```shell
qemu-system-x86_64 -nographic -bios Build/OvmfX64/DEBUG_GCC5/FV/OVMF.fd \
-serial tcp:127.0.0.1:1234,server -net none
```

* Run UDK Debugger

```shell
user@user-OST-VM:~$ /opt/intel/udkdebugger/bin/udk-gdb-server
Intel(R) UEFI Development Kit Debugger Tool Version 1.5.1
Debugging through TCP (localhost:1234)
Redirect Target output to TCP port (20715)
Debug agent revision: 0.4
GdbServer on user-OST-VM is waiting for connection on port 1235
Connect with 'target remote user-OST-VM:1235'
```

---

# Exercise #9: Enable Source Level Debugging

.small-code[```shell
user@user-OST-VM:~$ gdb
(gdb) target remote :1235
(gdb) source /opt/intel/udkdebugger/script/udk_gdb_script 
##############################################################
# This GDB configuration file contains settings and scripts
# for debugging UDK firmware.
# WARNING: Setting pending breakpoints is NOT supported by the GDB!
##############################################################
Loading symbol for address: 0xfffd04fa
add symbol table from file "/home/user/edk2/Build/OvmfX64/DEBUG_GCC5/X64/OvmfPkg/Sec/SecMain/DEBUG/SecMain.dll" at
        .text_addr = 0xfffcc2d4
        .data_addr = 0xfffd8c14
(udb) bt
#0  CpuBreakpoint () at /home/user/edk2/MdePkg/Library/BaseLib/X64/GccInline.c:60
#1  PeCoffLoaderExtraActionCommon (ImageContext=0x81fb80, ImageContext@entry=0x0, Signature=1145130828)
    at /home/user/edk2/SourceLevelDebugPkg/Library/PeCoffExtraActionLibDebug/PeCoffExtraActionLib.c:144
#2  0x00000000fffd2aab in PeCoffLoaderRelocateImageExtraAction (ImageContext=0x81fb80) at
    /home/user/edk2/SourceLevelDebugPkg/Library/PeCoffExtraActionLibDebug/PeCoffExtraActionLib.c:213
#3  FindAndReportEntryPoints (PeiCoreEntryPoint=0x81fb78, BootFirmwareVolumePtr=<synthetic pointer>) at /home/user/edk2/OvmfPkg/Sec/SecMain.c:708
#4  SecStartupPhase2 (Context=0x81fd20) at /home/user/edk2/OvmfPkg/Sec/SecMain.c:915
#5  0x00000000fffd5861 in DebugPortInitialize (Function=0xfffd0ae7 <InitializeDebugAgentPhase2>, Context=0x81fce0)
    at /home/user/edk2/SourceLevelDebugPkg/Library/DebugCommunicationLibSerialPort/DebugCommunicationLibSerialPort.c:66
#6  InitializeDebugAgent (Function=0xfffd2404 <SecStartupPhase2>, Context=0x81fd20, InitFlag=1)
    at /home/user/edk2/SourceLevelDebugPkg/Library/DebugAgent/SecPeiDebugAgent/SecPeiDebugAgentLib.c:413
#7  SecCoreStartupWithStack (BootFv=<optimized out>, TopOfCurrentStack=<optimized out>) at /home/user/edk2/OvmfPkg/Sec/SecMain.c:885
#8  0x00000000fffd58d1 in _ModuleEntryPoint ()
#9  0x5aa55aa55aa55aa5 in jQuery22406169556978992354_1653477231226 ()
#10 0x5aa55aa55aa55aa5 in jQuery22406169556978992354_1653477231226 ()
#11 0x5aa55aa55aa55aa5 in jQuery22404820768266656186_1655765907187 ()
#12 0x5aa55aa55aa55aa5 in jQuery22409580249608885256_1655765982338 ()
#13 0x0000000000000000 in jQuery22406487671619152364_1655766019494 ()
```]

As you can see UDK script recognize paths, location and correctly loaded text and data section. It also can recognize backtrace to some extent.


---

# Design principles

.image-40[.center[![Image](images/uefi_overview.png)]]
- **_Reuse of existing table-based interfaces_**
    + ACPI, SMBIOS, MultiProcessor Table, Device Tree
    + Various network related tables (e.g. ARP)
    + new UEFI e.g. `EFI_MEMORY_ATTRIBUTES_TABLE`
- **_System partition_** - storage for software consumed during firmware-OS
  transition
- **_Boot Services_** - access to platform hardware resources by OS Loaders
- **_Runtime Services_** - access to platform hardware resources by OS
<br>.footnote[Graphics source: UEFI Specification 2.9]

???

Lots of tools and applications for nowadays firmware relies on these Runtime
Services to perform various modifications to its configuration.

---

# Design principles

- UEFI firmware has specific hierarchical structure:
    + Firmware device - typically SPI NOR Flash
    + Firmware Volumes - logical representation of firmware device
    + Full firmware image consists of at least one Firmware Volume (FV)
    + FVs consist of GUID-identified files
    + Files consist of sections (such as code sections — PE) - various types of
    files are recognized by UEFI specification
- For passing information from PEI to further phases Hand-Off-Blocks (HOBs) are
  used.
    + HOBs are binary data structures stacked together so that amount of passed
    information is flexible at run-time
    + HOB list is built in PEI (limited memory phase) and read-only input for DXE
    (main initialization phase). HOBs contain information on various
    input/output media: system memory, (MM)I/O resources, firmware devices and
    volumes
    + HOBs also contain information on boot path (the same firmware is run on
    power on, reboot and wake-up)

???

- We already saw how firmware is organized, so let's try to organize that
  knowledge little bit more.
- The format of HOBs in not regulated by any specification (except HOB headers
    + GUID plus length?).
- The data they hold can be of any format.
- The HOB consumer must know the HOB structure to consume it properly.
- There can be some exceptions as to ACPI tables HOB or SMBIOS tables HOB (the
  hold the pointers to the start of structures IIRC).
- Of course, we just scratching the surface speaking about all those data
  structures used by UEFI BIOS.

---

class: center, middle, intro

# Practice #3: Explore firmware hierarchical structures

???

In this practice we would like to explore UEFITool and optionally `UEFIExtract`
which can be used for the same purpose but from command line without using GUI.

---

# Exercise #1: Install UEFITool

Normally we would start with installing tools required for compiling and
cloning EDKII sources, but to save time and network bandwidth this is already
part of VM image. For reference, this is what is required:

.small-code[
```shell
wget https://github.com/LongSoft/UEFITool/releases/download/A62/UEFITool_NE_A62_linux_x86_64.zip
unzip UEFITool_NE_A62_linux_x86_64.zip
./UEFITool
```
]

This should open UEFITool GUI application leaving some warning about window
manager compatiblity. Warnings can safely ignore. Let's explore UEFITool.

???

Initial UEFITool description:
- File menu
  - most options are self-explenatory
  - GUID:
    - Load GUID database - can load custom database, loading GUIDs makes them
      easier to identify in user interface
    - Load default GUID database - load databse from guids.csv file in local
      directory, this file can be found in UEFITool repository and contain
      around 4k identified GUIDs, there are also other community databases
      which can help with exploration
    - Export discovered GUIDs - export all GUIDs with associated text to CSV file
- Action menu - options in this menu are accessible depending on the type of
  componene highlighted in "Structure section". Actions are accessible from
  main menu as well as from context menu (when clicking right on file).
    - there are some generic actions for whole image like searching at address,
      at give base, by GUID, by text occurance and more
    - most typical operations are hex view and extraction from image
    - other options give ability to:
      - extract just a body of component without a header
      - extract uncompressed
- View menu
    - gives ability to highlight Intel Boot Guard related ranges
    - red - Intel Boot Guard IBB protected range
    - cyan - other protected range
    - yellow - partially protected ranges
- Bottom are status windows which activate in certain use cases
    - Parser window show parser status: complaining about problems with image
      structure or that some structures in image were recognized e.g. IBG
    - FIT activates for images using Intel Firmware Image Table description
    - Security show Intel Bood Guard related informations, public keys, hashes,
      boot policy manifest and similar


---

# Exercise #2: UEFITool

* Let's identify the content of `OVMF.fd` used to boot our QEMU system in
  Practice #2 and find the most important elements of the image mentioned in
  the PI specification.
* In your home directory there should be `UEFITool`, we will use it to identify
  `OVMF.fd` contents used to boot our QEMU system and find the most important
  elements of the image mentioned in the PI specification.

???

- Structure section - show expandable tree-like hierarchical structure of UEFI
  firmware image
- Clicking on any component show details about it in Information window on
  the right
- Top component that we see is called UEFI image - which represent here
  content of whole Firmware Device (FD), that's why our file extension is .fd
- UEFITool recognize our image as 4MB UEFI image
- Typically at top level we see Firmware Volumes, there are three in our case
- NVRAM - used for storing UEFI variables which will be discussed later in
  this course
- and two Firmware Filesystem v2 (FFSv2)
- First at address 0x84000
- Second at address 0x3CC000
- Please note FVs can be built into other FVs.
- Both `6938079B-B503-4E3D-9D24-B28337A25806` and
  `7CB8BDC9-F8EB-4F34-AAEA-3EE4AF6516A1` are in
  `48DB5E17-707C-472D-91CD-1613E7EF51B0` FV.
- We can figure out what those FV consist of, by looking for GUId in
  `./OvmfPkg/OvmfPkgX64.fdf`
- Or by loading default GUIDs in UEFITool and checking what file names are
  inside
- In PEI FV we can find PEI modules consisting of various sections
- UEFITool recognizes special modules like PEI core, SEC core, or DXE core.
- In DXE FV we can find DXE drivers consisting of various sections
- If we look carefully we can find also applications like UiApp which is our
  BIOS Setup Menu starting when we hit hotkey while booting or type exit in
  UEFI Shell

---

# Exercise #3: Install UEFIExtract (optional)

Normally we would start with installing tools required for compiling and
cloning EDKII sources, but to save time and network bandwidth this is already
part of VM image. For reference, this is what is required:

.small-code[
```shell
wget https://github.com/LongSoft/UEFITool/releases/download/A62/UEFIExtract_NE_A62_linux_x86_64.zip
unzip UEFIExtract_NE_A62_linux_x86_64.zip
./UEFIExtract
```
]

This should show the usage help. Please read it before proceeding to the next
section.


---

# Exercise #4: Prepare text report (optional)

* Let's identify the content of `OVMF.fd` used to boot our QEMU system in
  Practice #2 and find the most important elements of the image mentioned in
  the PI specification.
* In your home directory there should be `UEFIExtract`, we will use it to
  identify `OVMF.fd` contents used to boot our QEMU system and find the most
  important elements of the image mentioned in the PI specification.

```shell
cd
./UEFIExtract edk2/Build/OvmfX64/DEBUG_GCC5/FV/OVMF.fd report
less edk2/Build/OvmfX64/DEBUG_GCC5/FV/OVMF.fd.report.txt
```

```shell
  Type  |    Subtype   |   Base   |   Size   |  CRC32   |   Name
Image   | UEFI         | 00000000 | 00400000 | 2FAE7549 |  UEFI image
Volume  | NVRAM        | 00000000 | 00084000 | 94DF64CC | - FFF12B8D-7696-4C8B-A985-2747075B4F50
```

* UEFIExtract recognizes our image as a 4MB UEFI image which starts with a NVRAM
  region used for keeping UEFI variables

---

# Exercise #4: Prepare text report (optional)

```shell
Volume   | FFSv2    | 00084000 | 00348000 | 18C9F4D1 | - 48DB5E17-707C-472D-91CD-1613E7EF51B0
```

* At address 0x84000 we have the first Firmware Volume with Firmware FileSystem
version 2. Please note FVs can be built into other FVs:

```shell
grep FFS  Build/OvmfX64/DEBUG_GCC5/FV/OVMF.fd.report.txt
Volume   | FFSv2    | 00084000 | 00348000 | 1CE44229 | - 48DB5E17-707C-472D-91CD-1613E7EF51B0
Volume   | FFSv2    |   N/A    | 000E0000 | BC1DBD3B | ----- 6938079B-B503-4E3D-9D24-B28337A25806
Volume   | FFSv2    |   N/A    | 00C00000 | D181FE90 | ----- 7CB8BDC9-F8EB-4F34-AAEA-3EE4AF6516A1
Volume   | FFSv2    | 003CC000 | 00034000 | C99D77AD | - 763BED0D-DE9F-48F5-81F1-3E90E1B1A015
```

* Both `6938079B-B503-4E3D-9D24-B28337A25806` and
`7CB8BDC9-F8EB-4F34-AAEA-3EE4AF6516A1` are in
`48DB5E17-707C-472D-91CD-1613E7EF51B0` FV.
* We can figure out what those FV consist of, by looking in
`./OvmfPkg/OvmfPkgX64.fdf`
    * `6938079B-B503-4E3D-9D24-B28337A25806` - PEI Firmware Volume
    * `7CB8BDC9-F8EB-4F34-AAEA-3EE4AF6516A1` - DXE Firmware Volume

---

# Exercise #4: Prepare text report (optional)

* In PEI FV we can find PEI modules consisting of various sections:

.small-code[

```shell
File     | PEI module      |  N/A   | 00014802 | 5214FCD6 | ------ 222C386D-5ABC-4FB4-B124-FBB82488ACF4 | PlatformPei
Section  | PEI dependency  |  N/A   | 00000016 | 99E93E7C | ------- PEI dependency section
Section  | Raw             |  N/A   | 00000024 | C8C93A23 | ------- Raw section
Section  | PE32 image      |  N/A   | 00014784 | 7EF25DB9 | ------- PE32 image section
Section  | UI              |  N/A   | 0000001C | F791D416 | ------- UI section
Section  | Version         |  N/A   | 0000000E | 80E5540A | ------- Version section
```

]

* UEFIExtract recognizes special modules like PEI core, SEC core, or DXE core.
In DXE FV we can find DXE drivers consisting of various sections:

.small-code[

```shell
File     | DXE driver      |  N/A   | 000092BA | 53FA25B4 | ------ 11A6EDF6-A9BE-426D-A6CC-B22FE51D9224 | PciHotPlugInitDxe
Section  | DXE dependency  |  N/A   | 00000028 | E001C4CA | ------- DXE dependency section
Section  | PE32 image      |  N/A   | 00009244 | 6E262CA0 | ------- PE32 image section
Section  | UI              |  N/A   | 00000028 | 0F47669C | ------- UI section
Section  | Version         |  N/A   | 0000000E | 80E5540A | ------- Version section
```

]

And applications:

.small-code[

```shell
File     | Application     |  N/A   | 000345EE | 843DD61F | ------ 462CAA21-7614-4503-836E-8AB6F4662331 | UiApp
Section  | PE32 image      |  N/A   | 00034584 | 4C3B0E1F | ------- PE32 image section
Section  | Raw             |  N/A   | 00000034 | D7939489 | ------- Raw section
Section  | UI              |  N/A   | 00000010 | AFB40D8A | ------- UI section
Section  | Version         |  N/A   | 0000000E | 80E5540A | ------- Version section
```

]

---

# Booting sequence overview

.image-70[.center[![Image](images/uefi_booting_sequence.png)]]

- Above diagram is simplified to explain what is provided by UEFI and what is
  considered value added by a firmware supply chain
<br>.footnote[Graphics source: UEFI Specification 2.9]

???

- Let's try to understand what is division of responsibility between various
  entities in UEFI BIOS software supply chain. At least in theory.
- UEFI Specification assume that all non-architectural components are
  responsibility of IBV and OEMs.
- UEFI Specification does not enforce how to implement EFI
  Driver/Application/Bootcode or OS Loader. It only say about interfaces EFI
  Binaries should implement to be compliant with UEFI Specification. So all
  code that was created to implement EFI Binaries functionality is considered
  value added.
- Please note that there are various ways to deliver EFI binaries e.g. they can
  be provided in PCI device OptionROM, typical for storage controllers with
  RAID capabilities
- From one perspective this diagram is important since it present mindset of
  UEFI Forum and people behind whole unified effort, but on the other side it
  may be hard to grasp by engineers who didn't grow up in that ecosystem.

---

# Boot Manager

- Boot Manager - responsible for loading various types of applications written
  according to specification. UEFI recognizes the following
  applications/binaries:
    + UEFI Images - class of files containing executable code, use subset of PE32+
    image format, those files are differentiated by subsystem and machine,
    machine parameter describes the processor architecture of the executable,
    subsystem describes if the executable is an application, Boot Service, or
    Runtime Service
    + UEFI Applications - loaded by Boot Manager or other UEFI applications, may
    perform `EXIT_BOOT_SERVICES.Exit()`
    + UEFI OS Loaders - special type of UEFI Application that take over platform
    control and passes it to OS
    + UEFI Drivers - there are two types of drivers: boot service drivers and
    Runtime Service drivers
- Boot Manager - understands how to load application and provide required
  configuration data

.footnote[Source: UEFI Specification 2.9 (March 2021) chapter 2.1 Boot Manager]

???

Source: UEFI Specification 2.9 (March 2021) chapter 2.1 Boot Manager

If we plan to develop UEFI compatible images it is good to know there various
types. Here we explain generic types, but to understand better you have to
study INF file specification where you can decide precisely about module type
you develop.

Above types caused a lot of confusion and implementation problems, since
behavior of UEFI applications is not consistent in context of UEFI Exit Boot
Services call. Responsibility of OS Loader, which these days are integrated
into OS, is not clear.

---

# Firmware core

- Services are accessed through pointers to global functions, those pointers
  are located in EFI System Table.
- Boot Services
    + Event, Time, and Task Priority Functions
    + Memory allocation
    + Protocol handling - API installation on given handle
    + Image Services - image lookup, load and execution
    + Miscellaneous: watchdog, mem set/copy, stall, monotonic counter, CRC32 etc.
- Runtime Services
    + Variable Services - get/set variables
    + Time Services
    + Virtual Memory Services - deals with conversion between physical and
    virtual addressing
    + Miscellaneous: capsule update, reset, monotonic counter

???

- When transitioning to x64 OS EFI System Table pointer is passed through RDX.
- In case of 32bit OS it would be top of stack.
- Obviously way EFI table is passed depends on architecture.

---

# Calling convention

- All functions defined in the UEFI specification are called through pointers,
  via common architecturally-defined calling conventions found in C compilers
- Pointers to global functions (Runtime and Boot Services) are located via EFI
  System Table and relevant service tables
- Pointers to other functions are obtained dynamically through device handles
- All pointers are prepended with the word `EFIAPI`
- UEFI uses its own data types definitions (UINTN, UINT8 etc.)

---

# Calling convention

- When passing pointers as arguments to a function
    + It should always point to physical memory
    + It should be correctly aligned
    + It should not pass `NULL` unless it is allowed
    + There should be no assumption about the state of the pointer when the
    function call returns an error
    + Structures bigger than 4 bytes (on 32bit processor) and 8 bytes (on 64 bit
    processor) should be passed by pointer
- UEFI uses its own data types definitions (UINTN, UINT8 etc.)

---

# Protocols

.image-99[.center[![Image](images/uefi_protocol.png)]]

- A Protocol is a collection of related functions and data
- A Protocol has a GUID, an interface structure, and can use Protocol Services.
  It can be used only at boot time
- UEFI 2.9 define over 60 protocols e.g. `EFI_PCI_IO_PROTOCOL`, `EFI_SERIAL_IO_PROTOCOL`

???

- Handle is empyt object with a number, kept in global Handles Database.
- Handle is used as container of various Protocols, those protocols give
  meaning and role to handle, so it can become:
    + UEFI driver or application
    + or controller (we will discuss that later)
- Handles Database is used to keep track of what protocols where installed and
  where.

Essentially API installed on given object to gain some capability e.g.
communication using some bus protocol.

<https://excalidraw.com/#json=ds7DY1Sy0c_Oz89aJ7ejJ,XrWHeDESDz7WW5asOywa_g>

---

# UEFI Driver Model

.image-60[.center[![Image](images/uefi_system_view.png)]]

- UEFI views the system as a set of one or more processors connected to one or
  more core chipsets
- Core chipsets are responsible for producing one or more I/O buses
- The UEFI driver model describes buses, not processors or chipsets
- In other words: it is a tree of buses and devices with the core chipset as a
  root of the tree
<br>.footnote[Graphics source: UEFI Specification 2.9]

???

- Adding controller potentially means adding new bus with new devices.

It is important to understand specification author's point of view.
Implementation of controller and device drivers is our of scope, as well as and
problems related with those drivers.

Problems with buses are in scope.

---

# Quiz #5

**How do we access UEFI services?**

--

- Pointers to global functions (Runtime and Boot Services) are located via
  EFI System Table and relevant service tables

--

**What is the difference between UEFI and EDKII?**

--

- UEFI is OS-firmware communication specification
- EDKII is an implementation of UEFI and PI

---

**What was the reason to create UEFI (3 reasons)?**

--

- Obsolete and hard to develop BIOS
- Completely new CPU architecture to support (Itanium)
- Unmaintainable assembly code

--

**What does UEFI provide compared to EFI to improve security?**

- Secure Boot
- Driver signing
- Crypto material and key management

--

**Does UEFI cover the whole boot process?**

--

- No, it covers only the interface between firmware and OS, DXE

---

**How is code organized in UEFI?**

--

- Modules contain drivers
- Modules are grouped into packages
- Modules communicate using protocols

--

**What are the properties of UEFI considered issues by some critics?**

--

- Too complex, overengineered
- Many vulnerable implementations

--

**What are HOBs?**

--

- Hand-off Blocks, binary structures containing information on the environment.
HOB list is created in PEI and is input for DXE

---
class: center, middle, intro

# UEFI Boot Flow

???

50% of material

---

# Security phase (SEC)

.image-80[.center[![Image](images/sec.jpg)]]
.footnote[Source: <https://github.com/tianocore/tianocore.github.io/wiki/PI-Boot-Flow>]

---

# Security phase (SEC)

- Described in PI specification, chapter 13
- UEFI root of trust - SEC may authenticate PEI Foundation
- It can be additionally hardened by verifying SEC, e.g. with Intel Boot Guard,
  AMD Hardware Validated Boot
- It starts at reset vector so it includes the most basic initialization such as
  entering protected mode, loading microcode (on x86), and Temporary RAM (a.k.a.
  Cache As RAM)
- It is expected to pass the following information to PEI Foundation:
    + State of the platform
    + Location and size of the Boot Firmware Volume (BFV)
    + Location and size of the temporary RAM
    + Location and size of the stack
    + Optionally, one or more HOBs via the EFI_SEC_HOB_DATA_PPI.
- Besides this, it may pass any number of HOBs to be installed
- Not all CPUs can fill all those requirements e.g. [UEFI for Allwinner A13](https://blog.3mdeb.com/2022/2022-01-18-porting_edk2_to_a13_tablet/).

???

## Common SEC related misconception

- Among people familiar to some extent with UEFI there, it is common to hear
that "SEC is named after security, but it does not do anything to do with it,".
This is presumably due to EDKII having no code to do integrity checks in SEC.
- It depends on whether we are talking about theory (specification) or practice
(implementation). We cannot say much about what happens in practice (due to
closed-source implementations). We can primarily talk about things related to
the open source EDKII code, which does not implement any authentication of PEI
modules from the SecCore code. But we can imagine some implementations from
Independent BIOS Vendors which implement authentication in SEC.

If we rely on theory, the PI spec says in section 13.2.3:

"Finally, the Security (SEC) phase represents the root of trust in the system.
Any inductive security design in which the integrity of the subsequent module
to gain control is corroborated by the caller must have a root, or "first,"
component. For any PI Architecture deployment, the SEC phase represents the
initial code that takes control of the system. As such, a platform or
technology deployment may choose to authenticate the PEI Foundation from the
SEC phase before invoking the PEI Foundation."

- From that point, SEC should contain a chain of trust continuation even though
the UEFI reference implementation EDKII does not implement it.
- Not implementing PEI authentication in the SEC phase may be because one of
the most popular protection mechanisms, Intel Boot Guard, covers both SEC and
PEI Foundation.
- There is no limit to what SEC can authenticate theoretically. But in reality
it doesn't authenticate whole PEI, and PEI Foundation typically also has no
capabilities in the dispatcher to verify the PEIMs. That is why BootGuard or
HVB verifies whole SEC and PEI (including PEIMs).

> When there are multiple Firmware Volumes (FVs), what additional code is
required?

> The PI PEI core foundation already has a mechanism for handling multiple FVs.
> Multiple FVs is platform specific and would be described in a platform’s FDF
> file.

> A platform specific PEIM has to declare the location of additional FVs for
> them to be known to the PEI Core. Once they are declared to the PEI Core, the
> PEI Core can dispatch PEIMs from them. The only FV that is known to the PEI
> Core when it is started is the Boot Firmware Volume (BFV). The BFV contains
> SEC and the PEI Core and one or more PEIMs.

<https://github.com/tianocore/tianocore.github.io/wiki/UEFI-PI_FAQ>

---

# Pre-EFI Initialization (PEI)

.image-80[.center[![Image](images/pei.jpg)]]
.footnote[Source: <https://github.com/tianocore/tianocore.github.io/wiki/PI-Boot-Flow>]

---

# Pre-EFI Initialization (PEI)

- PEI is the main subject of the PI specification. The specification defines the following
PEI goals:
    + Maintain chain of trust consistency
    + Perform early initialization
    + DRAM initialization
    + Prepare HOB data for DXE and pass control to it
- Its structure resembles DXE, but must cope with very limited memory, that's
why we want to move on as fast as possible
- Memory initialization is one of the most complex parts of the boot process
    + In modern x86 code to perform it is provided as a binary component by the
  silicon vendor or even offloaded to a closed source peripheral processor
    + Silicon vendors typically implement memory initialization closely
    cooperating with memory vendors and standardization bodies like JEDEC

???

Why do we even need memory initialization?
- to train buses to achieve maximum bitrate/performance numbers

---

# PEI structure

- PEI consists of the following elements:
    + PEI Foundation — general code managing PEI execution and transition to DXE
    + PEI Modules (PEIMs) — single-purpose, interchangeable pieces of
  initialization code
    + PEIM-to-PEIM Interfaces (PPIs) — providing communication between modules
    + PEI Dispatcher — a state machine implemented in PEI Foundation iterating
  over available PEIMs and running them
    + PEI Services — set of core operations used in PEIMs
    + Dependency Expressions (DEPEX) — each PEIM has a set of PPI GUIDs that must
  be loaded before itself

---

# PEI services

- Framework services:
    + Load/install PPIs
    + Append HOBs
    + Progress/state reporting
- ACPI related services:
    + Power on/reboot/wake-up detection
    + Triggering reset
- Firmware volume services
- Memory management services

---

# PEI to DXE Handoff

- PEI finalization is described in chapter 9 of PI specification
- Before DXE is started some things must be done:
    + DXE Foundation code must be located
    + All components needed by it must be loaded to the memory
    + HOBs list must be passed to  `DXE Initial Program Load (IPL) PPI`
    which finally starts DXE phase
- There are some HOBs required in specification needed by DXE foundation:
    + One or more Resource Descriptors with information on physical memory
    + Bootstrap CPU stack location
    + Firmware Volumes & devices

There may be additional platform-specific HOB parameters

---

# PEIM interface

``` c
[Defines]
  INF_VERSION                    = 0x00010005
  BASE_NAME                      = PiSmmCommunicationPei
  MODULE_UNI_FILE                = PiSmmCommunicationPei.uni
  FILE_GUID                      = 1C8B7F78-1699-40e6-AF33-9B995D16B043
  MODULE_TYPE                    = PEIM
  VERSION_STRING                 = 1.0
  ENTRY_POINT                    = PiSmmCommunicationPeiEntryPoint
```

.code-comment[UefiCpuPkg/PiSmmCommunication/PiSmmCommunicationPei.inf]

``` c
EFI_STATUS
EFIAPI
PiSmmCommunicationPeiEntryPoint (
  IN EFI_PEI_FILE_HANDLE       FileHandle,
  IN CONST EFI_PEI_SERVICES    **PeiServices
  )
```

.code-comment[UefiCpuPkg/PiSmmCommunication/PiSmmCommunicationPei.c]

- File handle is meant only to pass to services (`typedef void*`)

???

This is how PEIM entry point is defined.

---

# Driver eXecution Environment (DXE)

.image-80[.center[![Image](images/dxe.jpg)]]
.footnote[Source: <https://github.com/tianocore/tianocore.github.io/wiki/PI-Boot-Flow>]

---

# Driver eXecution Environment (DXE)

- Specified in PI specification Volume 2
- When we finally have initialized memory, we can start devices initialization
- DXE provides a common interface for all drivers, DXE Dispatcher loads all of
  them
- This is practically the same as PEI, but provides a full execution
  environment, with full address space in DRAM and much more services
    + So in the entry point function, we find `EFI_PEI_FILE_HANDLE` and
    `EFI_SYSTEM_TABLE`
- Also naming is a little bit different: DXE Foundation, DXE Dispatcher, DXE
  Drivers, DXE Architectural Protocols

---

# DXE Protocols

.image-80[.center[![Image](images/dxe-protocols.png)]]

- Precise set of architectural protocols changes from version to version

???

Phase Handoff Information Table (PHIT) HOB - first and required HOB, contains
boot mode and description of memory region used by PEI

---

# DXE Services

- Thanks to full memory range we can have much more variety of services:
    + Task priority management
    + Memory allocation, retrieving memory map
    + Events + timers
    + Protocol management services
    + Image services — for loading PE/COFF executable images
    + Driver management services — (un)registering drivers

---

# during DXE… (x86 example)

- Most of the important initialization happens here
- PCI enumeration
- SMM initialization:
    + SMM IPL is a driver which loads SMMCore
    + SMM code is loaded to SMRAM and executed whenever SMI is caught
    + Locking SMRAM (we need to protect from adversaries)
- MSR configuration
    + This is very model specific
    + SMRR & MTRR (System Management / Memory Type Range Registers) — used for
    memory management and caching configuration

---

# Boot Device Selection (BDS)

.image-80[.center[![Image](images/bds.jpg)]]
.footnote[Source: <https://github.com/tianocore/tianocore.github.io/wiki/PI-Boot-Flow>]

---

# Boot Device Selection (BDS)

- Boot Manager selects boot media based on configuration
- Boot media contain OS Loader in PE+ Image format (typically `*.efi` file)
- Its configuration may be managed using UEFI variables available from OS or
  from UEFI Setup/Configuration Menu
- By default, BDS looks for `efi/boot/boot<arch>.efi` (e.g. `bootx64.efi`) on
  all bootable partitions (usually FAT32). First, such a file is loaded
- Usually in this phase `BootServices.ExitBootServices()` is called. This is
  the point when BootSevices aren't available anymore
- As we mentioned earlier there is some responsibility confusion, because most
  OS tend to integrate OS Loader, which means that having a bootloader like
  GRUB2, SeaBIOS or iPXE may cause technical difficulties

???

PE+ in contrast to PE32 supports 64 and 32 bit platforms

---

class: center, middle, intro

# Practice #4: Boot log overview

---

# Exercise #1: UEFI boot phases

* In following exercise we will find where each UEFI boot phase starts.
* Let's make sure we have:
  * Source Level Debugging enabled (please check Practice #2)
  * Gathered whole `debug.log` from boot process
  * Run QEMU
    ```shell
    qemu-system-x86_64 -nographic -bios Build/OvmfX64/DEBUG_GCC5/FV/OVMF.fd \
    -chardev file,path=debug.log,id=edk2-debug -device \
    isa-debugcon,iobase=0x402,chardev=edk2-debug -net none
    ```
  * Wait until `Shell>` and exit `Ctrl-a x`.
  * Debug log should be in known file `debug.log`
* Grep for case insensitive `SEC`, `PEI`, `DXE` and `BDS`

---

# Exercise #1: UEFI boot phases - SEC

* There is only one entry talking about SEC phase, shown below. The other
  entries are not related to the UEFI phase:

  ```shell
  SecCoreStartupWithStack(0xFFFCC000, 0x820000)
  ```

* Please note the above log entry, so we can explore the C code related to it
  in further exercises in this lab.

---

# Exercise #1: UEFI boot phases - PEI

* When looking for PEI we get way more occurances, including a lot of PEIM
  loading, PPI notifications, and discovery of volumes.
* The first occurance of Pei is in following log:

  ```shell
  DiscoverPeimsAndOrderWithApriori(): Found 0x7 PEI FFS files in the 0th FV
  ```

* But, is it really the PEI phase start? We will try to figure this out in
  the next exercise.

---

# Exercise #1: UEFI boot phases - DXE

* The first occurance is loading of the DXE IPL (Initial Program Loader), which prints the following log:

```shell
DXE IPL Entry
```

---

# Exercise #1: UEFI boot phases - BDS

* The first occurance is loading of the BDS DXE driver, which prints the following log:

```shell
[Bds] Entry...
```
---

# Exercise #2: Break in SEC

* Return to the `OvmfPkg/Sec/SecMain.c` file that you added a debug statement
  to in `Practice #2: EDK II Debugging -> Exercise #2: Debugging by printing`
* Search in this file to find the place where control is handed over from SEC
  to PEI core.
* Use the steps from `Practice #2: EDK II Debugging -> Exercise #3: Debugging
  using GDB` to break at the beginning of the function where control is handed
  off from SEC to PEI.
  * Feel free to use **tui enable** in gdb since code comments may help identify place
    where control is transferred.
* Please write down the type of the function pointer called during the hand off
  from SEC to PEI (we will refer to it as `<SEC_TO_PEI_FPTR_TYPE>`)
* If you continue step into the PEI core, GDB will not be able to determine the
  source code it is associated with, because of that we have to load the
  appropriate symbols.

???

First we will break in SEC at previous logging point and prepare to step
through to the PEI hand-off.

---

# Exercise #3: Load PEI core symbols

* Search for the location where the function pointer type
`<SEC_TO_PEI_FPTR_TYPE>` is *defined*, not just *used*.
  ```shell
  grep -r <SEC_TO_PEI_FPTR_TYPE>
  ```
* From this you should find a header file that has the definition. Translating
  that function pointer 
* But what is the actual target of that function pointer in our case? Here the
  complexity of EDKII and UEFI is added to the typical complexity of trying to
  follow indirect control flow through function pointers in C codebases.

???

Load PEI core symbols by finding information in build output files

---

# Exercise #3: Load PEI core symbols

* Rather than search through every usage of the function pointer, let's
  instead focus on the parameters that the function must take, as described in
  the definition we've already found. There are two of them. And we can use the
  following advanced grep magic to find other locations where those parameters
  occur right next to each other (as we would expect they would in the target
  function which implements the same interface as the function pointer.)
  ```shell
  grep "IN CONST <FIRST_PARAM_TYPE>" --include=\*.c . -r -A 1 \
  --exclude-dir=Build|grep "IN CONST <SECOND_PARAM_TYPE>" -B 1
  ```
* Based on this you should have found a single file that has the same two
  parameters that are used in the `<SEC_TO_PEI_FPTR_TYPE>` function pointer.

???

* To explain the above, it says: please look for `<FIRST_PARAM_TYPE>`
  recursively (`-r`) in all C files only (`--include=\*.c` (so this excludes our
  definition in the header that we already found)), except the Build directory
  (`--exclude-dir=Build`) and show one line after the occurrence. Then we pipe
  the result and grep the output buffer looking for occurrences of
  `<SECOND_PARAM_TYPE>`, which should happen in the subsequent line, and when
  found, print the occurrence and one line before.

---

# Exercise #3: Load PEI core symbols

* Now let's find what the `BASE_NAME` is according to the INF file that is used
  for compilation of code this. The relevant INF file is typically found in the
  same directory or one level above. But how would we know we are looking at the
  correct INF file? You should see that it contains our C source code file name
  in the `[Sources]` section.
* Finally we will look for the `BASE_NAME` of this module in map files, to find
  the BaseAddress where it is loaded in memory, which can be used to load debug
  the debug binary
  ```shell
  grep <BASE_NAME> --include=\*.map . -rw|grep BaseAddress
  ```
* We are a little bit lucky in the end, because we dealing with a core UEFI
  phase module, not some random driver. That's why it is visible in a map file.
* We are little bit lucky at the end, because we dealing with a core UEFI phase
  module, not some random driver. That's why it is visible in a map file.

---

# Exercise #3: Load PEI core symbols

* Use the steps you learned before in `Practice #2: EDK II Debugging ->
  Exercise #3: Debugging using GDB`, to load the debug symbols for the
  `<BASE_NAME>.debug` file.
* Step from the SecMain.c code into the PEI core code.

---

# Exercise #4: Jump to PEI

* Please check `./Build/OvmfX64/DEBUG_GCC5/Ovmf.map` to find PEI base address,
  so we can load into open session new file and symbols
* Let's start QEMU:
  ```shell
	qemu-system-x86_64 -nographic -bios Build/OvmfX64/DEBUG_GCC5/FV/OVMF.fd \
  -chardev file,path=debug.log,id=edk2-debug -device \
  isa-debugcon,iobase=0x402,chardev=edk2-debug -net none -s -S
  ```
---

# Exercise #4: Jump to PEI

* Let's start GDB and load symbols:

```shell
add-symbol-file Build/OvmfX64/DEBUG_GCC5/X64/SecMain.debug (0x00fffcc094+0x0000000000000240) \
-s .data (0x00fffcc094+0x00000000000088c0)
add-symbol-file Build/OvmfX64/DEBUG_GCC5/X64/PeiCore.debug (0x0000820140+0x0000000000000240) \
-s .data (0x0000820140+0x000000000000af40)
target remote :1234
break SecStartupPhase2
```

* Please note above addresses could change, if you using different code base or
  different compilation parameters.
* Step into (gdb `s`) `PeiCoreEntryPoint` and you are in PEI phase

---

# Exercise #5: Review PEI debug.log

* Continue debug session from Exercise #6 by stepping in (gdb `si`)
  `ProcessModuleEntryPointList` and then you can step over (gdb `n`) steps in
  `PeiCore` function.
* Please note that first PPI (PEIM to PEIM Interfaces) are installed together
  with some PPI Notifies, when PEI Core Service completes its initialization by
  calling `Initialize{SecurityServices,DispatcherData,ImageServices}`, as is
  indicated in debug.log.

---

# Exercise #5: Review PEI debug.log

* We can continue to `PeiDipatcher`, which is responsible for discovering and
  loading PEIMs (PEI Modules). It is worth to step into this function and go
  through the code more carefully.
	* `DiscoverPeimsAndOrderWithApriori` checks which PEIMs should be loaded
    before any other, which essentially determine the order of PEIMs.
	* Then dispatcher starts loading various PEIMs, starting with `PcdPeim`.
    Loading a PEIM may mean installation of additional PPIs and registration of
    associated Notifies.
	* The fourth PEIM is `PlatformPei` its logging is verbose because it handles:
    CMOS dumping, ACPI S3 detection and verification, various initialization
    related to memory and callbacks installation.

---

# Exercise #5: Review PEI debug.log

* `PlatformPei` leads to temporary RAM migration, which changes the way we load
  debug symbols.
* We should see log as follows:
```shell
TemporaryRamMigration(0x810000, 0x3F4E000, 0x10000)
Loading PEIM 52C05B14-0B98-496C-BC3B-04B50211D680
Loading PEIM at 0x00007EE8000 EntryPoint=0x00007EEFD0F PeiCore.efi
(...)
```

---

# Exercise #5: Review PEI debug.log

* From previous analysis we know that `.text` section of `PeiCore` is `0x240`
  and `.data` is `0xaf40`, to apply changes in debugging session we should do as
  follows

```shell
Breakpoint 6, PeiCore (SecCoreDataPtr=SecCoreDataPtr@entry=0x3f55d20, 
PpiList=PpiList@entry=0x0, Data=Data@entry=0x3f55628) at 
/home/user/edk2/MdeModulePkg/Core/Pei/PeiMain/PeiMain.c:169
(gdb) n
(gdb) si
0x0000000007eee4fd in jQuery22402537741639935416_1653953332175 ()
(gdb) symbol-file Build/OvmfX64/DEBUG_GCC5/X64/PeiCore.debug
Load new symbol table from "Build/OvmfX64/DEBUG_GCC5/X64/PeiCore.debug"? (y or n) y
Reading symbols from Build/OvmfX64/DEBUG_GCC5/X64/PeiCore.debug...
(gdb) add-symbol-file Build/OvmfX64/DEBUG_GCC5/X64/PeiCore.debug \
(0x00007EE8000+0x0000000000000240) -s .data (0x00007EE8000+0x000000000000af40)
add symbol table from file "Build/OvmfX64/DEBUG_GCC5/X64/PeiCore.debug" at
        .text_addr = 0x7ee8240
        .data_addr = 0x7ef2f40
(y or n) y
Reading symbols from Build/OvmfX64/DEBUG_GCC5/X64/PeiCore.debug...
```

---

# Exercise #5: Review PEI debug.log

* To continue debugging we have to do the same for `DxeIpl`

.small-code[
```shell
(gdb) symbol-file Build/OvmfX64/DEBUG_GCC5/X64/DxeIpl.debug
Load new symbol table from "Build/OvmfX64/DEBUG_GCC5/X64/DxeIpl.debug"? (y or n) y
Reading symbols from Build/OvmfX64/DEBUG_GCC5/X64/DxeIpl.debug...
(gdb) add-symbol-file Build/OvmfX64/DEBUG_GCC5/X64/DxeIpl.debug \
(0x00007EDE000+0x0000000000000240) -s .data (0x00007EDE000+0x0000000000004240)
add symbol table from file "Build/OvmfX64/DEBUG_GCC5/X64/DxeIpl.debug" at
        .text_addr = 0x7ede240
        .data_addr = 0x7ee2240
(y or n) y
```
]

* After that we are ready to start transition to DXE by using DXE IPL PPI (through `DxeIpl`, we can step to the point where `DXE IPL Entry`) string is printed in `debug.log` and then step into `TempPtr.DxeIpl->Entry` function
* If everything was loaded correctly we should land in `DxeLoadCore`, where first boot mode is analyzed and appropriate actions are taken
* Then `DxeLoadCore` look for DXE Core file and after loading it calls `HandOffToDxeCore` which prepare final switch to DXE.
* Since switching to DXE is not so trivial to observe we will describe it in DXE related exercise.

---

# Exercise #6: Welcome to DXE

* Based on exercise #7 we are ready to jump into DXE phase
* Let's start QEMU:
  ```shell
  qemu-system-x86_64 -nographic -bios Build/OvmfX64/DEBUG_GCC5/FV/OVMF.fd \
  -chardev file,path=debug.log,id=edk2-debug -device \
  isa-debugcon,iobase=0x402,chardev=edk2-debug -net none -s -S
  ```
* Let's start GDB and load symbols:
  ```shell
  add-symbol-file Build/OvmfX64/DEBUG_GCC5/X64/PeiCore.debug \
  (0x00007EE8000+0x0000000000000240) -s .data (0x00007EE8000+0x000000000000af40)
  add-symbol-file Build/OvmfX64/DEBUG_GCC5/X64/DxeIpl.debug \
  (0x00007EDE000+0x0000000000000240) -s .data (0x00007EDE000+0x0000000000004240)
  add-symbol-file Build/OvmfX64/DEBUG_GCC5/X64/DxeCore.debug \
  (0x00007E9E000+0x0000000000000240) -s .data (0x00007E9E000+0x0000000000021e00)
  target remote :1234
  ```

---

# Exercise #6: Welcome to DXE

* Following commands have to be executed manually.
  ```shell
  break PeiCore
  c
  break HandOffToDxeCore
  c
  ```
* Interestingly second break point is not always set, when things happen too
  fast, sometimes GDB claims it does not know the symbol in following way:
  ```shell
  (gdb) break HandOffToDxeCore
  Function "HandOffToDxeCore" not defined.
  Make breakpoint pending on future shared library load? (y or [n]) 
  ```

---

# Exercise #6: Welcome to DXE

* It probably means things happen to fast or GDB has some weird bug with
  looking for symbols, workaround that works for me most of the time  is using
  Tab for symbol searching in between continue commands:

```shell
(gdb) break Pei<TAB>
PeiAllocatePages       
PeiAllocatePool        
PeiCheckAndSwitchStack 
PeiCore                
PeiCoreEntry           
PeiCoreEntryPoint.c    
...
(gdb) break PeiCore
Breakpoint 1 at 0x7eee4fd: file
/home/user/edk2/MdeModulePkg/Core/Pei/PeiMain/PeiMain.c, line 169.
(gdb) c
Continuing.
Breakpoint 1, PeiCore (SecCoreDataPtr=0x3f55d20, PpiList=0x0, Data=0x3f55628)
at /home/user/edk2/MdeModulePkg/Core/Pei/PeiMain/PeiMain.c:169
169     {
(gdb) break Hand<TAB>
Handle.c                 HandoffInformationTable  
(gdb) break HandOffToDxeCore
Breakpoint 2 at 0x7ee05ce: file /home/user/edk2/MdeModulePkg/Core/DxeIplPeim/DxeLoad.c, line 450.
(gdb) c
```

---

# Exercise #6: Welcome to DXE

* Please note above addresses could change, if you using different code base or
  different compilation parameters.
* Set breakpoint for `SwitchStack` function since stepping through
  `HandOffToDxeCore`, especially since areas dealing with page tables or CR3
  breaks debugging sessions.

```shell
(gdb) break SwitchStack
Breakpoint 3 at 0x7edfd0e: SwitchStack. (2 locations)
(gdb) c
Continuing.

Breakpoint 3, SwitchStack (EntryPoint=EntryPoint@entry=0x7ea0ef0
<_ModuleEntryPoint>, Context1=Context1@entry=0x3f56000, Context2=0x0,
NewStack=NewStack@entry=0x7e9dff0, Context2=0x0)
    at /home/user/edk2/MdePkg/Library/BaseLib/SwitchStack.c:42
42      SwitchStack (
(gdb) 
```

---

# Exercise #6: Welcome to DXE

* If we step into `SwitchStack` we will get to `InternalSwitchStack`, we have
  no symbols loaded for that function. You should load them (this is your
  challenge).
* `InternalSwitchStack` calls `_ModuleEntryPoint` defined in
  `DxeCoreEntryPoint.c:37` which works in a very similar way to PEI Core entry
  point.
* Stepping in `_ModuleEntryPoint` should lead us to `DxeMain` for which the
  backtrace should look as follows:

```shell
(gdb) bt
#0  DxeMain (HobStart=0x3f56000) at
/home/user/edk2/MdeModulePkg/Core/Dxe/DxeMain/DxeMain.c:236
#1  0x0000000007ea0f06 in ProcessModuleEntryPointList (HobStart=<optimized
out>) at
/home/user/edk2/Build/OvmfX64/DEBUG_GCC5/X64/MdeModulePkg/Core/Dxe/DxeMain/DEBUG/AutoGen.c:507
#2  _ModuleEntryPoint (HobStart=<optimized out>) at
/home/user/edk2/MdePkg/Library/DxeCoreEntryPoint/DxeCoreEntryPoint.c:46
#3  0x0000000007ee10cf in InternalSwitchStack ()
```

* DxeMain is the DXE phase entry point.

---

# Exercise #7: DXE debug.log review

* Let's check debug.log file:
```shell
PlatformPei: ClearCacheOnMpServicesAvailable
DiscoverPeimsAndOrderWithApriori(): Found 0x0 PEI FFS files in the 1th FV
DXE IPL Entry
Loading PEIM D6A2CB7F-6A18-4E2F-B43B-9920A733700A
Loading PEIM at 0x00007E9E000 EntryPoint=0x00007EA0EF0 DxeCore.efi
Loading DXE CORE at 0x00007E9E000 EntryPoint=0x00007EA0EF0
AddressBits=36 5LevelPaging=0 1GPage=0
Pml5=1 Pml4=1 Pdp=64 TotalPage=66
Install PPI: 605EA650-C65C-42E1-BA80-91A52AB618C6
Notify: PPI Guid: 605EA650-C65C-42E1-BA80-91A52AB618C6, Peim notify entry point: 82D881
=============== here is end of PEI and next log messag coming from DXE ================
CoreInitializeMemoryServices:
  BaseAddress - 0x3F59000 Length - 0x3CA7000 MinimalMemorySizeNeeded - 0x320000
```

---

# Exercise #7: DXE debug.log review

* As we can see the first DXE related log happens a little bit earlier than
  switching between phases. So we can note that EDKII does not indicate places
  where we change phases.
* DXE phase is quite verbose and big in terms of executed code, so we will just
  get through its most important parts, as you can see above we started with
  initialization of memory related services.
* Then DXE proceed with initialization of other services and table, but does it
  quitely.
* After that DXE retreive from HOBs information deliverd by PEI phase:
* First are information about memory allocations, show memory type and adresses.
  ```shell
  (...)
  Memory Allocation 0x00000004 0x7C00000 - 0x7DFFFFF
  Memory Allocation 0x00000007 0x7E00000 - 0x7E7DFFF
  Memory Allocation 0x00000004 0x3F36000 - 0x3F55FFF
  ```

---

# Exercise #7: DXE debug.log review

* Second Firmware Volume information:
  ```shell
  FV Hob            0x900000 - 0x14FFFFF
  ```
* And then it proceeds with architectural protocols installation
  ```shell
  InstallProtocolInterface: D8117CFE-94A6-11D4-9A3A-0090273FC14D 7EC0F10
  (...)
  ```

---

# Exercise #7: DXE debug.log review

* Finally DXE puts the main actor on stage, namely the DXE dispatcher,
  responsible for loading DXE drivers, by calling the `CoreDispatcher` function.
  Each driver has its own entry point which performs some action. For OVMF X64 95
  drivers are loaded:
  ```shell
  grep "Loading driver at"  debug.log
  (...)
  Loading driver at 0x00006CF6000 EntryPoint=0x00006CFC735 UefiPxeBcDxe.efi
  Loading driver at 0x00006CB8000 EntryPoint=0x00006CBF808 IScsiDxe.efi
  Loading driver at 0x00006D20000 EntryPoint=0x00006D23B7C VirtioNetDxe.efi
  Loading driver at 0x00006CE8000 EntryPoint=0x00006CED1EE UhciDxe.efi
  Loading driver at 0x00006CAF000 EntryPoint=0x00006CB533A EhciDxe.efi
  Loading driver at 0x00006CA2000 EntryPoint=0x00006CAB912 XhciDxe.efi
  Loading driver at 0x00006C98000 EntryPoint=0x00006C9E6D6 UsbBusDxe.efi
  Loading driver at 0x00006CE1000 EntryPoint=0x00006CE5112 UsbKbDxe.efi
  Loading driver at 0x00006D1A000 EntryPoint=0x00006D1DC76 UsbMassStorageDxe.efi
  Loading driver at 0x00006CDA000 EntryPoint=0x00006CDDD91 QemuVideoDxe.efi
  Loading driver at 0x00006CEF000 EntryPoint=0x00006CF2C3C VirtioGpuDxe.efi
  ```

* After the DXE dispatcher, some minor cleanup is performed and the BDS phase
  entry is called.

---

# Exercise #8: Welcome in BDS

* Based on exercise #9 we are redy to jump into BDS phase.
* Please try to perform step in (`s`) transition from DXE to BDS by adding the
  symbols mentioned below.
* Let's start QEMU:
  ```shell
	qemu-system-x86_64 -nographic -bios Build/OvmfX64/DEBUG_GCC5/FV/OVMF.fd \
  -chardev file,path=debug.log,id=edk2-debug -device \
  isa-debugcon,iobase=0x402,chardev=edk2-debug -net none -s -S
  ```
* Let's start debugging BDS:
  ```shell
  add-symbol-file Build/OvmfX64/DEBUG_GCC5/X64/BdsDxe.debug \
  (0x00007062000+0x0000000000000240) -s .data (0x00007062000+0x000000000001a080)
  target remote :1234
  break BdsEntry
  c
  ```
* We are in BDS.

---

# Exercise #8: Welcome in BDS

* Quckily looking at the `BdsEntry`
  (`MdeModulePkg/Universal/BdsDxe/BdsEntry.c`) function, we see that the log
  output from the first exercise, `[Bds] Entry...`, is first thing BDS does.
* BDS quietly deals with all actions related to variables validation and
  loading the variable policy protocol:
  ```shell
  [BdsDxe] Locate Variable Policy protocol - Success
  ```
* Then BDS deal with locales (languages) by initializing the setup menu
  language accordingly:
  ```shell
  Variable Driver Auto Update Lang, Lang:eng, PlatformLang:en Status: Success
  ```

---

# Exercise #8: Welcome in BDS

* Then, since BDS would like to show something to user it tries to use the
  console, which triggers PCI scanning:

.small-code[
```shell
PlatformBootManagerBeforeConsole  

Registered NotifyDevPath Event  

PCI Bus First Scanning  

PciBus: Discovered PCI @ [00|00|00]  

PciBus: Discovered PCI @ [00|01|00]  

PciBus: Discovered PCI @ [00|01|01]  
   BAR[4]: Type =   Io32; Alignment = 0xF;      Length = 0x10;  Offset = 0x20  

PciBus: Discovered PCI @ [00|01|03]  

PciBus: Discovered PCI @ [00|02|00]  
   BAR[0]: Type = PMem32; Alignment = 0xFFFFFF; Length = 0x1000000;     Offset = 0x10  
   BAR[2]: Type =  Mem32; Alignment = 0xFFF;    Length = 0x1000;        Offset = 0x18  

PciBus: Discovered PCI @ [00|03|00]  
   BAR[0]: Type =  Mem32; Alignment = 0x1FFFF;  Length = 0x20000;       Offset = 0x10  
   BAR[1]: Type =   Io32; Alignment = 0x3F;     Length = 0x40;  Offset = 0x14  
(...)
```
]

---

# Exercise #8: Welcome in BDS

* Scanning triggers various drivers (e.g. `QemuVideo`), protocols
  installations, and security checks `[Security]`. This is mostly preparation for
  potential user interaction in the following steps.
* Next, hot keys are registered, to support entering the setup or quick boot
  menu:
  ```shell
  [Bds]RegisterKeyNotify: 000C/0000 80000000/00 Success
  [Bds]RegisterKeyNotify: 0017/0000 80000000/00 Success
  [Bds]RegisterKeyNotify: 0000/000D 80000000/00 Success
  ```
* Then BDS connects all the default consoles, which means on outputs detected
  earlier, to whichever drivers were found. You can find out something happened
  since our terminal where we are running QEMU changed color.

---

# Exercise #8: Welcome in BDS

* Finally BDS informs us about OS-indicated features, and dumps various
information related to the boot process:

```shell
[Bds]OsIndication: 0000000000000000
[Bds]=============Begin Load Options Dumping ...=============
  Driver Options:
  SysPrep Options:
  Boot Options:
    Boot0000: UiApp              0x0109
    Boot0001: UEFI QEMU DVD-ROM QM00003                  0x0001
    Boot0002: UEFI PXEv4 (MAC:525400123456)              0x0001
    Boot0003: EFI Internal Shell                 0x0001
  PlatformRecovery Options:
    PlatformRecovery0000: Default PlatformRecovery               0x0001
[Bds]=============End Load Options Dumping=============
```

* Now BDS waits for user interaction. E.g. entering setup or quick boot menu.
  If no action is taken, boot options would be executed according to priority.

---

# Exercise #8: Welcome in BDS

* On the screen we can find:
```shell
BdsDxe: failed to load Boot0001 "UEFI QEMU DVD-ROM QM00003 " from
PciRoot(0x0)/Pci(0x1,0x1)/Ata(Secondary,Master,0x0): Not Found
```

* This is quite normal assuming we didn't connect a DVD-ROM to QEMU.  
* Then if we hadn't given the `-net none` option we would see the PXE loading
  screen. And as a final step we land in the UEFI Shell since no options was
  selected:
```shell
UEFI Interactive Shell v2.2
```

---
# UEFI Shell

- The UEFI Shell is an example of an OS-Absent App. Another could be MemTest
- If disabled (as it usually is on consumer platforms), it can be included on a
  bootable device
- Usually used for maintenance (e.g. updates, security/compliance tests) and
  recovery
- Before an interactive shell is launched, boot media are searched for
  `startup.nsh` file. If found, it's executed as initialization
- It could be used for more sophisticated BDS implementation or as a way to
  achieve the goals of potential adversary

---

# UEFI Shell

.image-70[.center[![Image](images/uefi-shell.png)]]

???

Resembles MS-DOS a lot. That's a pity that its syntax is similar too. It's not
as flexible as POSIX shell.

---

# UEFI Shell

- Useful tool when your OS refuses to boot
- It can access the boot media (however with limited FS support), and run UEFI applications,
including boot loaders (`*.efi` files, PE format)

```c
#include <Uefi.h>

EFI_STATUS
EFIAPI
UefiMain (
   IN EFI_HANDLE        ImageHandle,
   IN EFI_SYSTEM_TABLE  *SystemTable
   )
{
   Print(L"Hello, world!\n");

   return EFI_SUCCESS;
}
```

.code-comment[Hello World for UEFI environment]

---

# Quiz #6

--

**When are Boot Services disabled?**

--

- On `BootServices.ExitBootServices()` call
- Usually in BDS, right before calling kernel code

--

**Compare PEI and DXE (provide 3 differences)**

--

- They are almost the same. Architecture is almost the same, the names are
  different
- PEI works in a memory-limited environment and thus only core services are
  available
- PEI produces HOBs, DXE consumes them

--

**What specification describes the SEC and PEI phases?**

--

- PI architecture specification (same as DXE and BDS)

---

**What is the core goal of PEI? Why?**

--

- DRAM initialization
- Few megabytes of cache is not enough to create a full initialization
  environment

---

class: center, middle, intro

# Practice #5: UEFI Shell exploration

---

# Exercise #1: Enter UEFI Shell

UEFI Shell commands are like small applications built into the UEFI Shell
Application. To enter the UEFI Shell in the case of our QEMU+OVFM setup, we
would just boot the platform and wait for Shell. Since it would be selected
according to boot order.

Start QEMU, and wait for the UEFI shell to start:

```shell
qemu-system-x86_64 -nographic -bios Build/OvmfX64/DEBUG_GCC5/FV/OVMF.fd \
-chardev file,path=debug.log,id=edk2-debug \
-device isa-debugcon,iobase=0x402,chardev=edk2-debug \
-net none -device qemu-xhci,id=xhci -net none
```

To list all available commands simply type `help`. Get familiar with available
commands. Please note not all commands are available in all versions of UEFI
Shell. Available commands are selected at build time in EDKII.

```shell
Shell> help
```

???

- As first thing we will something very basic and then you can try something
  more complex as homework.
- Please let me know which command is most interesting to you at first glance
  and why.

---

# Exercise #1: Enter UEFI Shell

* Based on number of commands we can easily say this is very simple OS-like
  environment similar to DOS. But what is more important it runs in ring 0, so it
  is highly priviledged.
* Try to read help for some commands and get through all documentation
  sections. (For instancel look at `mm` and think about how you could do things
  with PCIe access that you learned about in Arch4001, if you took that already.)
* You can also read more about the commands, as well as scripting in the shell,
  in the [UEFI Shell Specification
  2.0](https://uefi.org/sites/default/files/resources/UEFI_Shell_Spec_2_0.pdf)
  document.

---

# Exercise #2: List all block devices

* Create a filesystem to act as a USB device
* From Exercise #1 you may have looked at the `map` command, which shows
  existing block devices and filesystems.
* `map -r` is very useful to detect hotplugged devices, since UEFI does
  enumeration just once.
* In other terminal let's create FAT32 image that will be used as USB stick:
```shell
user@user-OST-VM:~$ qemu-img create -f raw /tmp/usb.img 128M
Formatting '/tmp/usb.img', fmt=raw size=134217728
user@user-OST-VM:~$ mkfs.vfat -F32 /tmp/usb.img 
mkfs.fat 4.1 (2017-01-24)
```

---

# Exercise #2: List all block devices

* Now let's add some files to the filesystem. The EFI Self-Certification Test
  (SCT) can be used to evaluate the quality of a UEFI implementation.
* Obtain SCT:
```shell
wget
https://github.com/tianocore/edk2-test/releases\
/download/edk2-test-stable202108/UefiSctBinaryX64.zip
unzip UefiSctBinaryX64.zip
```
([Mirror copy of UefiSctBinaryX64.zip incase the above link breaks](https://gitlab.com/opensecuritytraining/arch4021-intro-uefi-additional-files/-/blob/master/UefiSctBinaryX64.zip))

* Mount our USB disk image and copy SCT to it
```shell
sudo mount /tmp/usb.img /media
sudo cp -r SctPackageX64/ /media
sudo umount /media
```

---

# Exercise #2: List all block devices

* Start QEMU and mount the virtual USB device
* Now start qemu with USB XHCI controller.
  ```shell
  qemu-system-x86_64 -nographic -bios Build/OvmfX64/DEBUG_GCC5/FV/OVMF.fd \
  -chardev file,path=debug.log,id=edk2-debug -device \
  isa-debugcon,iobase=0x402,chardev=edk2-debug -net none -device \
  qemu-xhci,id=xhci
  ```
* Check `map` output
  ```shell
  Shell> map
  Mapping table
       BLK0: Alias(s):
            PciRoot(0x0)/Pci(0x1,0x1)/Ata(0x0)
  ```
* Let's check how the block devices listed change when we will add a file as a
  USB device. 

---

# Exercise #2: List all block devices

* Enter QEMU Monitor (`Ctrl-a c`) and add the virtual USB stick.

```shell
(qemu) drive_add 0 if=none,id=stick,format=raw,file=/tmp/usb.img
OK
(qemu) device_add usb-storage,id=ost2,bus=xhci.0,drive=stick
```

* Let's get back to our UEFI Shell, by `Ctrl-a c`, to clean screen use `cls`
  command
* Let's run `map` again. It looks like nothing happen?
* Let's use `map -r`:

```shell
Shell> map -r
Mapping table
      FS0: Alias(s):HD1a0:;BLK1:
          PciRoot(0x0)/Pci(0x4,0x0)/USB(0x0,0x0)
     BLK0: Alias(s):
          PciRoot(0x0)/Pci(0x1,0x1)/Ata(0x0)
```

* As we can see `FS0` appeared under `BLK1` device.
* We would see similar behavior with real hardware.

---

# Exercise #2: List all block devices

* Now you can change directory to the fs0 filesystem by just typing `fs0:` (the
  colon is required), and use `ls` or `dir` to see the filesystem contents.

  ```shell
  Shell> fs0:
  FS0:\> ls
  Directory of: FS0:\
  10/21/2022  12:01 <DIR>           512  SctPackageX64
            0 File(s)           0 bytes
            1 Dir(s)
  
  ```

---

# Exercise #3: Run UEFI SCT

* Instead of dynamically adding the virtual USB at runtime, you can also attach
  the disk at boot time. This will cause it to be found by the initial
  enumeration.
  ```shell
  qemu-system-x86_64 -nographic -bios Build/OvmfX64/DEBUG_GCC5/FV/OVMF.fd \
  -chardev file,path=debug.log,id=edk2-debug -device \
  isa-debugcon,iobase=0x402,chardev=edk2-debug -net none -drive \
  file=/tmp/usb.img,index=0,media=disk,format=raw
  ```

---

# Exercise #3: Run UEFI SCT

.small-code[
```shell
UEFI Interactive Shell v2.2
EDK II
UEFI v2.70 (EDK II, 0x00010000)
Mapping table
      FS0: Alias(s):F0a:;BLK0:
          PciRoot(0x0)/Pci(0x1,0x1)/Ata(0x0)
     BLK1: Alias(s):
          PciRoot(0x0)/Pci(0x1,0x1)/Ata(0x0)
Press ESC in 1 seconds to skip startup.nsh or any other key to continue.
Shell> fs0:
FS0:\> ls  
Directory of: FS0:\
10/21/2022  12:18              10,367  NvVars
10/21/2022  12:01 <DIR>           512  SctPackageX64
          1 File(s)      10,367 bytes
          1 Dir(s)
FS0:\> cd SctPackageX64
FS0:\SctPackageX64\> ls
Directory of: FS0:\SctPackageX64\
10/21/2022  12:01 <DIR>           512  .
10/21/2022  12:01 <DIR>             0  ..
10/21/2022  12:01              32,800  InstallX64.efi
10/21/2022  12:01               1,505  SctStartup.nsh
10/21/2022  12:01 <DIR>         1,024  X64
FS0:\SctPackageX64\>     
```
]

---

# Exercise #3: Run UEFI SCT

* Run `InstallX64.efi` and select index 1 for FS0. It will create a directory
  in `FS0:\` and `startup.nsh` script which will be used after reboot.
* After installation completes, go to `FS0:\SCT`
* Running a full test suite using `SCT.efi -a` hangs in QEMU (at least it
  hanged for me) after around 10 hours of execution. `-v` should accelerate
  execution 70-80% but it is still a lot.
* So, let's try running only some subset of tests through a sequence file:
```shell
FS0:\Sct\> SCT.efi -u
```

---

# Exercise #3: Run UEFI SCT

* Go to `Test Case Management->GenericTest->EfiCompliantTest` and highlight
  `PlatformSpecificElements`.
* Let's run that test case. Things would be easy if we could just hit `F9` as
  application bottom bar say, unfortunately in our case it will send different
  characters to SCT which will lead to setting 20 iteration without exiting. So
  we have to use the trick with sending `F9` from QEMU monitor.
* Let's switch to monitor `Ctrl-a c` and type `sendkey f9`. This will run tests
  with result of 14 passes and 16 fails.
* Let's get back to main menu, and generate test report using `Test Report
  Generator`. `F2` have to be sent using above trick to enter report file name.
  Test report can be found in `FS0:\SCT\Report\<report_name>.csv`. You can view
  them with `edit`, and exit edit with `Ctrl-q`.
* We can also take a look at test logs by using `View Test Log` and choosing
  `GenericTest->EfiCompliantTest0` and file that is inside directory - now you
  are in primitive editor of UEFI (`Ctrl-e` for help).

---

# Exercise #3: Run UEFI SCT

* Hit `Ctrl-g` and give line 142, this should give you following screen.

.image-70.center[![](https://gitlab.com/opensecuritytraining/arch4021_uefi_intro_slides_and_subtitles/-/raw/main/images/sct_test_log_view.png)]

* You can exit the editor with `Ctrl-q`.

---

# Exercise #4: Save sequence file

* If it is not selected, go to `Test Case
Management->GenericTest->EfiCompliantTest` and highlight
`PlatformSpecificElements`.
* Now we have to save sequence file. Exit back to the main starting menu. You
should see the F6 option to save a sequence at the bottom of the screen.

---

# Exercise #4: Save sequence file

* Let's switch to monitor `Ctrl-a c` and type `sendkey f6`, we should see
following screen.

.image-80.center[![](https://gitlab.com/opensecuritytraining/arch4021_uefi_intro_slides_and_subtitles/-/raw/main/images/sct_sendkey_f6.png)]

---

# Exercise #4: Save sequence file

* Then type `sendkey f2` and let's switch back (`Ctrl-a c`) top provide
filename `ost2` and hit `Enter`, this should give us following screen.

.image-60.center[![](https://gitlab.com/opensecuritytraining/arch4021_uefi_intro_slides_and_subtitles/-/raw/main/images/sct_seq_save_succeed.png)]

* Then type `Ctrl-a c` to get out of the QEMU menu and type the sequence file
name of "ost2", and hit enter.

---

# Exercise #4: Save sequence file

* We should see file in `Sequence` directory:
```shell
FS0:\Sct\> ls Sequence
```

* It should be possible to run those tests using following command:
```shell
Shell> SCT.efi -s ost2.seq
```

---

# Exercise #5: HelloWorld EFI application

* Build HelloWorld and copy to a mountable drive
* Future classes like Arch4023 will dive much deeper into programming in a UEFI
  environment. For now we'd like to give you a basic HelloWorld that will run as
  an OS-absent application from the UEFI shell. This will give you something to
  play with, and somewhere to put code you either write based on reading the UEFI
  specification on your own, or based on finding code on the internet and wanting
  to try it out.
* As it turns out, EDKII has a HelloWorld application already built in at
  MdeModulePkg/Application/HelloWorld/HelloWorld.c
* This is a little more complicated than a minimal HelloWorld, because instead
  of just hardcoding in the HelloWorld string, it chooses to look it up via a PCD
  entry (which as we mentioned earlier in the class is a Portable Configuration
  Database entry, which is just like a glorified configuration-specific global
  variable.) Lets proceed to just running the executable and then we can return
  to finding the PCD definitions (since simple grepping for now would yield too
  many hits to make it clear.) 

---

# Exercise #5: HelloWorld EFI application

* To compile it, we must build the MdeModulePkg where the file is found.
```shell
build -a X64 -p MdeModulePkg/MdeModulePkg.dsc -t GCC5 -n$(nproc)
```

* Let's make a new /tmp/usb2.img and copy HelloWorld.efi to it:
```bash
qemu-img create -f raw /tmp/usb2.img 128M
mkfs.vfat -F32 /tmp/usb2.img
sudo mount /tmp/usb2.img /media
sudo cp Build/MdeModule/DEBUG_GCC5/X64/HelloWorld.efi /media
sudo umount /media
```

---

# Exercise #5: HelloWorld EFI application

* Start QEMU and with the drive attached
* Now start qemu with the /tmp/usb2.img file attached at boot time:
  ```shell
  qemu-system-x86_64 -nographic -bios Build/OvmfX64/DEBUG_GCC5/FV/OVMF.fd \
  -chardev file,path=debug.log,id=edk2-debug -device \
  isa-debugcon,iobase=0x402,chardev=edk2-debug -net none -drive \
  file=/tmp/usb2.img,index=0,media=disk,format=raw
  ```
* Now run QEMU and launch HelloWorld.efi from the UEFI shell. If you read the
  comments at the beginning of HelloWorld.c, you'll also know that it supports a
  `-?` option, so let's try that too.

.small-code[
```shell
UEFI Interactive Shell v2.2
EDK II
UEFI v2.70 (EDK II, 0x00010000)
Mapping table
      FS0: Alias(s):F0a:;BLK0:
          PciRoot(0x0)/Pci(0x1,0x1)/Ata(0x0)
     BLK1: Alias(s):
          PciRoot(0x0)/Pci(0x1,0x1)/Ata(0x0)
Press ESC in 5 seconds to skip startup.nsh or any other key to continue.
Shell> fs0:
FS0:\> HelloWorld.efi
UEFI Hello World!
FS0:\> HelloWorld.efi -?
HelloWorld application.
```
]

---

# Exercise #5: HelloWorld EFI application

* Finding where the PCDs are defined
* Now that we've actually executed the code, we know that the
  PcdHelloWorldPrintString corresponds to the string "UEFI Hello World!". So
  let's search for that.

.small-code[
```shell
grep -r "UEFI Hello World!" *
...

MdeModulePkg/MdeModulePkg.dec:  gEfiMdeModulePkgTokenSpaceGuid.PcdHelloWorldPrintString|L"UEFI Hello World!\n"|VOID*|0x40000004
```
]

* There were a decent number of hits, but only the last one looked like it
  wasn't a comment. So recall that .dec files are Package Description files, and
  `MdeModulePkg/MdeModulePkg.dec` seems to be the description for the MdeModule
  Package.
* If we open up `MdeModulePkg/MdeModulePkg.dec` we will see the other PCD
  entries found in HelloWorld.c such as PcdHelloWorldPrintEnable and
  PcdHelloWorldPrintTimes.

---

# Exercise #5: HelloWorld EFI application

* Go ahead and edit PcdHelloWorldPrintTimes to be a number other than one, and
  also add your own Print() string to HelloWorld.c. But ***note***, UEFI uses
  2-byte long-character strings. So you must prefix your string with an L, like
  L"This is my string".
* Recompile your HelloWorld, re-copy it into the disk image, re-run QEMU, and
  re-launch your HelloWorld.efi to see your changes took effect.

---
class: center, middle, intro

# UEFI variables

???

75% of material

Version:
<https://gitlab.com/opensecuritytraining/arch4021_uefi_intro_slides_and_subtitles/-/blob/f9e1953282622f140dedba77c6aab33eee13f579/1-2-uefi-variables.md>

---

# Introduction

- UEFI variables are used to store UEFI firmware configuration data.
- Variables are defined as key/value pairs
- The key - consists of identifying information and attributes
- The value - arbitrary data, typically in binary format
- The goal of variables is to provide required information to all system
  components (boot firmware, OS loaders, operating systems and applications).
- Variables Services are part of Runtime Services in UEFI Specification - for
  detailed description please refer to chapter 8.2.
- UEFI Specification does not cover variable storage media and its security.
- Knowledge of UEFI Variables is critical to understanding UEFI platform
  security.
- All options of UEFI-compliant boot firmware are stored in UEFI Variables.

---

# UEFI Variables protection mechanisms

- **Integrity protection**
    + Variable authentication - discussed widely in this course
    + Access in Trused Execution Environment (e.g. SMM) - access to variable in
    thighly controlled and isolated environment improve protection. Discussed
    slightly in practical use cases section.
    + Lock - locking access and modification to some variables at given boot stage.
    + Sanity checks - help make sure that variable can get only valid value.
    + Support for Replay Protected Memory Block of NVMe, eMMC and UFS compliant
    devices.
- **Availability protection**
    + Flash wear-out protection - built-in in EDKII
    + Variable quota management - built-in in EDKII
- **Confidentiality protection**
    + User Key Encrypted Variable - supported through TPM or CSME

???

- There are multiple protection mechanisms, both SW and HW, built-in modern
  computing system design and UEFI firmware.
- We just showing couple of those here, but there are more.

---

# Variable attributes

- Implemented as bit field. In spec we find the following:

```c
//*******************************************************
// Variable Attributes
//*******************************************************
#define EFI_VARIABLE_NON_VOLATILE                          0x00000001
#define EFI_VARIABLE_BOOTSERVICE_ACCESS                    0x00000002
#define EFI_VARIABLE_RUNTIME_ACCESS                        0x00000004
#define EFI_VARIABLE_HARDWARE_ERROR_RECORD                 0x00000008
//This attribute is identified by the mnemonic 'HR' elsewhere
//in this specification.
#define EFI_VARIABLE_AUTHENTICATED_WRITE_ACCESS            0x00000010
//NOTE: EFI_VARIABLE_AUTHENTICATED_WRITE_ACCESS is deprecated
//and should be considered reserved.
#define EFI_VARIABLE_TIME_BASED_AUTHENTICATED_WRITE_ACCESS 0x00000020
#define EFI_VARIABLE_APPEND_WRITE                          0x00000040
#define EFI_VARIABLE_ENHANCED_AUTHENTICATED_ACCESS         0x00000080
//This attribute indicates that the variable payload begins
//with an EFI_VARIABLE_AUTHENTICATION_3 structure, and
//potentially more structures as indicated by fields of this
//structure. See definition below and in SetVariable().
```

.code-comment[ UEFI specification v2.9 ]

---

# Variable attributes

- Dependencies between attributes are quite complicated - security researchers
  may find UEFI Variables an interesting area of investigation. Especially the
  difference in that regard between specification versions
- `EFI_VARIABLE_NON_VOLATILE`
    + If a variable is non-volatile it's stored in non-volatile memory instead of
    DRAM. Non-volatile storage may be limited so should be used for a reasonable
    purpose.
- `EFI_VARIABLE_RUNTIME_ACCESS`
    + If set, it implies that `EFI_VARIABLE_BOOTSERVICE_ACCESS` is also set
    + `ExitBootServices()` function call is when `RUNTIME` policy starts to apply.
    + `ExitBootServices()` function is typically called in the bootloader when
    all initialization is done and the kernel is loaded to memory.
- `EFI_VARIABLE_HARDWARE_ERROR_RECORD`
    + Hardware Error Record variables, which have architecturally defined meaning.
- `EFI_VARIABLE_AUTHENTICATED_WRITE_ACCESS` - deprecated

???

- There are quite tricky rules like setting no access attribute causing
  variable deletion.
- This is not that important at this point, but it's good to know, that if
  something unexpected happens tricky rules may be the reason and we need to
  check in spec.
- All rules description is quite fascinating since the way spec writers did
  that seem to complicate overall analysis and implementation e.g. note related
  to EFI_VARIABLE_HARDWARE_ERROR_RECORD that in a text it is referred to as HR

Old notes:
- In `EDKII` sources we find also `EFI_VARIABLE_READ_ONLY` with a note that
this one is non-standard, kept for backward compatibility.

---

# Variable attributes

- `EFI_VARIABLE_TIME_BASED_AUTHENTICATED_WRITE_ACCESS`
    + Authentication shall use `EFI_VARIABLE_AUTHENTICATION_2`
    + Secure Boot Policy Variable must set this attribute
    + Process of setting this variable is quite complicated
- `EFI_VARIABLE_APPEND_WRITE`
    + Never set in returned attributes bitmask (`GetVariable()`)
    + Allow appending to an existing variable value
    + Creates a lot of corner cases and interesting use cases
    + Used by Secure Boot Key Exchange Keys and Signature Database
- `EFI_VARIABLE_ENHANCED_AUTHENTICATED_ACCESS`
    + Authentication shall use `EFI_VARIABLE_AUTHENTICATION_3`
    + It is mutually exclusive with
  `EFI_VARIABLE_TIME_BASED_AUTHENTICATED_WRITE_ACCESS`
    + Even more complicated than
  `EFI_VARIABLE_TIME_BASED_AUTHENTICATED_WRITE_ACCESS`

???

- `EFI_VARIABLE_ENHANCED_AUTHENTICATED_ACCESS` - we can't see any sign of usage
  of this variable in edk2 code

---

# Variable attributes

.small-code[

```C
//
// EFI_VARIABLE_AUTHENTICATION_2 descriptor
//
// A time-based authentication method descriptor template
//
typedef struct {
  EFI_TIME
    TimeStamp;
  WIN_CERTIFICATE_UEFI_GUID AuthInfo;
} EFI_VARIABLE_AUTHENTICATION_2;
```

```C
//
// EFI_VARIABLE_AUTHENTICATION_3 descriptor
//
// An extensible implementation of the Variable Authentication
// structure.
//
#define EFI_VARIABLE_AUTHENTICATION_3_TIMESTAMP_TYPE 1
#define EFI_VARIABLE_AUTHENTICATION_3_NONCE_TYPE 2
typedef struct {
  UINT8     Version;
  UINT8     Type;
  UINT32    MetadataSize;
  UINT32    Flags;
} EFI_VARIABLE_AUTHENTICATION_3;
```

]

- `EFI_VARIABLE_AUTHENTICATION_2` consists of timestamp and certificate
information
- `EFI_VARIABLE_AUTHENTICATION_3` consists of type, metadata size and flags
- Authenticated access flags cause additional verification steps

???

- Existence of one bitmask leads to existence of other flags. Pandora's box.
- In EFI_VARIABLE_AUTHENTICATION_3 type triggers adding secondary descriptor,
  but to explain this flow we would have to finish earlier or stay little bit
  longer.

---

# Data with EFI_VARIABLE_AUTHENTICATION_2

.image-55[.center[![Image](images/uefi_setvariable_authentication_2.svg)]]

- `SetVariable(VariableName, VendorGuid, Attributes, DataSize, Data)`

???

Above diagram present complexity of UEFI Authenticated variables. It shows just
the `SetVariable()` caller's side. The caller is responsible for gathering all
required information, hashing, signing, constructing PKCS#7 and concatenating it
with the data. Whole concatenation is passed to `SetVariable()`.

We have to ge through this process every time we want to create authenticated
variable, but there are tools to automate that (e.g. sbsigntools). Unfortunately those are not
always well maintained.

EFI_VARIABLE_AUTHENTICATION_3 is even more complex.

PlantUML source:

```
@startuml
skinparam defaultTextAlignment center
skinparam backgroundcolor transparent
start
:Create
EFI_VARIABLE_AUTHENTICATION_2
descriptor;
:Hash(VariableName, VendorGuid, Attributes, TimeStamp, Variable_New_Content);
:Sign digest;
:Construct a DER-encoded SignedData structure
(PKCS#7 version 1.5 (RFC 2315));
:EFI_VARIABLE_AUTHENTICATION_2.AuthInfo.CertData = SignedData;
:Concatenate serialized
EFI_VARIABLE_AUTHENTICATION_2
and
Variable_New_Content;
end
@enduml
```

---

# Data with EFI_VARIABLE_AUTHENTICATION_3

.image-50[.center[![Image](images/uefi_setvariable_authentication_3.svg)]]

???

@startuml
skinparam defaultTextAlignment center
skinparam backgroundcolor transparent
start
:Create
EFI_VARIABLE_AUTHENTICATION_3
descriptor;
switch(Descriptor Type)
case (EFI_VARIABLE_AUTHENTICATION_3_TIMESTAMP_TYPE)
:Create SecondaryDescriptor with
EFI_TIME TimeStamp;
case (EFI_VARIABLE_AUTHENTICATION_3_NONCE_TYPE)
:Create SecondaryDescriptor with
EFI_VARIABLE_AUTHENTICATION_3_NONCE;
endswitch
if (update or deletion of EFI_VARIABLE_AUTHENTICATION_3_NONCE_TYPE) then (yes)
:AditionalContentToHash ||= NonceBufferOfExistingVariable;
else (no)
endif
if (EFI_VARIABLE_AUTHENTICATION_3.Flags == EFI_VARIABLE_ENHANCED_AUTH_FLAG_UPDATE_CERT 0x0) then (yes)
:AditionalContentToHash ||= NewCert;
else (no)
endif
:Hash(VariableName, VendorGuid, Attributes, SecondaryDescriptor, Variable_New_Content, AdditionalContentToHash);
:Sign digest;
:Create WIN_CERTIFICATE_UEFI_GUID.CertType = EFI_CERT_TYPE_PKCS7_GUID;
:Construct a DER-encoded SignedData structure
(PKCS#7 version 1.5 (RFC 2315));
:Update the EFI_VARIABLE_AUTHENTICATION_3.MetadataSize field;
:Concatenate serialized
EFI_VARIABLE_AUTHENTICATION_3
and
Variable_New_Content;
end
stop
@enduml

---

# SetVariable() for Authenticated Variables

Checks performed when `EFI_VARIABLE_TIME_BASED_AUTHENTICATED_WRITE_ACCESS`
attribute is passed to `SetVariable()` (simplified):

- Verify format of data.
- Verify that TimeStamp is later than the current value assigned to variable,
  if any.
- Check if signature is valid
    + At this time both data and signature from `SetVariable()` input is used for
    verification.
- Check if target variable can be changed by this signer.
    + This is the first time existing variable is consulted.
    + A lot of special cases, depending on variable "type", different rules (and
    sometimes backdoors) for PK, KEK, SecureBoot, OS recovery...
    + If a variable doesn't belong to any of the special classes, it is called
    Private Authenticated Variable. Such variables can be changed only by the
    original signer that created this variable.
- If any of the checks fail, return `EFI_SECURITY_VIOLATION`

???

- Let's see what will happen behind the scene when we passed created previously
  structure to SetVariable.

To some extend:
- caller of `SetVariable()` handles authentication
    + "data I provided is authentic"
- `SetVariable()` performs authorization
    + "you are authorized to write to this variable"

Mentioned backdoors include SetupMode which will be explained in presentation
about SecureBoot. It allows for writing to _some_ of authenticated variables
with dummy `EFI_VARIABLE_AUTHENTICATION_2` structure.

Questions:
- while verifying signature during `SetVariable()` how RuntimeServices know
  which public key use to verify?

---

# Practical use cases

- Major operating systems already provide tools for managing authenticated
  variables, meaning that the complexity is wrapped.
- Authenticated variables are designed to provide and maintain the UEFI Secure
  Boot status.
- Current implementation of UEFI Authenticated Variables is in EDKII System
  Management Mode code.
    + The assumption is that it improves security
- Authenticated variables do not protect against physical SPI NOR Flash
  modification.
    + Authenticated variables prevent OS level modification, if code doing
    modification is protected at runtime against ring0 attacks - SMM and
    appropriate SPI controller configuration should provide that protection.
    + Please note that UEFI Authenticated Variables does not provide
    confidentiality, so anyone can read the content of variables, but only the
    key owner can write and update.

???

Typically Intel chipsets have the bit that denies flash access by software (BWPD
- BIOS Write Protection Disable or BWE - BIOS Write Enable). Additionally,
there is EISS which allows access to the SPI flash only in SMM. So only with
EISS (Enable InSMM.STS) the Auth Variables provide the offered protection.
Otherwise any software could actually implement an application that would write
variables in UEFI variable compliant format.

Platform builder/firmware developer should ensure by any means necessary (e.g.
hardware protections) that write access to the UEFI variable storage is
possible only with Authenticated Variables interface. SPI controller is just
one example which mainly focuses on x86.

---

# Use cases for UEFI variables

- UEFI Specification in section 3.3 defines 40 global variables with different
  purposes.
    + We want to mention most of the important ones
- UEFI Secure Boot - refer to "Arch4201: UEFI Secure Boot"
- Boot order and related configuration
- Console input and output
- Driver loading options
- Language
- OS-firmware features communication
- UEFI Capsule Update
- Storing ACPI SSDT (Secondary System Descriptor Table) overlays
- Others

???

- Hypervisors developers may care also because of control that UEFI variables
  may give over managed VMs
- CVM are hot topic gaining on importance so most probably in near future UEFI
  Secure Boot and other firmware security feature would be very important to
  all hypervisor vendors.
- Recent Microsoft Certification calls from more features like vTPMs.
- Both SysPrep and OsIndication global UEFI variables should be interesting to
  researchers
- SSDT overlays - are used for open-ended hardware configurations like
  development boards to expose additional interfaces like i2c or spi buses,
  recompiling firmware for that purpose is not practical, so overly can be
  provided by firmware variable and other means in Linux (configfs and initird)

---

class: center, middle, intro

# Practice #6: Accessing variables from Linux

---

# Accessing variables from Linux

- UEFI variables are exposed by kernel.
- Historically, two different mechanisms existed:
    + "the old one" in `sysfs`, used by original EFI and early UEFI
    + [efivarfs](https://www.kernel.org/doc/html/latest/filesystems/efivarfs.html)
- `sysfs` access was very limited, it is no longer used and won't be described
- `efivarfs` is typically mounted at `/sys/firmware/efi/efivars`
- Reading is easy
    + file content starts with attributes of the variable (4 bytes, little-endian)
- Deleting and modification must be explicitly enabled "due to the presence of
  numerous firmware bugs"
- Creating and writing to variables requires special treatment, described in
  [SSDT overlays](https://www.kernel.org/doc/html/latest/admin-guide/acpi/ssdt-overlays.html#loading-acpi-ssdts-from-efi-variables)
    + strict format of file name: `Name-GUID`
    + file must start with UEFI variable attributes (as when it's read)
    + writing to the file must be done with one write operation

---

# Exercise #1: Reading UEFI variables

- Now we can boot QEMU:

```shell
qemu-system-x86_64 -nographic -bios Build/OvmfX64/DEBUG_GCC5/FV/OVMF.fd \
-chardev file,path=debug.log,id=edk2-debug \
-device isa-debugcon,iobase=0x402,chardev=edk2-debug \
-net none -device qemu-xhci,id=xhci -m 512M \
/home/user/alpine-virt-3.16.1-x86_64.iso
```

- Login with root

---

# Exercise #1: Reading UEFI variables

To list all runtime-accessible variables:

```shell
localhost:~# ls /sys/firmware/efi/efivars/
Boot0000-8be4df61-93ca-11d2-aa0d-00e098032b8c
Boot0001-8be4df61-93ca-11d2-aa0d-00e098032b8c
Boot0002-8be4df61-93ca-11d2-aa0d-00e098032b8c
(...)
PlatformLang-8be4df61-93ca-11d2-aa0d-00e098032b8c
PlatformLangCodes-8be4df61-93ca-11d2-aa0d-00e098032b8c
PlatformRecovery0000-8be4df61-93ca-11d2-aa0d-00e098032b8c
Timeout-8be4df61-93ca-11d2-aa0d-00e098032b8c
VarErrorFlag-04b37fe8-f6ae-480b-bdd5-37d98c5e89aa
```

Note that most of them has the same GUID `8BE4DF61-93CA-11D2-AA0D-00E098032B8C`,
but there are some different values, too. We can check in
`Build/OvmfX64/DEBUG_GCC5/FV/Guid.xref` that was produced during building OVMF
that this GUID corresponds to `gEfiGlobalVariableGuid`.

Check what is the GUID of a variable with name `NvVars`.

???

NvVars-964e5b22-6459-11d2-8e39-00a0c969723b - gEfiSimpleFileSystemProtocolGuid

---

# Exercise #1: Reading UEFI variables

UEFI variables rarely contain just printable characters, and even if they did,
the variable starts with its attributes, which have bytes with value 0. Because
of that, `cat` can't be used, we must utilize `xxd` or `hexdump`, for example:

```shell
localhost:~# xxd /sys/firmware/efi/efivars/Boot0003-8be4df61-93ca-11d2-aa0d-00e098032b8c -g 1
00000000: 07 00 00 00 01 00 00 00 2c 00 45 00 46 00 49 00  ........,.E.F.I.
00000010: 20 00 49 00 6e 00 74 00 65 00 72 00 6e 00 61 00   .I.n.t.e.r.n.a.
00000020: 6c 00 20 00 53 00 68 00 65 00 6c 00 6c 00 00 00  l. .S.h.e.l.l...
00000030: 04 07 14 00 c9 bd b8 7c eb f8 34 4f aa ea 3e e4  .......|..4O..>.
00000040: af 65 16 a1 04 06 14 00 83 a5 04 7c 3e 9e 1c 4f  .e.........|>..O
00000050: ad 65 e0 52 68 d0 b4 d1 7f ff 04 00              .e.Rh.......
```

---

# Boot options variables

- `Boot####` variables contain boot media options. It contains `LOAD_OPTION`
structure:

```c
#pragma pack(1)
typedef struct _EFI_LOAD_OPTION {
  UINT32                        Attributes;         // for LOAD_OPTION, not variable!
  UINT16                        FilePathListLength; // in bytes
  CHAR16                        Description[];      // 0-terminated label
  EFI_DEVICE_PATH_PROTOCOL      FilePathList[];     // image path
  UINT8                         OptionalData[];     // if present, passed to image
} EFI_LOAD_OPTION;
#pragma pack()
```

.code-comment[ MdePkg/Include/Uefi/UefiSpec.h ]

```c
typedef struct {
  UINT8 Type;
  UINT8 SubType;
  UINT8 Length[2];
} EFI_DEVICE_PATH_PROTOCOL;
```

.code-comment[ MdePkg/Include/Protocol/DevicePath.h ]

???

Note that in actual code there are very useful comments
for each field.

---

# Boot options variables

```
00000000: 07 00 00 00 01 00 00 00 2c 00 45 00 46 00 49 00  ........,.E.F.I.
          ^ var att ^ ^ L.O. att^ ^FPL^ ^ UTF16 label
00000010: 20 00 49 00 6e 00 74 00 65 00 72 00 6e 00 61 00   .I.n.t.e.r.n.a.

00000020: 6c 00 20 00 53 00 68 00 65 00 6c 00 6c 00 00 00  l. .S.h.e.l.l...
                                         (0-terminated) ^
00000030: 04 07 14 00 c9 bd b8 7c eb f8 34 4f aa ea 3e e4  .......|..4O..>.
          T  ST ^len^ ^ device path 1
00000040: af 65 16 a1 04 06 14 00 83 a5 04 7c 3e 9e 1c 4f  .e.........|>..O
                    ^ T  ST ^len^ ^ device path 2
00000050: ad 65 e0 52 68 d0 b4 d1 7f ff 04 00              .e.Rh.......
                                ^ T  ST ^len^
 ```

.code-comment[ example boot option structure ]

- Variable attributes = 7 = 4 + 2 + 1, non-volatile, runtime accessible
(bootservice access is required by runtime access)

???

Abbreviations:
- L.O. - load option
- FPL - FilePathListLen
- T - Type:
    + 04 = Media Device Path
    + 7F = End of Device Path
- ST - SubType, depends on Type, for Media Device Path:
    + 07 is PIWG Firmware Volume (data is GUID of Volume, PIWG = PI Working Group)
    + 06 is PIWG Firmware File (data is GUID of File)
- SubType for Type = 7F:
    + FF - End Entire Device Path (as opposed to end this Device Path and start a
    new one, not used for `Boot####`)

---

# Boot options variables

```
BootOrder-8be4df61-93ca-11d2-aa0d-00e098032b8c
00000000: 07 00 00 00 00 00 01 00 02 00 03 00              ............
          ^ var att ^ ^1st^ ^2nd^ ^3rd^ ^4th^
```

- As we see boot options are quite straightforward, just 16-bit option
numbers

```
Timeout-8be4df61-93ca-11d2-aa0d-00e098032b8c
00000000: 07 00 00 00 00 00                                ......
          ^ var att ^ ^val^
```

- The same with boot timeout (time for choosing boot menu), just 16-bit
time in seconds
- Both values unprotected
- Most OSes provide tools to manipulate those values

???

- BootOrder describe which of the Boot#### should be first, second and so
  forth.
- You may recall that in UEFI BIOS setup menu we can change boot order, as a
  result of that operation this variable is changed. During next boot BDS phase
  Boot Manager use new order.

---

# Other variables

- `Capsule####` and `CapsuleMax` variables contain reports from Capsule update
reports. Where `CapsuleMax` specify last `Capsule####` variable

```c
typedef struct {
  UINT32     VariableTotalSize;  // There is more than this structure
  UINT32     Reserved;           // Alignment
  EFI_GUID   CapsuleGuid;
  EFI_TIME   CapsuleProcessed;   // Timestamp
  EFI_STATUS CapsuleStatus;
} EFI_CAPSULE_RESULT_VARIABLE_HEADER;

// Following part:

typedef struct {
  UINT16   Version; // structure version, 0x01

  UINT8    PayloadIndex;         // in Firmware Management Protocol
  UINT8    UpdateImageIndex;
  EFI_GUID UpdateImageTypeId;
  CHAR16 CapsuleFileName[];      // 0-terminated
  CHAR16 CapsuleTarget[];        // Text based device path
} EFI_CAPSULE_RESULT_VARIABLE_FMP;
```

.code-comment[ MdePkg/Include/Guid/CapsuleReport.h ]

???

More details are in the source code.

---

# Other variables

- Platform specific `PlatformCpuInfo` lets you easily read various information
about the platform

```c
typedef struct {
  BOOLEAN                            IsIntelProcessor;
  UINT8                              BrandString[MAXIMUM_CPU_BRAND_STRING_LENGTH + 1];
  UINT32                             CpuidMaxInputValue;
  UINT32                             CpuidMaxExtInputValue;
  EFI_CPU_UARCH                      CpuUarch;
  EFI_CPU_FAMILY                     CpuFamily;
  EFI_CPU_PLATFORM                   CpuPlatform;
  EFI_CPU_TYPE                       CpuType;
  EFI_CPU_VERSION_INFO               CpuVersion;
  EFI_CPU_CACHE_INFO                 CpuCache;
  EFI_CPU_FEATURES                   CpuFeatures;
  EFI_CPU_CSTATE_INFO                CpuCState;
  EFI_CPU_PACKAGE_INFO               CpuPackage;
  EFI_CPU_POWER_MANAGEMENT           CpuPowerManagement;
  EFI_CPU_ADDRESS_BITS               CpuAddress;
  EFI_MSR_FEATURES                   Msr;
} EFI_PLATFORM_CPU_INFO;
```

.code-comment[ edk2/Vlv2TbltDevicePkg/Include/Guid/PlatformCpuInfo.h ]

???

This is a nice shortcut as doesn't need to use assembly when calling the
same call with different parameters.

TODO for Arch4022: doing similar analysis of UEFI variable on Dell OptiPlex
could be informative

---

# Other variables

- `ConIn`, `ConOut` and `ConErr` contain console standard streams configuration,
`ConInDev`, `ConOutDev`, `ConErrDev` are volatile variables with actual choice.
Both base on `EFI_DEVICE_PATH_PROTOCOL` structure
- Those are only a few examples that are likely to be found, most of the
variables are platform/implementation specific

---

# Exercise #2: Creating variables from Linux

Creating variables is much more complicated that reading them. It can be done
with a one-liner, but to make it easier to understand, we will split this into
separate steps.

&#49;. Create attributes (non-volatile, runtime and bootservice access):

```shell
echo -en "\x07\x00\x00\x00" > var.attr
```

&#50;. Create value (or you may use existing file instead):

```shell
echo "hello world" > var.val
```

---

# Exercise #2: Creating variables from Linux

&#51;. Obtain new random GUID:

```shell
guid="$(cat /proc/sys/kernel/random/uuid)"
```

> Depending on use case, you may want to have a predefined GUID that is always
used for given variable. Note that some of them are reserved and may be blocked
by UEFI. An example of such reserved GUID is `gEfiGlobalVariableGuid`.

&#52;. Put them together, attributes first:

```shell
cat var.attr var.val > var
```

> If we were to use Authenticated Variables, we would have to insert serialized,
signed `EFI_VARIABLE_AUTHENTICATION_2` descriptor between attributes and value.
Creating such descriptor is complicated, and out of scope for this exercise.

---

# Exercise #2: Creating variables from Linux

&#53;. Do the write in one operation (`bs` parameter equals size of file):

```shell
dd if=var of="/sys/firmware/efi/efivars/Test-$guid" bs=$(stat -c %s var)
```

> In some cases you can `cp var /sys/firmware/efi/efivars/Test-$guid` and it may
work, depending on size of file. It may be good enough in this case, but to be
safe, always use `dd` with explicitly specified block size.

&#54;. Confirm that data was written successfully:

```shell
xxd /sys/firmware/efi/efivars/Test-$guid
```

Expected output is:

```shell
00000000: 0700 0000 6865 6c6c 6f20 776f 726c 640a  ....hello world.
```

---

# Exercise #3: Modifying variables from Linux

Trying to just remove the file we created results in error:

.code-13px[

```shell
localhost:~# rm /sys/firmware/efi/efivars/Test-$guid
rm: remove '/sys/firmware/efi/efivars/Test-1282c5c5-fa6d-4631-8139-697048c329f8'? y
rm: can't remove '/sys/firmware/efi/efivars/Test-1282c5c5-fa6d-4631-8139-697048c329f8': Operation not permitted
```

]

To be able to do so, we have to manually remove the `immutable` attribute from
this file:

.code-13px[

```shell
localhost:~# chattr -i /sys/firmware/efi/efivars/Test-$guid
localhost:~# rm /sys/firmware/efi/efivars/Test-$guid
localhost:~# ls /sys/firmware/efi/efivars/Test-$guid
ls: /sys/firmware/efi/efivars/Test-1282c5c5-fa6d-4631-8139-697048c329f8: No such file or directory
```

]

`chattr -i` also has to be used before changing the content of a variable, that
is before doing `dd` with updated value.

???

As mentioned earlier, efivarfs files are immutable by default "due to the
presence of numerous firmware bugs"

---

# Quiz #7

**What are variable attributes for?**

--

- Non-volatile access
- BootServices/Runtime access
- Authentication requirement

---

# References

- "A Tour Beyond BIOS Implementing UEFI Authenticated Variables in SMM with
EDKII", Jiewen Yao, Vincent J. Zimmer, September 2014
- UEFI Specification

---
class: center, middle, intro

# Q&A

---
class: center, middle, intro

# Thank you

