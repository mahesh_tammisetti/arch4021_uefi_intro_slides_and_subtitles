1
00:00:01,680 --> 00:00:07,680
Hello and welcome into Arch4021

2
00:00:05,120 --> 00:00:10,400
Introductory UEFI. I'm Piotr Król, and I will

3
00:00:07,680 --> 00:00:13,679
be your trainer during this course.

4
00:00:10,400 --> 00:00:15,839
What are the goals of this training?

5
00:00:13,679 --> 00:00:18,000
First of all we would like to learn

6
00:00:15,839 --> 00:00:20,320
fundamentals of

7
00:00:18,000 --> 00:00:23,119
firmware. What is firmware how it works,

8
00:00:20,320 --> 00:00:25,439
where it is stored and some similar

9
00:00:23,119 --> 00:00:28,080
basics. Then we will talk about UEFI

10
00:00:25,439 --> 00:00:31,359
basics like what are the goals of

11
00:00:28,080 --> 00:00:33,600
UEFI specification what are the

12
00:00:31,359 --> 00:00:36,480
basic components of it and what's the

13
00:00:33,600 --> 00:00:37,440
history behind UEFI specification and the

14
00:00:36,480 --> 00:00:39,680
previous

15
00:00:37,440 --> 00:00:41,040
ones, what is the architect architecture

16
00:00:39,680 --> 00:00:43,040
of UEFI

17
00:00:41,040 --> 00:00:44,480
and we will also discuss some

18
00:00:43,040 --> 00:00:47,520
implementations,

19
00:00:44,480 --> 00:00:48,879
the reference implementations and

20
00:00:47,520 --> 00:00:51,680
many others.

21
00:00:48,879 --> 00:00:55,280
Also we will discuss what is criticism

22
00:00:51,680 --> 00:00:56,559
pointed into UEFI and

23
00:00:55,280 --> 00:00:59,600
what are the

24
00:00:56,559 --> 00:01:01,039
main concerns of people criticizing this

25
00:00:59,600 --> 00:01:03,280
Specification.

26
00:01:01,039 --> 00:01:06,400
Finally, we will discuss boot flow all

27
00:01:03,280 --> 00:01:08,880
the phases which are related to

28
00:01:06,400 --> 00:01:11,760
booting the

29
00:01:08,880 --> 00:01:13,360
modern platform using UEFI

30
00:01:11,760 --> 00:01:15,759
BIOS.

31
00:01:13,360 --> 00:01:18,640
From practical point of view, we will

32
00:01:15,759 --> 00:01:21,520
learn how to build a UEFI reference

33
00:01:18,640 --> 00:01:22,960
implementation called EDK2 as well as

34
00:01:21,520 --> 00:01:25,840
how to debug it

35
00:01:22,960 --> 00:01:27,600
and we will also

36
00:01:25,840 --> 00:01:31,680
look into

37
00:01:27,600 --> 00:01:34,880
how images are built and how

38
00:01:31,680 --> 00:01:37,439
they look inside and how to decompose

39
00:01:34,880 --> 00:01:38,799
those images to find

40
00:01:37,439 --> 00:01:40,720
components

41
00:01:38,799 --> 00:01:43,040
that are

42
00:01:40,720 --> 00:01:45,040
in those images that building this

43
00:01:43,040 --> 00:01:48,000
UEFI BIOS.

44
00:01:45,040 --> 00:01:49,680
Then we will talk about key structures

45
00:01:48,000 --> 00:01:50,960
used inside

46
00:01:49,680 --> 00:01:54,560
boot process,

47
00:01:50,960 --> 00:01:57,200
as well as what are the boot phases and

48
00:01:54,560 --> 00:01:59,600
how to find them in the boot log how to

49
00:01:57,200 --> 00:02:00,960
recognize what kind of features they

50
00:01:59,600 --> 00:02:03,759
Have.

51
00:02:00,960 --> 00:02:05,759
So, why to take this class first of all

52
00:02:03,759 --> 00:02:08,399
you may be interested in discuss because

53
00:02:05,759 --> 00:02:10,560
you want to learn basics of the boot

54
00:02:08,399 --> 00:02:11,920
process of your of your computer maybe

55
00:02:10,560 --> 00:02:15,200
you are interested what's going on

56
00:02:11,920 --> 00:02:17,840
before operating system start maybe you

57
00:02:15,200 --> 00:02:20,319
are at the beginning of your path in

58
00:02:17,840 --> 00:02:23,599
firmware development firmware engineering

59
00:02:20,319 --> 00:02:27,200
or firmware security or researching and

60
00:02:23,599 --> 00:02:28,160
looking for bugs inside firmware.

61
00:02:27,200 --> 00:02:30,640

62
00:02:28,160 --> 00:02:31,840
It is also important to

63
00:02:30,640 --> 00:02:35,120
understand

64
00:02:31,840 --> 00:02:38,400
what is the role of UEFI in context

65
00:02:35,120 --> 00:02:41,760
of open-source firmware ecosystem and

66
00:02:38,400 --> 00:02:44,160
community because there are multiple

67
00:02:41,760 --> 00:02:45,280
alternative approaches to boot the

68
00:02:44,160 --> 00:02:47,680
platform

69
00:02:45,280 --> 00:02:48,440
so for example there is other lecture

70
00:02:47,680 --> 00:02:51,680
Called,

71
00:02:48,440 --> 00:02:53,519
“R4031” talking about core boot which is

72
00:02:51,680 --> 00:02:55,599

73
00:02:53,519 --> 00:02:58,080
alternative implementation of

74
00:02:55,599 --> 00:03:00,159
boot process. Not exactly complying with

75
00:02:58,080 --> 00:03:03,840
UEFI specification, although there are

76
00:03:00,159 --> 00:03:05,760
some similarities. Knowing multiple

77
00:03:03,840 --> 00:03:08,959
implementations uh

78
00:03:05,760 --> 00:03:10,720
makes you makes you understand better

79
00:03:08,959 --> 00:03:12,879
the boot process of modern

80
00:03:10,720 --> 00:03:14,879
computing systems and then if you

81
00:03:12,879 --> 00:03:17,280
understand this process you can make it

82
00:03:14,879 --> 00:03:19,599
more secure make it more reliable and

83
00:03:17,280 --> 00:03:21,360

84
00:03:19,599 --> 00:03:23,200
improve its quality and

85
00:03:21,360 --> 00:03:25,200
improve its implementation finally you

86
00:03:23,200 --> 00:03:27,840
may feel some satisfaction from

87
00:03:25,200 --> 00:03:29,760
understanding the nitty-gritty ideas

88
00:03:27,840 --> 00:03:32,799
details which are behind

89
00:03:29,760 --> 00:03:32,799
the scene.


