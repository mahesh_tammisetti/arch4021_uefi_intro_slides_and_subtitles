1
00:00:00,080 --> 00:00:05,359
So and we get to the point of

2
00:00:02,720 --> 00:00:09,519
basic input output system. So the BIOS

3
00:00:05,359 --> 00:00:10,960
term was first introduced in 1975 by

4
00:00:09,519 --> 00:00:14,559
Gary Kildall

5
00:00:10,960 --> 00:00:18,000
in CP/M OS

6
00:00:14,559 --> 00:00:20,480
which was predecessor of MS-DOS and

7
00:00:18,000 --> 00:00:25,279
this is very interesting because

8
00:00:20,480 --> 00:00:28,400
Gary Kildall get 

9
00:00:25,279 --> 00:00:31,599
IEEE award for this invention

10
00:00:28,400 --> 00:00:33,680
and I really um recommend to

11
00:00:31,599 --> 00:00:35,440
read story about

12
00:00:33,680 --> 00:00:39,200
CP/M OS

13
00:00:35,440 --> 00:00:40,239
and how the BIOS was

14
00:00:39,200 --> 00:00:43,120
invented.

15
00:00:40,239 --> 00:00:46,480
So, the BIOS was the name of the boot

16
00:00:43,120 --> 00:00:49,760
firmware implementation on x86 used for

17
00:00:46,480 --> 00:00:51,680
over 30 years. This term is

18
00:00:49,760 --> 00:00:53,600
familiar even to

19
00:00:51,680 --> 00:00:55,760
some people which are not very technical,

20
00:00:53,600 --> 00:00:58,879
so they understand that during the boot

21
00:00:55,760 --> 00:01:00,640
process you have this

22
00:00:58,879 --> 00:01:03,199


23
00:01:00,640 --> 00:01:05,360
blue gray window. But, right now of course

24
00:01:03,199 --> 00:01:07,360
the BIOS is more and more sophisticated

25
00:01:05,360 --> 00:01:09,600
it's starting to be graphical user

26
00:01:07,360 --> 00:01:10,880
interface. That's why

27
00:01:09,600 --> 00:01:13,600


28
00:01:10,880 --> 00:01:16,080
the BIOS term is always often used as a

29
00:01:13,600 --> 00:01:18,000
synonym of the boot firmware. We

30
00:01:16,080 --> 00:01:20,240
rather like the boot firmware because

31
00:01:18,000 --> 00:01:23,759
it's more clear what it is really,

32
00:01:20,240 --> 00:01:26,560
the BIOS is rather very x86

33
00:01:23,759 --> 00:01:29,119
and legacy term.

34
00:01:26,560 --> 00:01:30,560
So, the BIOS was written in pure assembly,

35
00:01:29,119 --> 00:01:31,640
it needed very deep knowledge and

36
00:01:30,560 --> 00:01:34,079
debugging

37
00:01:31,640 --> 00:01:35,200


38
00:01:34,079 --> 00:01:36,799
skills

39
00:01:35,200 --> 00:01:40,159


40
00:01:36,799 --> 00:01:40,960
to develop to maintain.

41
00:01:40,159 --> 00:01:43,520


42
00:01:40,960 --> 00:01:45,280
So, the development of the BIOS was very

43
00:01:43,520 --> 00:01:48,479
time consuming,

44
00:01:45,280 --> 00:01:50,079
not very portable

45
00:01:48,479 --> 00:01:53,280
and the BIOS provided hardware

46
00:01:50,079 --> 00:01:55,759
initialization, configuration, utilities

47
00:01:53,280 --> 00:01:57,680
and runtime services, which could be used

48
00:01:55,759 --> 00:02:00,479
by the operating system. We will talk about

49
00:01:57,680 --> 00:02:02,399
that how the modern UEFI

50
00:02:00,479 --> 00:02:03,280
replacement of the BIOS providing

51
00:02:02,399 --> 00:02:04,399
that.

52
00:02:03,280 --> 00:02:07,040


53
00:02:04,399 --> 00:02:10,800
So, BIOS was looking for the bootloader

54
00:02:07,040 --> 00:02:14,400
in the first 512 bytes of the boot

55
00:02:10,800 --> 00:02:16,640
media, which could be some floppy disk or

56
00:02:14,400 --> 00:02:17,680
or hard disk

57
00:02:16,640 --> 00:02:20,160


58
00:02:17,680 --> 00:02:22,400
and in that way it it passed control to

59
00:02:20,160 --> 00:02:24,800
some to operating system.

60
00:02:22,400 --> 00:02:26,800
So there are BIOS like

61
00:02:24,800 --> 00:02:29,280
WinZent firmware

62
00:02:26,800 --> 00:02:32,480
which use exactly the same methodology

63
00:02:29,280 --> 00:02:34,319
of pure assembly implementation. And

64
00:02:32,480 --> 00:02:37,120
by the way WinZent

65
00:02:34,319 --> 00:02:40,319
BIOS is very well known from rapid boot

66
00:02:37,120 --> 00:02:41,120
process and is extremely fast.

67
00:02:40,319 --> 00:02:43,760


68
00:02:41,120 --> 00:02:45,519
But, it's still like our assembly

69
00:02:43,760 --> 00:02:46,720
implementation of the

70
00:02:45,519 --> 00:02:47,760
of the BIOS.

71
00:02:46,720 --> 00:02:49,680
And

72
00:02:47,760 --> 00:02:52,080
back in the days BIOS were typically

73
00:02:49,680 --> 00:02:55,599
closed source. There was proprietary

74
00:02:52,080 --> 00:02:58,480
license related to it, and

75
00:02:55,599 --> 00:03:02,000
definitely delivered only in

76
00:02:58,480 --> 00:03:05,760
binary form. So, from 1975

77
00:03:02,000 --> 00:03:10,319
we’re just like jumping very fast

78
00:03:05,760 --> 00:03:13,360
to 1990, where we suddenly got 32-bit

79
00:03:10,319 --> 00:03:15,519
processors with protected

80
00:03:13,360 --> 00:03:16,720
mode.

81
00:03:15,519 --> 00:03:19,040
We have

82
00:03:16,720 --> 00:03:21,599
way more memory than

83
00:03:19,040 --> 00:03:22,720
all BIOS can handle

84
00:03:21,599 --> 00:03:25,519
than

85
00:03:22,720 --> 00:03:26,879
those applications which are delivered

86
00:03:25,519 --> 00:03:29,760
are

87
00:03:26,879 --> 00:03:31,840
capable of fully accessing the hardware.

88
00:03:29,760 --> 00:03:33,680
Thanks to BIOS services.

89
00:03:31,840 --> 00:03:36,159
We’re facing

90
00:03:33,680 --> 00:03:37,040
upcoming Windows 95

91
00:03:36,159 --> 00:03:39,599
and

92
00:03:37,040 --> 00:03:40,400
really complexity explodes because of

93
00:03:39,599 --> 00:03:42,720
the

94
00:03:40,400 --> 00:03:45,200
more hardware, more needs, more

95
00:03:42,720 --> 00:03:47,760
sophisticated software running. In the

96
00:03:45,200 --> 00:03:50,319
mid 1990s

97
00:03:47,760 --> 00:03:52,560
first time extensible firmware

98
00:03:50,319 --> 00:03:55,840
interface concept appears

99
00:03:52,560 --> 00:03:58,239
because Intel starts working on EFI to

100
00:03:55,840 --> 00:04:01,280
replace BIOS because they have this

101
00:03:58,239 --> 00:04:05,280
project with HP called, “Itanium”

102
00:04:01,280 --> 00:04:08,959
which was 64-bit processor

103
00:04:05,280 --> 00:04:12,080
for high performance computing

104
00:04:08,959 --> 00:04:14,799
but unfortunately Itanium eventually

105
00:04:12,080 --> 00:04:17,519
fall apart and was not very successful.

106
00:04:14,799 --> 00:04:19,759
But from those days we got the

107
00:04:17,519 --> 00:04:23,759
predecessor of modern UEFI which was

108
00:04:19,759 --> 00:04:26,479
called EFI extensible firmware interface.

109
00:04:23,759 --> 00:04:29,520
So access to EFI specification at that

110
00:04:26,479 --> 00:04:31,040
point was close to Intel partners

111
00:04:29,520 --> 00:04:34,479
then

112
00:04:31,040 --> 00:04:36,560
in 2005 there was EFI 1.1

113
00:04:34,479 --> 00:04:38,560
release

114
00:04:36,560 --> 00:04:40,800
and this was last

115
00:04:38,560 --> 00:04:41,600
EFI specification released because after

116
00:04:40,800 --> 00:04:44,639
that

117
00:04:41,600 --> 00:04:47,360
there was a UEFI forum established which

118
00:04:44,639 --> 00:04:49,919
initially got ARM, AMD, HP, Lenovo

119
00:04:47,360 --> 00:04:52,320
Phoenix, Microsoft, IBM and some

120
00:04:49,919 --> 00:04:53,919
other partners.

121
00:04:52,320 --> 00:04:56,639
And

122
00:04:53,919 --> 00:04:59,759
this UEFI forum

123
00:04:56,639 --> 00:05:03,440
obtained EFI specification as a

124
00:04:59,759 --> 00:05:06,800
contribution from Intel and then unified

125
00:05:03,440 --> 00:05:09,840
accessible firmware interface forum

126
00:05:06,800 --> 00:05:11,759
was born and after that

127
00:05:09,840 --> 00:05:16,800
we could wait for

128
00:05:11,759 --> 00:05:16,800
versions of UEFI specification.



