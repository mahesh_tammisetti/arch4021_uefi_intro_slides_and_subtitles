1
00:00:00,000 --> 00:00:03,279
So, we discussed

2
00:00:01,839 --> 00:00:06,800
initial

3
00:00:03,279 --> 00:00:08,960
EFI goals and let's now go to current

4
00:00:06,800 --> 00:00:10,319
UEFI goals and compare how it is

5
00:00:08,960 --> 00:00:12,000
different from

6
00:00:10,319 --> 00:00:14,719
from the initial goals.

7
00:00:12,000 --> 00:00:16,959
So, first specification aims to

8
00:00:14,719 --> 00:00:18,719
define a complete solution for the

9
00:00:16,959 --> 00:00:20,879
firmware to describe all platform

10
00:00:18,719 --> 00:00:23,520
features and provide that to

11
00:00:20,879 --> 00:00:26,400
operating system incoherent way. So the

12
00:00:23,520 --> 00:00:28,400
goal is to have one way to describe

13
00:00:26,400 --> 00:00:29,759
multiple platform designs, if we have

14
00:00:28,400 --> 00:00:33,599
multiple

15
00:00:29,759 --> 00:00:36,399
platform designs, all those

16
00:00:33,599 --> 00:00:38,320
expose the platform defined features in

17
00:00:36,399 --> 00:00:39,919
the same way to operating

18
00:00:38,320 --> 00:00:42,000
system or to

19
00:00:39,919 --> 00:00:44,079
bootloader.

20
00:00:42,000 --> 00:00:46,960
Second specification goal is to define

21
00:00:44,079 --> 00:00:49,040
interfaces to platform capabilities

22
00:00:46,960 --> 00:00:51,280
those interfaces should simplify design

23
00:00:49,040 --> 00:00:52,399
and implementation of the operating

24
00:00:51,280 --> 00:00:54,399
system

25
00:00:52,399 --> 00:00:56,559
loaders, and

26
00:00:54,399 --> 00:00:58,559
those also should provide stable

27
00:00:56,559 --> 00:01:00,320
boundary between

28
00:00:58,559 --> 00:01:02,079
the platform

29
00:01:00,320 --> 00:01:03,200
hardware firmware and the operating

30
00:01:02,079 --> 00:01:05,999
system.

31
00:01:03,200 --> 00:01:07,840
So, implementation do not exactly follow

32
00:01:05,999 --> 00:01:08,800
these goals because

33
00:01:07,840 --> 00:01:10,639

34
00:01:11,280 --> 00:01:15,200
there is division there is problem

35
00:01:10,639 --> 00:01:14,240
with the division of labor between

36
00:01:12,720 --> 00:01:15,679
operating system loader and the

37
00:01:14,240 --> 00:01:18,240
operating system

38
00:01:15,679 --> 00:01:21,360
kernel. We will discuss that

39
00:01:18,240 --> 00:01:24,720
later, especially in the boot device

40
00:01:21,360 --> 00:01:26,480
selection boot stage, where

41
00:01:24,720 --> 00:01:28,480
we will mention that

42
00:01:26,480 --> 00:01:31,440
there is question who should call 

43
00:01:28,480 --> 00:01:35,840
ExitBootServices() function but maybe right

44
00:01:31,440 --> 00:01:38,080
now this is to too much detail.

45
00:01:35,840 --> 00:01:39,919
Third, reasonable device abstraction free of

46
00:01:38,080 --> 00:01:41,279
legacy interfaces

47
00:01:39,919 --> 00:01:43,279

48
00:01:43,759 --> 00:01:47,680
this goal

49
00:01:43,279 --> 00:01:46,720
should make operating system loaders

50
00:01:45,200 --> 00:01:48,880
more portable

51
00:01:46,720 --> 00:01:51,599
since its operation should be abstracted

52
00:01:48,880 --> 00:01:54,639
from specific hardware implementation.

53
00:01:51,599 --> 00:01:56,400
We can install and boot most of 

54
00:01:54,639 --> 00:01:58,800
the operating system right now on

55
00:01:56,400 --> 00:02:00,560
modern hardware, so it seems that this

56
00:01:58,800 --> 00:02:03,040
goal is

57
00:02:00,560 --> 00:02:06,800
quite well implemented and quite well

58
00:02:03,040 --> 00:02:09,119
defined by UEFI forum. Fourth goal,

59
00:02:06,800 --> 00:02:10,240
abstraction of option ROMs from the

60
00:02:09,119 --> 00:02:11,520
firmware.

61
00:02:10,240 --> 00:02:13,280

62
00:02:14,000 --> 00:02:18,879
This goal leads to

63
00:02:13,280 --> 00:02:18,960
defining in specification interfaces

64
00:02:16,399 --> 00:02:21,280
that can abstract standard buses like

65
00:02:18,960 --> 00:02:23,119
PCI, USB, SCSI

66
00:02:21,280 --> 00:02:25,440
and

67
00:02:23,119 --> 00:02:26,320
using those bus interfaces

68
00:02:25,440 --> 00:02:28,160

69
00:02:28,800 --> 00:02:33,120
there is a standard way to discover

70
00:02:28,160 --> 00:02:34,640
option ROMs and also standard way to

71
00:02:30,640 --> 00:02:37,520
consume those and interact with them.

72
00:02:34,640 --> 00:02:41,119
There's also this extensibility built

73
00:02:37,520 --> 00:02:43,920
into the specification because

74
00:02:41,119 --> 00:02:46,399
the abstraction for the bus itself

75
00:02:43,920 --> 00:02:48,479
if there will be even ever growing

76
00:02:46,399 --> 00:02:50,399
number of buses

77
00:02:48,479 --> 00:02:53,440
is already in the specifications so

78
00:02:50,399 --> 00:02:55,039
there is no problem with adding new

79
00:02:53,440 --> 00:02:56,000
new buses.

80
00:02:55,039 --> 00:02:57,280

81
00:02:58,480 --> 00:03:02,720
So,

82
00:02:57,280 --> 00:03:03,039
these goals lead to better consumption

83
00:03:00,240 --> 00:03:05,839
of option ROMs and better

84
00:03:03,039 --> 00:03:07,200
discoverability and execution of those.

85
00:03:05,839 --> 00:03:08,720
So, last goal

86
00:03:07,200 --> 00:03:11,119
architecturally sharable system

87
00:03:08,720 --> 00:03:12,960
partition, led to creation of

88
00:03:11,119 --> 00:03:14,320
additional partition to hold software

89
00:03:12,960 --> 00:03:15,920
that can be

90
00:03:14,320 --> 00:03:16,880
used by firmware.

91
00:03:15,920 --> 00:03:18,960
So, this

92
00:03:16,880 --> 00:03:22,160
technique creates

93
00:03:18,960 --> 00:03:24,880
not so constrained space on the on the

94
00:03:22,160 --> 00:03:27,600
hard disk so the space is not so

95
00:03:24,880 --> 00:03:28,720
constrained in comparison to SPI flash

96
00:03:27,600 --> 00:03:33,200
size.

97
00:03:28,720 --> 00:03:36,479
And this hard disk partition um

98
00:03:33,200 --> 00:03:38,479
can deliver us a software that can be

99
00:03:36,479 --> 00:03:41,039
consumed by firmware

100
00:03:38,479 --> 00:03:43,920
so we typically use that for

101
00:03:41,039 --> 00:03:47,759
bootloader for operating system

102
00:03:43,920 --> 00:03:50,399
kernel, for RAM disk and and large

103
00:03:47,759 --> 00:03:51,920
various other shareable

104
00:03:50,399 --> 00:03:53,759

105
00:03:54,400 --> 00:03:58,159
binaries, shareable software that

106
00:03:53,759 --> 00:03:56,640
can be used that and can be consumed by

107
00:03:55,679 --> 00:03:58,959
firmware.

108
00:03:56,640 --> 00:04:01,920
And also this partition can be used for

109
00:03:58,959 --> 00:04:05,360
firmware update, so definitely this was

110
00:04:01,920 --> 00:04:08,000
interesting uh goal which was already

111
00:04:05,360 --> 00:04:09,280
achieved and probably

112
00:04:08,000 --> 00:04:12,240

113
00:04:11,760 --> 00:04:18,239
can be leveraged for various extension

114
00:04:12,240 --> 00:04:17,999
of the UEFI specification.

115
00:04:15,759 --> 00:04:21,040
It is worth to mention that

116
00:04:17,999 --> 00:04:25,120
specifications want to or

117
00:04:21,040 --> 00:04:27,040
specification authors claim to

118
00:04:25,120 --> 00:04:29,999
follow the evolutionary, not

119
00:04:27,040 --> 00:04:29,999
revolutionary

120
00:04:30,159 --> 00:04:34,159
approach

121
00:04:31,439 --> 00:04:35,120
and so they anticipate long transition

122
00:04:34,159 --> 00:04:36,080
between

123
00:04:35,120 --> 00:04:38,320

124
00:04:38,560 --> 00:04:41,520
between one version and another between

125
00:04:38,320 --> 00:04:41,439
the

126
00:04:39,040 --> 00:04:43,920
features and between

127
00:04:41,439 --> 00:04:44,960
extensions of the specification.

128
00:04:43,920 --> 00:04:48,080

129
00:04:47,440 --> 00:04:53,199
The main goal is to maintain current

130
00:04:48,080 --> 00:04:52,400
partners advantage so whatever ecosystem

131
00:04:50,719 --> 00:04:54,080
was built so far,

132
00:04:52,400 --> 00:04:54,960
it should be maintained

133
00:04:54,080 --> 00:04:56,240
and

134
00:04:54,960 --> 00:04:58,080
they want to have

135
00:04:56,240 --> 00:05:01,439
compatibility by design.

136
00:04:58,080 --> 00:05:03,120
Of course this leads to

137
00:05:01,439 --> 00:05:04,800
creating

138
00:05:03,120 --> 00:05:07,760
functions with the

139
00:05:04,800 --> 00:05:08,960
number at the end. So, for example we have

140
00:05:07,760 --> 00:05:11,760
various

141
00:05:08,960 --> 00:05:13,040
functions which end with

142
00:05:11,760 --> 00:05:14,559
two or three

143
00:05:13,040 --> 00:05:16,559
because there are new versions of those

144
00:05:14,559 --> 00:05:19,279
functions.

145
00:05:16,559 --> 00:05:20,480
And for a compatibility the older one

146
00:05:19,279 --> 00:05:23,680
are left

147
00:05:20,480 --> 00:05:25,999
in the spec and then

148
00:05:23,680 --> 00:05:28,559
the other goal is to

149
00:05:25,999 --> 00:05:31,600
simplify addition of OS neutral platform

150
00:05:28,559 --> 00:05:32,400
value added software so this

151
00:05:31,600 --> 00:05:35,760

152
00:05:34,880 --> 00:05:40,000
sharable partition helps with that

153
00:05:35,760 --> 00:05:39,840
and of course this is also needed for

154
00:05:37,520 --> 00:05:41,840
the partners for the ecosystem

155
00:05:39,840 --> 00:05:43,760
that was already built by

156
00:05:41,840 --> 00:05:46,800
UEFI forum members or

157
00:05:43,760 --> 00:05:48,240
before even by EFI

158
00:05:46,800 --> 00:05:50,960
creators.

159
00:05:48,240 --> 00:05:53,520
And they also don't want to throw

160
00:05:50,960 --> 00:05:54,800
away whatever was built so far, so they

161
00:05:53,520 --> 00:05:57,760
don't want to

162
00:05:54,800 --> 00:06:00,880
waste already spent money

163
00:05:57,760 --> 00:06:03,439
and want to continue some

164
00:06:00,880 --> 00:06:05,839
ideas which are in the ecosystem. Which

165
00:06:03,439 --> 00:06:09,279
may be good may be bad depending on the

166
00:06:05,839 --> 00:06:11,760
directions and depending on the

167
00:06:09,279 --> 00:06:14,400
what really

168
00:06:11,760 --> 00:06:16,159
features are blocked in long run.

169
00:06:14,400 --> 00:06:18,400
So,

170
00:06:16,159 --> 00:06:20,800
the minimalism of the interfaces is

171
00:06:18,400 --> 00:06:22,159
quite far from reality right now

172
00:06:20,800 --> 00:06:24,320
because 

173
00:06:22,159 --> 00:06:27,040
it's very hard to

174
00:06:24,320 --> 00:06:28,800
create a wide range of support, flexible

175
00:06:27,040 --> 00:06:30,719
support

176
00:06:28,800 --> 00:06:32,559
when we

177
00:06:30,719 --> 00:06:34,559
claim to have minimal interfaces so

178
00:06:32,559 --> 00:06:35,999
either we have wide range of support

179
00:06:34,559 --> 00:06:37,999
with

180
00:06:35,999 --> 00:06:41,120
flexible interfaces either we have

181
00:06:37,999 --> 00:06:41,120
minimal interfaces. 

