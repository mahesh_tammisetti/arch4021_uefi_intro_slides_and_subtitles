1
00:00:00,160 --> 00:00:05,839
So, let's try to untangle the

2
00:00:02,399 --> 00:00:08,240
terminology related to UEFI.

3
00:00:05,839 --> 00:00:11,679
Many people usually

4
00:00:08,240 --> 00:00:14,160
confuse EFI UEFI EDK2 and the kind of

5
00:00:11,679 --> 00:00:17,039
using that in an interchangeable way

6
00:00:14,160 --> 00:00:18,160
there's also term Tianocore, so let's

7
00:00:17,039 --> 00:00:20,400
dive into

8
00:00:18,160 --> 00:00:23,840
explanation what does it mean.

9
00:00:20,400 --> 00:00:28,000
So, UEFI PI and UEFI are specification

10
00:00:23,840 --> 00:00:29,760
maintained by UEFI forum. UEFI PI is a

11
00:00:28,000 --> 00:00:30,400
platform integration specifications

12
00:00:29,760 --> 00:00:31,279
which

13
00:00:30,400 --> 00:00:32,960
cover

14
00:00:31,279 --> 00:00:35,920
various topics, there are multiple

15
00:00:32,960 --> 00:00:39,520
topics covered there. Mostly

16
00:00:35,920 --> 00:00:41,680
the specification talking about

17
00:00:39,520 --> 00:00:45,440
core components of the

18
00:00:41,680 --> 00:00:47,840
PEI phase and DXE phase and we will

19
00:00:45,440 --> 00:00:49,760
dive into specifics of that.

20
00:00:47,840 --> 00:00:52,800
It also talks about shared

21
00:00:49,760 --> 00:00:56,079
elements both 

22
00:00:52,800 --> 00:00:57,600
that can be found on

23
00:00:56,079 --> 00:00:59,680
the

24
00:00:57,600 --> 00:01:02,879
architecturally sharable partition as

25
00:00:59,680 --> 00:01:05,199
well as in the

26
00:01:02,879 --> 00:01:08,720
firmware storage like SPI

27
00:01:05,199 --> 00:01:11,520
so it talks about firmware volumes and

28
00:01:08,720 --> 00:01:12,720
how inside firmware

29
00:01:11,520 --> 00:01:14,479
various firmware files can be

30
00:01:12,720 --> 00:01:16,000
represented and so on and so on, we will

31
00:01:14,479 --> 00:01:18,560
discuss that later.

32
00:01:16,000 --> 00:01:20,479
Also management mode which is like many

33
00:01:18,560 --> 00:01:23,680
of us probably know system management

34
00:01:20,479 --> 00:01:24,799
mode but management mode is a more

35
00:01:23,680 --> 00:01:28,000
generic

36
00:01:24,799 --> 00:01:29,439
idea of

37
00:01:28,000 --> 00:01:32,479
which comes from

38
00:01:29,439 --> 00:01:34,880
SMM, but also applies to other

39
00:01:32,479 --> 00:01:36,720
architectures. So, if UEFI is a

40
00:01:34,880 --> 00:01:40,240
specification which

41
00:01:36,720 --> 00:01:42,479
just treat about interface between

42
00:01:40,240 --> 00:01:44,960
operating system and the firmware and

43
00:01:42,479 --> 00:01:47,759
the goals of firmware in this

44
00:01:44,960 --> 00:01:51,200
communication is to expose platform,

45
00:01:47,759 --> 00:01:54,240
features platform capabilities.

46
00:01:51,200 --> 00:01:57,119
and this specification supports multiple

47
00:01:54,240 --> 00:01:57,119
architectures.

48
00:01:57,439 --> 00:02:01,680
So, what's UEFI forum? UEFI forum is a

49
00:02:00,079 --> 00:02:04,399
alliance between

50
00:02:01,680 --> 00:02:07,040
several leading technology companies

51
00:02:04,399 --> 00:02:10,239
those that alliance maintains multiple

52
00:02:07,040 --> 00:02:12,160
specification, 

53
00:02:10,239 --> 00:02:13,760
most well known are two that

54
00:02:12,160 --> 00:02:15,920
that I mentioned on previous

55
00:02:13,760 --> 00:02:18,480
slide but there is also UEFI shell

56
00:02:15,920 --> 00:02:20,879
specification there is also a UEFI

57
00:02:18,480 --> 00:02:23,280
platform initialization distributions

58
00:02:20,879 --> 00:02:25,680
packaging specification, there is ACPI

59
00:02:23,280 --> 00:02:28,000
specification which is related to that

60
00:02:25,680 --> 00:02:28,000
.

61
00:02:28,080 --> 00:02:33,599
to that group to that alliance.

62
00:02:30,959 --> 00:02:35,920
TianoCore is a community that

63
00:02:33,599 --> 00:02:38,000
supports an open source

64
00:02:35,920 --> 00:02:40,319
implementation of UEFI

65
00:02:38,000 --> 00:02:42,000
and that's very interesting because

66
00:02:40,319 --> 00:02:45,040
you may be a

67
00:02:42,000 --> 00:02:46,400
community member, you can develop code

68
00:02:45,040 --> 00:02:49,760
and

69
00:02:46,400 --> 00:02:50,720
get your patches applied by TianoCore

70
00:02:49,760 --> 00:02:52,000
community to

71
00:02:50,720 --> 00:02:55,599
UEFI

72
00:02:52,000 --> 00:02:56,879
open source reference implementation. But,

73
00:02:55,599 --> 00:03:00,560
in the same

74
00:02:56,879 --> 00:03:04,720
time you may be not the UEFI forum

75
00:03:00,560 --> 00:03:07,599
member, so those two bodies are different

76
00:03:04,720 --> 00:03:09,440
and have different powers. So TianoCore

77
00:03:07,599 --> 00:03:12,080
is more about implementation and

78
00:03:09,440 --> 00:03:14,800
forum is about specification.

79
00:03:12,080 --> 00:03:17,519
EDKII is open source

80
00:03:14,800 --> 00:03:20,239
to 2-clause BSD reference implementation of

81
00:03:17,519 --> 00:03:20,959
UEFI and PI specification is written in

82
00:03:20,239 --> 00:03:23,519
C.

83
00:03:20,959 --> 00:03:25,680
Typically it is not enough to build

84
00:03:23,519 --> 00:03:27,440
fully functional firmware maybe except 

85
00:03:25,680 --> 00:03:29,120
some virtualized environment which we

86
00:03:27,440 --> 00:03:30,080
will use in this course.

87
00:03:29,120 --> 00:03:32,159


88
00:03:30,080 --> 00:03:35,040
But,

89
00:03:32,159 --> 00:03:38,239
contain enough code to create some

90
00:03:35,040 --> 00:03:40,640
fundamental components and like for

91
00:03:38,239 --> 00:03:42,720
example PCI support, USB support as we

92
00:03:40,640 --> 00:03:46,319
mentioned in the goals of the

93
00:03:42,720 --> 00:03:49,120
specification. The EDK2 code

94
00:03:46,319 --> 00:03:51,440
is maintained and supported by

95
00:03:49,120 --> 00:03:53,760
the TianoCore community.

96
00:03:51,440 --> 00:03:56,799
So, that's the difference the

97
00:03:53,760 --> 00:03:59,120
UEFI forum, TianoCore is a community, UEFI forum is

98
00:03:56,799 --> 00:04:01,599
an alliance. TianoCore is a community

99
00:03:59,120 --> 00:04:04,400
around the reference implementation

100
00:04:01,599 --> 00:04:08,720
about implementation of specification

101
00:04:04,400 --> 00:04:11,120
made by UEFI forum and EDK2 is 

102
00:04:08,720 --> 00:04:14,000
an implementation of UEFI and PI

103
00:04:11,120 --> 00:04:14,000
specifications.



