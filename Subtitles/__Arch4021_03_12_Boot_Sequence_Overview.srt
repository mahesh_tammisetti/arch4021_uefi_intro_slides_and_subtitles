1
00:00:02,159 --> 00:00:07,600
So, how boot sequence looks like from

2
00:00:05,200 --> 00:00:11,120
perspective of the

3
00:00:07,600 --> 00:00:13,759
UEFI specification

4
00:00:11,120 --> 00:00:14,920
designers.

5
00:00:13,759 --> 00:00:16,480


6
00:00:14,920 --> 00:00:18,000
So,

7
00:00:16,480 --> 00:00:20,880
the boot process

8
00:00:18,000 --> 00:00:23,119
is divided in two components.

9
00:00:20,880 --> 00:00:26,640
First component is

10
00:00:23,119 --> 00:00:29,359
boot manager and second component are EFI

11
00:00:26,640 --> 00:00:32,880
binaries. So there are multiple

12
00:00:29,359 --> 00:00:36,079
values EFI binaries and on this diagram

13
00:00:32,880 --> 00:00:37,680
the time pass from left to right.

14
00:00:36,079 --> 00:00:40,559


15
00:00:37,680 --> 00:00:42,879
Boot manager are those components of

16
00:00:40,559 --> 00:00:44,640
UEFI which are

17
00:00:42,879 --> 00:00:46,800
responsible for

18
00:00:44,640 --> 00:00:49,760
loading, which are generic, which are

19
00:00:46,800 --> 00:00:50,640
responsible for loading for doing some

20
00:00:49,760 --> 00:00:52,640


21
00:00:50,640 --> 00:00:55,920
initial basic stuff.

22
00:00:52,640 --> 00:00:57,280
So, in case of this diagram we have a

23
00:00:55,920 --> 00:01:00,160
standard platform firmware

24
00:00:57,280 --> 00:01:01,199
initialization in the platform init

25
00:01:00,160 --> 00:01:03,039
circle

26
00:01:01,199 --> 00:01:05,199
which is like a basic

27
00:01:03,039 --> 00:01:06,320
initialization of the platform, it may

28
00:01:05,199 --> 00:01:09,840
use some

29
00:01:06,320 --> 00:01:11,360
proprietary stuff but that's not

30
00:01:09,840 --> 00:01:14,320
part of the spec.

31
00:01:11,360 --> 00:01:17,360
Then there is EFI image loading in

32
00:01:14,320 --> 00:01:19,020
drivers and application

33
00:01:17,360 --> 00:01:20,320
loading phase.

34
00:01:19,020 --> 00:01:22,400


35
00:01:20,320 --> 00:01:25,840
And then

36
00:01:22,400 --> 00:01:28,320
there is EFI operating system loading

37
00:01:25,840 --> 00:01:31,040
when we doing booting from

38
00:01:28,320 --> 00:01:34,560
some ordered list of the

39
00:01:31,040 --> 00:01:37,200
of the supported OSes or loaders

40
00:01:34,560 --> 00:01:38,400
and then there is a boot service

41
00:01:37,200 --> 00:01:40,479


42
00:01:38,400 --> 00:01:44,079
terminate.

43
00:01:40,479 --> 00:01:46,880
So, when we hand-off control to

44
00:01:44,079 --> 00:01:49,759
some operating system or some bootloader

45
00:01:46,880 --> 00:01:52,159
depending who do ExitBootServices().

46
00:01:49,759 --> 00:01:55,360
And that's end of the

47
00:01:52,159 --> 00:01:57,119
boot manager components.

48
00:01:55,360 --> 00:02:00,000
How those

49
00:01:57,119 --> 00:02:01,680
components interact

50
00:02:00,000 --> 00:02:04,399
each each component

51
00:02:01,680 --> 00:02:07,840
when loading EFI binaries

52
00:02:04,399 --> 00:02:11,440
have specific API. So, there is specific

53
00:02:07,840 --> 00:02:12,879
API for loading EFI drivers, EFI

54
00:02:11,440 --> 00:02:16,000
applications

55
00:02:12,879 --> 00:02:17,360
and there is specific

56
00:02:16,000 --> 00:02:19,840
API for

57
00:02:17,360 --> 00:02:23,840
kicking the

58
00:02:19,840 --> 00:02:26,400
UEFI application shell or bootloader or

59
00:02:23,840 --> 00:02:28,800
some boot code which initialize

60
00:02:26,400 --> 00:02:31,040
bootloader or operating system, and

61
00:02:28,800 --> 00:02:34,720
there's also some specific

62
00:02:31,040 --> 00:02:38,080
failure path if something goes wrong.

63
00:02:34,720 --> 00:02:40,160
There is only one dotted arrow when

64
00:02:38,080 --> 00:02:41,519
there is discussion about value-added

65
00:02:40,160 --> 00:02:44,490
implementation,

66
00:02:41,519 --> 00:02:45,599
so so this is about

67
00:02:44,490 --> 00:02:47,360


68
00:02:45,599 --> 00:02:50,080
extending the

69
00:02:47,360 --> 00:02:51,120
capabilities through shared partition

70
00:02:50,080 --> 00:02:53,040
and

71
00:02:51,120 --> 00:02:54,720
through use of

72
00:02:53,040 --> 00:02:57,680
various components,

73
00:02:54,720 --> 00:03:00,800
And in comparison to what was defined

74
00:02:57,680 --> 00:03:02,840
in UEFI specification. That's the place

75
00:03:00,800 --> 00:03:05,599
when we load the

76
00:03:02,840 --> 00:03:08,560
final operating system, that's the

77
00:03:05,599 --> 00:03:11,200
place when any value added should be

78
00:03:08,560 --> 00:03:13,519
provided according to UEFI specification

79
00:03:11,200 --> 00:03:16,959
designers. So, this is about

80
00:03:13,519 --> 00:03:19,840
our setup menu this is about

81
00:03:16,959 --> 00:03:21,920
some additional things that can be

82
00:03:19,840 --> 00:03:24,480
executed at this point.

83
00:03:21,920 --> 00:03:26,720
So, as we can see there is this

84
00:03:24,480 --> 00:03:29,680
generic boot manager related

85
00:03:26,720 --> 00:03:32,159
code which handles basic initialization

86
00:03:29,680 --> 00:03:35,760
and loading of the components and

87
00:03:32,159 --> 00:03:39,760
those are these EFI binaries, and those EFI

88
00:03:35,760 --> 00:03:40,959
binaries typically are provided by

89
00:03:39,760 --> 00:03:43,760


90
00:03:40,959 --> 00:03:46,000
some vendors someone who did do the

91
00:03:43,760 --> 00:03:48,640
implementation and the boot manager part

92
00:03:46,000 --> 00:03:53,280
is provided by

93
00:03:48,640 --> 00:03:53,280
UEFI spec and reference implementation.

94
00:03:54,239 --> 00:03:57,519
Yeah, so the specification assumes

95
00:03:56,720 --> 00:04:00,560
that

96
00:03:57,519 --> 00:04:01,840
all non-architectural components are

97
00:04:00,560 --> 00:04:04,400

98
00:04:01,840 --> 00:04:06,959
responsibility of the independent BIOS

99
00:04:04,400 --> 00:04:09,200
vendors (IBVs) or OEM.

100
00:04:06,959 --> 00:04:10,640
So those non-architectural components

101
00:04:09,200 --> 00:04:12,239
are

102
00:04:10,640 --> 00:04:14,879
EFI binaries,

103
00:04:12,239 --> 00:04:16,320
and architectural components is boot

104
00:04:14,879 --> 00:04:19,759
manager.

105
00:04:16,320 --> 00:04:22,400
So there is no enforcement how to

106
00:04:19,759 --> 00:04:25,440
implement those

107
00:04:22,400 --> 00:04:27,759
non-architectural

108
00:04:25,440 --> 00:04:29,120
components like EFI drivers, EFI

109
00:04:27,759 --> 00:04:32,960
applications,

110
00:04:29,120 --> 00:04:36,560
boot code or OS loader it is up to the

111
00:04:32,960 --> 00:04:39,120
IBV or OEM how to do that.

112
00:04:36,560 --> 00:04:41,440
So UEFI spec only say about the

113
00:04:39,120 --> 00:04:45,040
interface for those

114
00:04:41,440 --> 00:04:46,320
so this API specification which is in

115
00:04:45,040 --> 00:04:48,800
bold arrow

116
00:04:46,320 --> 00:04:50,720
in this diagram.

117
00:04:48,800 --> 00:04:51,840
From one perspective this diagram is

118
00:04:50,720 --> 00:04:54,840
important

119
00:04:51,840 --> 00:04:57,600
since it presents a mindset of UEFI

120
00:04:54,840 --> 00:05:01,039
Forum and people

121
00:04:57,600 --> 00:05:04,560
behind a whole unified effort but on the

122
00:05:01,039 --> 00:05:07,199
other side it may be hard to grasp

123
00:05:04,560 --> 00:05:09,520
by engineers who did not grow in that

124
00:05:07,199 --> 00:05:12,160
ecosystem and kind of

125
00:05:09,520 --> 00:05:13,680
may have hard time to recognize the

126
00:05:12,160 --> 00:05:17,759
difference between

127
00:05:13,680 --> 00:05:17,759
boot manager and EFI binaries.



