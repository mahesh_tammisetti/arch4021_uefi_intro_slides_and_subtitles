1
00:00:00,080 --> 00:00:05,759
Let's discuss UEFI calling conversion

2
00:00:03,600 --> 00:00:07,279
so all functions defined in the UEFI

3
00:00:05,759 --> 00:00:08,960
specification are called through

4
00:00:07,279 --> 00:00:11,120
pointers,

5
00:00:08,960 --> 00:00:13,200
in a way that is common and

6
00:00:11,120 --> 00:00:15,759
architecturally defined

7
00:00:13,200 --> 00:00:17,760
as in calling conventions which can be

8
00:00:15,759 --> 00:00:19,840
found in C compilers.

9
00:00:17,760 --> 00:00:21,039
Pointers to global functions like

10
00:00:19,840 --> 00:00:24,960
runtime

11
00:00:21,039 --> 00:00:26,240
services functions but boot time

12
00:00:24,960 --> 00:00:27,760
services

13
00:00:26,240 --> 00:00:29,599
functions

14
00:00:27,760 --> 00:00:31,359
are located in

15
00:00:29,599 --> 00:00:34,079
relevant

16
00:00:31,359 --> 00:00:35,040
service tables. So, runtime service table

17
00:00:34,079 --> 00:00:38,239
and boot...

18
00:00:35,040 --> 00:00:39,680
a boot service table as well as

19
00:00:38,239 --> 00:00:42,239
and those

20
00:00:39,680 --> 00:00:44,719
tables pointers to those tables are in

21
00:00:42,239 --> 00:00:46,239
EFI system table.

22
00:00:44,719 --> 00:00:48,640
Pointers to

23
00:00:46,239 --> 00:00:50,719
other functions are obtained dynamically

24
00:00:48,640 --> 00:00:54,320
through device handles.

25
00:00:50,719 --> 00:00:56,719
And all pointers are prepended with

26
00:00:54,320 --> 00:00:59,600
the word EFIAPI

27
00:00:56,719 --> 00:01:03,359
and that helps in

28
00:00:59,600 --> 00:01:04,320
adjusting in every environment,

29
00:01:03,359 --> 00:01:06,479


30
00:01:04,320 --> 00:01:07,680
correctly adjusting to the calling

31
00:01:06,479 --> 00:01:09,680
convention,

32
00:01:07,680 --> 00:01:12,000
used on given

33
00:01:09,680 --> 00:01:14,080
architecture.

34
00:01:12,000 --> 00:01:15,280
When passing pointers as arguments

35
00:01:14,080 --> 00:01:18,080
to function

36
00:01:15,280 --> 00:01:20,799
there are some rules, first of all

37
00:01:18,080 --> 00:01:22,240
the pointers should be always to

38
00:01:20,799 --> 00:01:24,840
physical memory,

39
00:01:22,240 --> 00:01:26,799
those pointers should be correctly

40
00:01:24,840 --> 00:01:29,840
aligned

41
00:01:26,799 --> 00:01:32,000
there should be not-

42
00:01:29,840 --> 00:01:34,400
like the caller should make sure that

43
00:01:32,000 --> 00:01:35,680
not passing null

44
00:01:34,400 --> 00:01:37,119
unless it is allowed by the

45
00:01:35,680 --> 00:01:39,119
specification.

46
00:01:37,119 --> 00:01:42,720
And there shouldn't be no assumption

47
00:01:39,119 --> 00:01:44,720
about the pointer state when

48
00:01:42,720 --> 00:01:48,240
when functions call

49
00:01:44,720 --> 00:01:50,720
returns an error. So, if we pass the

50
00:01:48,240 --> 00:01:51,680
pointer and and we call the function,

51
00:01:50,720 --> 00:01:53,040
and function

52
00:01:51,680 --> 00:01:55,119
just fails

53
00:01:53,040 --> 00:01:57,040
and we should not assume anything about

54
00:01:55,119 --> 00:01:59,840
the content of the

55
00:01:57,040 --> 00:02:02,640
memory behind the pointer.

56
00:01:59,840 --> 00:02:04,880
Structures bigger than four bytes on

57
00:02:02,640 --> 00:02:07,360
32-bit processors and eight bytes on

58
00:02:04,880 --> 00:02:08,399
64-bit processors should also be passed by

59
00:02:07,360 --> 00:02:11,120
pointer,

60
00:02:08,399 --> 00:02:13,920


61
00:02:11,120 --> 00:02:15,120
and it is very important to note that

62
00:02:13,920 --> 00:02:17,280


63
00:02:15,120 --> 00:02:18,720
that UEFI uses its own

64
00:02:17,280 --> 00:02:21,680


65
00:02:18,720 --> 00:02:24,879
data types definitions for

66
00:02:21,680 --> 00:02:27,040
unsigned integer so it is UINTN

67
00:02:24,879 --> 00:02:28,239
and

68
00:02:27,040 --> 00:02:29,280
UINT8

69
00:02:28,239 --> 00:02:32,400


70
00:02:29,280 --> 00:02:34,080
with the capital letters and so on.

71
00:02:32,400 --> 00:02:36,239
So, this is something that you have to

72
00:02:34,080 --> 00:02:37,120
use to when you're doing development

73
00:02:36,239 --> 00:02:39,599
under

74
00:02:37,120 --> 00:02:39,599
UEFI.



