1
00:00:01,040 --> 00:00:05,600
Okay, next phase is driver execution

2
00:00:04,319 --> 00:00:07,200
environment.

3
00:00:05,600 --> 00:00:09,120
So after

4
00:00:07,200 --> 00:00:10,960
finalizing our memory initialization

5
00:00:09,120 --> 00:00:13,679
processor initialization,

6
00:00:10,960 --> 00:00:15,759
chipset initialization, we're going to

7
00:00:13,679 --> 00:00:18,320
we're going to

8
00:00:15,759 --> 00:00:21,520
to DXE phase. So, DXE phase is

9
00:00:18,320 --> 00:00:22,560
described in PEI specification volume

10
00:00:21,520 --> 00:00:24,560
2.

11
00:00:22,560 --> 00:00:26,640
And

12
00:00:24,560 --> 00:00:27,840
we finally have memory initialized we

13
00:00:26,640 --> 00:00:29,279
can start

14
00:00:27,840 --> 00:00:31,679
some device initialization more

15
00:00:29,279 --> 00:00:33,040
sophisticated stuff, PCI enumeration these

16
00:00:31,679 --> 00:00:33,920
kind of things.

17
00:00:33,040 --> 00:00:35,040


18
00:00:33,920 --> 00:00:37,120
And

19
00:00:35,040 --> 00:00:39,440
driver execution environment phase

20
00:00:37,120 --> 00:00:41,920
provides common interface for all

21
00:00:39,440 --> 00:00:44,960
drivers. So, we know that this interface

22
00:00:41,920 --> 00:00:47,760
from the picture

23
00:00:44,960 --> 00:00:50,800
we see that this phase is responsible for

24
00:00:47,760 --> 00:00:54,719
creation UEFI interface

25
00:00:50,800 --> 00:00:58,079
which UEFI interface is a main topic of

26
00:00:54,719 --> 00:00:59,039
the UEFI specification.

27
00:00:58,079 --> 00:01:00,800

28
00:00:59,039 --> 00:01:03,440
Okay

29
00:01:00,800 --> 00:01:06,320
so because we have common

30
00:01:03,440 --> 00:01:08,799
interface for loading drivers, we have

31
00:01:06,320 --> 00:01:11,360
something exactly the same as in PEI so

32
00:01:08,799 --> 00:01:13,360
we have DXE dispatcher which loads all

33
00:01:11,360 --> 00:01:15,520
those drivers, make sure that those are

34
00:01:13,360 --> 00:01:18,640
executed in correct order.

35
00:01:15,520 --> 00:01:21,280


36
00:01:18,640 --> 00:01:23,439
So, the difference is just between PEI

37
00:01:21,280 --> 00:01:26,640
and DXE that we have full address space

38
00:01:23,439 --> 00:01:29,840
in DRAM and way more services and

39
00:01:26,640 --> 00:01:33,520
and we have a goal of producing

40
00:01:29,840 --> 00:01:36,400
a UEFI interface using all those 

41
00:01:33,520 --> 00:01:36,400
drivers.

42
00:01:38,640 --> 00:01:44,880
So, typically entry point function to DXE

43
00:01:41,920 --> 00:01:44,880
drivers take

44
00:01:45,600 --> 00:01:52,000
file handle and and system table.

45
00:01:50,000 --> 00:01:54,960
The naming is also a little bit

46
00:01:52,000 --> 00:01:56,880
different, 

47
00:01:54,960 --> 00:01:58,799
we have other components so

48
00:01:56,880 --> 00:02:00,560
instead of PEI foundation we have DXE

49
00:01:58,799 --> 00:02:03,040
foundation, PEI dispatcher, DXE

50
00:02:00,560 --> 00:02:05,840
dispatcher, but then we don't have PI

51
00:02:03,040 --> 00:02:08,000
modules but we have DXE drivers and

52
00:02:05,840 --> 00:02:09,679
not DXE modules.

53
00:02:08,000 --> 00:02:13,280
And there are some DXE architectural

54
00:02:09,679 --> 00:02:16,000
protocols instead of PPIs or 

55
00:02:13,280 --> 00:02:19,200
PEIM-to-PEIM interfaces.

56
00:02:16,000 --> 00:02:22,720
How DXE protocols are defined? 

57
00:02:19,200 --> 00:02:25,360
So, you can see on top of the hardware

58
00:02:22,720 --> 00:02:28,400
a lot of various protocols though those

59
00:02:25,360 --> 00:02:30,879
protocols change often in UEFI

60
00:02:28,400 --> 00:02:33,519
specification. So, this diagram may be

61
00:02:30,879 --> 00:02:36,480
different from what is available

62
00:02:33,519 --> 00:02:38,959
right now in the spec. And I don't

63
00:02:36,480 --> 00:02:42,640
want to go into much details here,

64
00:02:38,959 --> 00:02:42,640
what is important here is

65
00:02:43,599 --> 00:02:49,760
what kind of like

66
00:02:45,519 --> 00:02:53,200
examples of protocols we have. 

67
00:02:49,760 --> 00:02:55,360
So we have real-time clock protocol, we

68
00:02:53,200 --> 00:02:57,519
have status code protocol which is

69
00:02:55,360 --> 00:02:59,440
related to post codes,

70
00:02:57,519 --> 00:03:01,599
monotonic counter which we talked about

71
00:02:59,440 --> 00:03:04,159
runtime services used that,

72
00:03:01,599 --> 00:03:05,760
there is a variable write protocol which

73
00:03:04,159 --> 00:03:06,959
is related to

74
00:03:05,760 --> 00:03:08,239

75
00:03:06,959 --> 00:03:10,720
ability of

76
00:03:08,239 --> 00:03:10,720
writing

77
00:03:11,200 --> 00:03:16,080
various variables from the operating

78
00:03:13,200 --> 00:03:19,599
system like

79
00:03:16,080 --> 00:03:22,879
BootOrder or some options

80
00:03:19,599 --> 00:03:26,720
like maybe secure boot related variables.

81
00:03:22,879 --> 00:03:29,360
There are some BDS protocols which

82
00:03:26,720 --> 00:03:30,959
are related due to next phase of booting

83
00:03:29,360 --> 00:03:32,720
boot device selection (BDS)

84
00:03:30,959 --> 00:03:35,200
phase.

85
00:03:32,720 --> 00:03:36,879
There are some cpu related security

86
00:03:35,200 --> 00:03:37,840
related protocols,

87
00:03:36,879 --> 00:03:39,360
so

88
00:03:37,840 --> 00:03:41,599


89
00:03:39,360 --> 00:03:42,319
this may change from from time to

90
00:03:41,599 --> 00:03:44,879
time.

91
00:03:42,319 --> 00:03:47,519
On the top you can see also HOB list and

92
00:03:44,879 --> 00:03:50,640
how HOB list typically look like, there is

93
00:03:47,519 --> 00:03:52,319
PHIT hope which is just a dummy

94
00:03:50,640 --> 00:03:53,599
hope that's saying that's the beginning of

95
00:03:52,319 --> 00:03:55,760
the HOB list,

96
00:03:53,599 --> 00:03:59,920
and then there are there is linked

97
00:03:55,760 --> 00:04:02,879
list of HOBs and as we said that

98
00:03:59,920 --> 00:04:05,680
what's inside the HOB is not

99
00:04:02,879 --> 00:04:07,760
not precisely defined, 

100
00:04:05,680 --> 00:04:10,400
the format of data is not defined there

101
00:04:07,760 --> 00:04:14,159
is just a rough structure there

102
00:04:10,400 --> 00:04:17,120
and an exact content is up to the 

103
00:04:14,159 --> 00:04:17,120
implementation.

104
00:04:18,639 --> 00:04:23,360

105
00:04:19,840 --> 00:04:26,320
What are the DXE services,

106
00:04:23,360 --> 00:04:29,199
what services DXE

107
00:04:26,320 --> 00:04:31,919
can use? So, first of all there is task

108
00:04:29,199 --> 00:04:33,919
priority management there is memory

109
00:04:31,919 --> 00:04:35,120
related stuff like memory allocation

110
00:04:33,919 --> 00:04:37,360
services,

111
00:04:35,120 --> 00:04:39,440
there are services related with

112
00:04:37,360 --> 00:04:41,680
retrieving memory map. We have

113
00:04:39,440 --> 00:04:43,520
asynchronous events

114
00:04:41,680 --> 00:04:45,919
and timers,

115
00:04:43,520 --> 00:04:48,639
protocol management services so this

116
00:04:45,919 --> 00:04:49,680
installation of various APIs on

117
00:04:48,639 --> 00:04:53,040
handles,

118
00:04:49,680 --> 00:04:55,360
image services which include

119
00:04:53,040 --> 00:04:56,479
security verification so for example we

120
00:04:55,360 --> 00:04:57,759
can check

121
00:04:56,479 --> 00:05:02,320
if the

122
00:04:57,759 --> 00:05:04,320
executable that we loading is

123
00:05:02,320 --> 00:05:08,720


124
00:05:04,320 --> 00:05:10,639
correctly signed and so on.

125
00:05:08,720 --> 00:05:12,400
And there are driver management services

126
00:05:10,639 --> 00:05:16,320
which means registering and

127
00:05:12,400 --> 00:05:18,880
unregistering device drivers,

128
00:05:16,320 --> 00:05:23,280
and during this phase

129
00:05:18,880 --> 00:05:25,680
for x86 architecture, we're doing

130
00:05:23,280 --> 00:05:28,160
some

131
00:05:25,680 --> 00:05:29,680
most important initialization

132
00:05:28,160 --> 00:05:30,720
of the devices.

133
00:05:29,680 --> 00:05:33,039
So,

134
00:05:30,720 --> 00:05:34,400
I would say in PEI we will talk more

135
00:05:33,039 --> 00:05:37,199
about

136
00:05:34,400 --> 00:05:42,160
core features like memory

137
00:05:37,199 --> 00:05:44,320
CPU chipset and now we should have those

138
00:05:42,160 --> 00:05:46,400
key core components

139
00:05:44,320 --> 00:05:49,680
initialized and now we can go through

140
00:05:46,400 --> 00:05:51,600
the buses and find devices and load the

141
00:05:49,680 --> 00:05:52,400
drivers for those devices if we have

142
00:05:51,600 --> 00:05:55,039
those

143
00:05:52,400 --> 00:05:57,759
and if there is need for

144
00:05:55,039 --> 00:06:00,160
exposing those for further phases.

145
00:05:57,759 --> 00:06:01,360
So, PCI enumeration for example happen

146
00:06:00,160 --> 00:06:02,479
in this stage.

147
00:06:01,360 --> 00:06:04,560


148
00:06:02,479 --> 00:06:07,520
System management mode initialization

149
00:06:04,560 --> 00:06:08,880
and system management mode initial

150
00:06:07,520 --> 00:06:12,000
program loader

151
00:06:08,880 --> 00:06:14,800
is executed

152
00:06:12,000 --> 00:06:16,319
which loads SMM core functionality

153
00:06:14,800 --> 00:06:19,520
which is like a

154
00:06:16,319 --> 00:06:22,000
main support for system management mode.

155
00:06:19,520 --> 00:06:25,600


156
00:06:22,000 --> 00:06:28,720
So, from that point SMIs could be

157
00:06:25,600 --> 00:06:30,720
generated and 

158
00:06:28,720 --> 00:06:32,160
and of course code which is stored in

159
00:06:30,720 --> 00:06:33,199
SMRAM

160
00:06:32,160 --> 00:06:35,759
can be

161
00:06:33,199 --> 00:06:37,520
reached and executed.

162
00:06:35,759 --> 00:06:39,840
In this phase also at the end of this

163
00:06:37,520 --> 00:06:43,840
phase there is SMRAM locking and all

164
00:06:39,840 --> 00:06:46,000
the security mechanism are applied

165
00:06:43,840 --> 00:06:48,400
at the end of this phase,

166
00:06:46,000 --> 00:06:49,840
although it may change depending on

167
00:06:48,400 --> 00:06:51,520
the implementation.

168
00:06:49,840 --> 00:06:53,120
In this phase typically MSR

169
00:06:51,520 --> 00:06:55,360
configuration happen,

170
00:06:53,120 --> 00:06:57,440
this is model specific this is like

171
00:06:55,360 --> 00:07:00,319
hardware specific situation,

172
00:06:57,440 --> 00:07:01,599
but also system management

173
00:07:00,319 --> 00:07:04,000
mode,

174
00:07:01,599 --> 00:07:07,280
range registers and memory type range

175
00:07:04,000 --> 00:07:09,360
registers which are responsible for

176
00:07:07,280 --> 00:07:12,639


177
00:07:09,360 --> 00:07:15,120
memory management and caching of

178
00:07:12,639 --> 00:07:18,960
system management memory and

179
00:07:15,120 --> 00:07:20,720
other memory regions this is done in

180
00:07:18,960 --> 00:07:21,840
DXE phase

181
00:07:20,720 --> 00:07:24,800
close to the

182
00:07:21,840 --> 00:07:24,800
end of it.



