1
00:00:00,640 --> 00:00:04,319
The next phrase is boot device selection

2
00:00:03,360 --> 00:00:06,799
phase,

3
00:00:04,319 --> 00:00:10,160
and we can see that already in this

4
00:00:06,799 --> 00:00:13,679
phase we got UEFI interface defined

5
00:00:10,160 --> 00:00:16,400
so our BDS phase can use all the

6
00:00:13,679 --> 00:00:18,240
boot services all the runtime services

7
00:00:16,400 --> 00:00:19,600
everything what is defined in UEFI

8
00:00:18,240 --> 00:00:21,760
specification.

9
00:00:19,600 --> 00:00:24,480
So, we can see that

10
00:00:21,760 --> 00:00:25,439
that despite UEFI specifications is quite

11
00:00:24,480 --> 00:00:28,160
big.

12
00:00:25,439 --> 00:00:31,679
The use of those interfaces is quite

13
00:00:28,160 --> 00:00:34,320
late. All the

14
00:00:31,679 --> 00:00:38,719
guts of the components that established

15
00:00:34,320 --> 00:00:41,520
this interface had to be developed

16
00:00:38,719 --> 00:00:43,600
separately and probably very close to

17
00:00:41,520 --> 00:00:45,520
the vendors of the hardware

18
00:00:43,600 --> 00:00:47,360
to make it correct,

19
00:00:45,520 --> 00:00:50,160
to correctly expose that interface. So,

20
00:00:47,360 --> 00:00:51,440
BDS is responsible for loading

21
00:00:50,160 --> 00:00:54,000


22
00:00:51,440 --> 00:00:55,199
operating system or some

23
00:00:54,000 --> 00:00:58,559
transient

24
00:00:55,199 --> 00:01:01,600
applications, some for example

25
00:00:58,559 --> 00:01:03,840
memtest or some UEFI shell those

26
00:01:01,600 --> 00:01:06,799
can be run

27
00:01:03,840 --> 00:01:06,799
in BDS.

28
00:01:06,880 --> 00:01:12,479
So, typically in BDS

29
00:01:09,439 --> 00:01:14,799
we can see all the devices. So, because we

30
00:01:12,479 --> 00:01:17,200
can see all the devices we can select

31
00:01:14,799 --> 00:01:19,040
boot media and we can decide what

32
00:01:17,200 --> 00:01:21,759
operating system we're booting

33
00:01:19,040 --> 00:01:22,880
typically,

34
00:01:21,759 --> 00:01:25,200


35
00:01:22,880 --> 00:01:28,000
in this boot media will look for OS

36
00:01:25,200 --> 00:01:29,439
loader which would be in

37
00:01:28,000 --> 00:01:31,920
portable

38
00:01:29,439 --> 00:01:36,320
executable image format

39
00:01:31,920 --> 00:01:38,240
typically with .efi extension.

40
00:01:36,320 --> 00:01:41,200


41
00:01:38,240 --> 00:01:44,159
The boot order is and

42
00:01:41,200 --> 00:01:46,880
the behavior of the BDS phase is

43
00:01:44,159 --> 00:01:48,479
defined through UEFI variables

44
00:01:46,880 --> 00:01:51,040


45
00:01:48,479 --> 00:01:52,840
which we can modify either from

46
00:01:51,040 --> 00:01:54,560
operating system either

47
00:01:52,840 --> 00:01:57,600
from

48
00:01:54,560 --> 00:02:00,560
UEFI setup menu

49
00:01:57,600 --> 00:02:02,799
which is this grey blue

50
00:02:00,560 --> 00:02:05,439
screens that we can enter during the

51
00:02:02,799 --> 00:02:07,759
boot process or maybe in modern cases

52
00:02:05,439 --> 00:02:09,039
this is even fancy GUI.

53
00:02:07,759 --> 00:02:11,120


54
00:02:09,039 --> 00:02:14,400
So, by default there are some

55
00:02:11,120 --> 00:02:17,360
default file that BDS look for,

56
00:02:14,400 --> 00:02:20,160
so it looks on the first partition for

57
00:02:17,360 --> 00:02:24,160
the file 

58
00:02:20,160 --> 00:02:27,040
efi/boot/boot{architecture}.efi

59
00:02:24,160 --> 00:02:28,319
so for example for x86 

60
00:02:27,040 --> 00:02:33,280
architecture

61
00:02:28,319 --> 00:02:34,480
64-bit would be bootx64.efi

62
00:02:33,280 --> 00:02:36,319


63
00:02:34,480 --> 00:02:39,040


64
00:02:36,319 --> 00:02:41,440
And of course it iterates through all

65
00:02:39,040 --> 00:02:43,920
bootable partitions and first one which

66
00:02:41,440 --> 00:02:46,560
will be found will be executed.

67
00:02:43,920 --> 00:02:49,440
In this case this phase typically

68
00:02:46,560 --> 00:02:50,640
ends with ExitBootServices(), 

69
00:02:49,440 --> 00:02:51,920


70
00:02:50,640 --> 00:02:53,840
we discussed that there are various

71
00:02:51,920 --> 00:02:56,640
types of applications one of this

72
00:02:53,840 --> 00:02:59,440
application can be OS loader and this OS

73
00:02:56,640 --> 00:03:02,720
loader or kernel which this OS loader

74
00:02:59,440 --> 00:03:05,440
loads execute function, ExitBootServices()

75
00:03:02,720 --> 00:03:07,840
which ends up this phase and

76
00:03:05,440 --> 00:03:09,760
ends up

77
00:03:07,840 --> 00:03:12,239
availability of the boot services

78
00:03:09,760 --> 00:03:12,239
functions.

79
00:03:12,959 --> 00:03:18,879
And of course we mentioned that there

80
00:03:14,879 --> 00:03:21,200
is this responsibility confusion because

81
00:03:18,879 --> 00:03:25,040
most operating systems tend to integrate

82
00:03:21,200 --> 00:03:26,560
OS loader with the kernel

83
00:03:25,040 --> 00:03:28,879
and

84
00:03:26,560 --> 00:03:32,239
that means that having separate

85
00:03:28,879 --> 00:03:34,319
bootloader like GRUB, SeaBIOS, iPXE may

86
00:03:32,239 --> 00:03:35,599
cause some technical difficulties with

87
00:03:34,319 --> 00:03:36,480
the question,

88
00:03:35,599 --> 00:03:38,720
should

89
00:03:36,480 --> 00:03:40,400
the bootloader do the ExitBootServices()

90
00:03:38,720 --> 00:03:43,040
or maybe

91
00:03:40,400 --> 00:03:44,959
it should load the operating system and

92
00:03:43,040 --> 00:03:46,959
rely on operating system that the

93
00:03:44,959 --> 00:03:49,200
operating system will do the

94
00:03:46,959 --> 00:03:50,400
ExitBootServices().
