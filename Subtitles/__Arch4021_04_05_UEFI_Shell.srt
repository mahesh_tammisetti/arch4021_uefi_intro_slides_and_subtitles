1
00:00:00,799 --> 00:00:06,640
So, as I mentioned one of the examples of

2
00:00:03,600 --> 00:00:08,639
this applications that can be run in

3
00:00:06,640 --> 00:00:10,960
BDS is UEFI shell

4
00:00:08,639 --> 00:00:14,000
you probably saw UEFI shell

5
00:00:10,960 --> 00:00:17,119
it looks like like on the screen

6
00:00:14,000 --> 00:00:20,000
it's very MS-DOS like,

7
00:00:17,119 --> 00:00:22,720
it has some kind of mix of the

8
00:00:20,000 --> 00:00:23,680
feeling between Linux and

9
00:00:22,720 --> 00:00:27,760
and DOS

10
00:00:23,680 --> 00:00:27,760
but yes this is like more like

11
00:00:28,080 --> 00:00:32,399
a primitive environment with

12
00:00:30,480 --> 00:00:33,840
everything's running in

13
00:00:32,399 --> 00:00:34,800
ring zero

14
00:00:33,840 --> 00:00:36,239

15
00:00:34,800 --> 00:00:37,520

16
00:00:36,239 --> 00:00:39,360
so

17
00:00:37,520 --> 00:00:42,079
according to UEFI

18
00:00:39,360 --> 00:00:45,360
specification this is example UEFI shell

19
00:00:42,079 --> 00:00:48,800
is example of OS absent application so

20
00:00:45,360 --> 00:00:49,760
yes, we have no OS but we have something

21
00:00:48,800 --> 00:00:51,280
running.

22
00:00:49,760 --> 00:00:53,280

23
00:00:51,280 --> 00:00:54,879
It is typically disabled on consumer

24
00:00:53,280 --> 00:00:58,160
platforms

25
00:00:54,879 --> 00:01:00,079
but we can include it as a 

26
00:00:58,160 --> 00:01:02,719
executable on the

27
00:01:00,079 --> 00:01:05,760
bootable device and in that way we can

28
00:01:02,719 --> 00:01:08,960
just run from it, and of course

29
00:01:05,760 --> 00:01:13,600
if the the bootable device contained

30
00:01:08,960 --> 00:01:16,080
this [as] bootx64.efi and standard EFI

31
00:01:13,600 --> 00:01:18,240
will iterate over assuming that

32
00:01:16,080 --> 00:01:21,360
we have no secure boot enable, it will

33
00:01:18,240 --> 00:01:24,479
find it and it will execute it.

34
00:01:21,360 --> 00:01:27,200
Typically, before interactive shell of

35
00:01:24,479 --> 00:01:28,080
UEFI shell is started there is

36
00:01:27,200 --> 00:01:30,240
startup.nsh

37
00:01:28,080 --> 00:01:33,200
script executed and we can see

38
00:01:30,240 --> 00:01:35,520
that in that script there might be

39
00:01:33,200 --> 00:01:36,560
some automation put and we can do

40
00:01:35,520 --> 00:01:41,600
some

41
00:01:36,560 --> 00:01:43,759
automated stuff in startup.nsh.

42
00:01:41,600 --> 00:01:48,000
So, it can be used for more

43
00:01:43,759 --> 00:01:49,920
sophisticated BDS implementation

44
00:01:48,000 --> 00:01:51,759
but, typically this is not used like that

45
00:01:49,920 --> 00:01:54,079
typically it goes through through the

46
00:01:51,759 --> 00:01:55,280
boot order variables and that's it. The

47
00:01:54,079 --> 00:01:59,280
control is

48
00:01:55,280 --> 00:01:59,280
more on the OS loader side.

49
00:02:03,439 --> 00:02:08,399
So, UEFI file shell can be very

50
00:02:05,680 --> 00:02:11,440
useful when your OS refused to

51
00:02:08,399 --> 00:02:13,920
boot and in that situation we can

52
00:02:11,440 --> 00:02:16,800
access various boot media and find for

53
00:02:13,920 --> 00:02:19,280
our EFI files bootloader EFI

54
00:02:16,800 --> 00:02:20,720
file and just like kick

55
00:02:19,280 --> 00:02:21,680
the

56
00:02:20,720 --> 00:02:23,360

57
00:02:21,680 --> 00:02:24,480
loader from the shell

58
00:02:23,360 --> 00:02:25,200
directly

59
00:02:24,480 --> 00:02:27,120
and

60
00:02:25,200 --> 00:02:28,400
it should work if nothing else is

61
00:02:27,120 --> 00:02:32,720
collected.

62
00:02:28,400 --> 00:02:34,560
And below we have a very simple UFI

63
00:02:32,720 --> 00:02:38,239
application we're just doing, “hello world”

64
00:02:34,560 --> 00:02:40,560
so you can see that it's 

65
00:02:38,239 --> 00:02:43,040
extremely simple

66
00:02:40,560 --> 00:02:45,519
maybe compilation of that requires some

67
00:02:43,040 --> 00:02:48,400
additional stuff, but

68
00:02:45,519 --> 00:02:51,400
it's very simple programming

69
00:02:48,400 --> 00:02:51,400
environment.


