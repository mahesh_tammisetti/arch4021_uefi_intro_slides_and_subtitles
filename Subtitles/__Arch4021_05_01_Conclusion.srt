1
00:00:00,240 --> 00:00:06,799
Congratulations, we’ll get to the end of Arch4021

2
00:00:03,600 --> 00:00:09,920
Introductory UEFI. Let's quickly

3
00:00:06,799 --> 00:00:11,679
recap what we learned in this course.

4
00:00:09,920 --> 00:00:14,639
We started with firmware fundamentals,

5
00:00:11,679 --> 00:00:17,359
what is firmware

6
00:00:14,639 --> 00:00:20,320
how modern computing system built, how

7
00:00:17,359 --> 00:00:21,279
firm and what role firmware play in boot

8
00:00:20,320 --> 00:00:23,920
process.

9
00:00:21,279 --> 00:00:26,480
We learned what's the relation between

10
00:00:23,920 --> 00:00:28,640
typical firmware term and BIOS,

11
00:00:26,480 --> 00:00:31,039
and then we moved

12
00:00:28,640 --> 00:00:33,760
trying to explore what's the history of

13
00:00:31,039 --> 00:00:36,880
bootstrapping computing system so we

14
00:00:33,760 --> 00:00:38,719
learned about legacy BIOS and UEFI BIOS

15
00:00:36,880 --> 00:00:40,239
and UEFI specification and PI

16
00:00:38,719 --> 00:00:44,480
specification.

17
00:00:40,239 --> 00:00:45,840
And then we dived deeper into UEFI basics

18
00:00:44,480 --> 00:00:48,320
learning about goals of the

19
00:00:45,840 --> 00:00:50,800
specification learning about features

20
00:00:48,320 --> 00:00:53,360
about architecture also about

21
00:00:50,800 --> 00:00:56,320
Implementation. From the implementation

22
00:00:53,360 --> 00:00:58,239
point of view we explored EDK2

23
00:00:56,320 --> 00:01:00,559
reference implementation of UEFI

24
00:00:58,239 --> 00:01:03,760
Specification, we

25
00:01:00,559 --> 00:01:06,000
did a build of it

26
00:01:03,760 --> 00:01:07,680
of various types, we learned how to enable

27
00:01:06,000 --> 00:01:10,080
and disable some features

28
00:01:07,680 --> 00:01:12,479
we learned how what are the methods of

29
00:01:10,080 --> 00:01:13,680
debugging EDK2

30
00:01:12,479 --> 00:01:16,799
and then

31
00:01:13,680 --> 00:01:20,560
we were able to create an image for the

32
00:01:16,799 --> 00:01:22,159
QEMU platform which can boot this

33
00:01:20,560 --> 00:01:25,520
emulated platform.

34
00:01:22,159 --> 00:01:28,640
Thanks to that we were able to explore

35
00:01:25,520 --> 00:01:30,640
basics of the boot process and

36
00:01:28,640 --> 00:01:32,400
understood structures

37
00:01:30,640 --> 00:01:34,560
which are used to pass information

38
00:01:32,400 --> 00:01:36,320
between boot stages which are called

39
00:01:34,560 --> 00:01:37,040
hands of blocks.

40
00:01:36,320 --> 00:01:41,600
So,

41
00:01:37,040 --> 00:01:44,000
finally we were able to understand

42
00:01:41,600 --> 00:01:46,720
internals of each stages, how to step

43
00:01:44,000 --> 00:01:49,600
through each stages, and how to get from

44
00:01:46,720 --> 00:01:51,200
one stage to another using the

45
00:01:49,600 --> 00:01:54,560
debugging process

46
00:01:51,200 --> 00:01:57,280
and finally we get to UEFI shell and

47
00:01:54,560 --> 00:01:59,119
we explore basic commands.

48
00:01:57,280 --> 00:02:01,680
So, thank you very much for taking this

49
00:01:59,119 --> 00:02:04,960
class, and I hope this class will help

50
00:02:01,680 --> 00:02:07,520
you learn more about UEFI about

51
00:02:04,960 --> 00:02:10,479
boot process, about firmware in general

52
00:02:07,520 --> 00:02:13,120
I hope it will challenge you to explore

53
00:02:10,479 --> 00:02:14,959
more firmware implementations

54
00:02:13,120 --> 00:02:18,959
and maybe

55
00:02:14,959 --> 00:02:21,680
it will help you in basic debugging

56
00:02:18,959 --> 00:02:24,879
tasks, development tasks in your in

57
00:02:21,680 --> 00:02:27,520
your work as well as

58
00:02:24,879 --> 00:02:30,800
researching vulnerabilities in existing

59
00:02:27,520 --> 00:02:33,360
implementation since you already know

60
00:02:30,800 --> 00:02:35,200
what are the weak spots what are various

61
00:02:33,360 --> 00:02:36,959
locations that

62
00:02:35,200 --> 00:02:38,879
those vulnerabilities should be looked

63
00:02:36,959 --> 00:02:41,280
for inside

64
00:02:38,879 --> 00:02:44,239
UEFI implementation. Thank you very much

65
00:02:41,280 --> 00:02:47,120
and good luck with further gaining of

66
00:02:44,239 --> 00:02:47,120
firmware knowledge.


